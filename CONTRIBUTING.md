# Contributing

Any help is welcome.

1. Install cargo-gra 
    ```
    cargo install cargo-gra --locked
    ```
2. Run
    ```
    cargo gra gen
    ```
3. Build flatpak
    ```
    cargo gra flatpak
    ```
   or build binary
    ```
    cargo build --release
    ```
## Merge requests

Please create merge requests into the `develop` branch. Your commits must fit the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) pattern. Karlender does automated releases on merges to the `main` branch. The public release notes are generated by all commit messages. So having a clean and correct commit history is important here.

## Required dependencies

```
sudo apt install libgtk-4-dev
# and libadwaita which is not available yet :(
```

## Logging

You can adjust the loglevel via `RUST_LOG` env var.

> Note: this is not using the `env_logger` crate but I reused the var name

```
RUST_LOG=debug flatpak run codes.loers.Karlender
```

You can also set the level for specific crates via this pattern: `# <overall level>,<lib name>=<level>`.

E.g.:

```
RUST_LOG=trace,karlender=info,minicaldav=trace cargo run
```

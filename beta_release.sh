#!/bin/sh

if [ -z "$1" ]; then
    echo You need to provide an access token
  exit 1
fi

echo Running release...

sha=$(git rev-parse --short HEAD)
cog bump --auto --pre "beta.$sha"  || exit 1
TAG_VERSION=$(grep version Cargo.toml | head -1 | sed 's/version = "//g' | sed 's/"//g')
git checkout -b beta/$TAG_VERSION
git push origin beta/$TAG_VERSION  || exit 1
git tag -d v$TAG_VERSION
git tag pre$TAG_VERSION
git push origin pre$TAG_VERSION  || exit 1

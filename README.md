# karlender

[![pipeline status](https://gitlab.com/floers/karlender/badges/main/pipeline.svg)](https://gitlab.com/floers/karlender/-/commits/main)

GTK calendar application written in Rust.

Karlender is a mobile-friendly calendar app. It uses libadwaita and can access Calendars using CalDAV.

<img src="screenshots/1.png" width="200">

## Required libraries

The following dependencies must be installed:

```
sudo apt install libgtk-3-dev libadwaita-1-dev appstream-util flatpak flatpak-builder gettexet
```

## Support

Wether you submit issues or contribute code: I appreciate all help.

## Installation: Flatpak

`Karlender` is a flatpak app and available on [flathub](https://flathub.org/apps/details/codes.loers.Karlender).

It can be installed and started with:

```shell
# flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak install codes.loers.Karlender
flatpak run codes.loers.Karlender
```

## Installation: Manual

One can manually build and start `Karlender` with these steps:

```shell
cargo install cargo-gra --locked
cargo gra gen
sudo make install-gsettings
RUST_LOG=error,info,debug,trace cargo run
```

See [CONTRIBUTE.md](CONTRIBUTE.md) for more details.

Karlender can also be installed as a system app via

```sh
cargo build --release
cargo gra gen
sudo make -C target/gra-gen/ install ROOT="/usr"
```

and uninstalled via

```sh
sudo make -C target/gra-gen/ install ROOT="/usr"
```
## Implementation Details

See [DOC.md](DOC.md)

## License

Karlender is distributed under the terms of the GPL-3.0 license. See [LICENSE](LICENSE) for details.

## Project status

In active development.

## Further Documentation

- [CONTRIBUTING.md](CONTRIBUTING.md)

# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## v0.10.11 - 2024-11-10
#### Bug Fixes
- Use new version of cargo-gra - (1488f86) - Florian Loers

- - -

## v0.10.10 - 2024-11-10
#### Bug Fixes
- Document dependencies - (9a3415f) - Florian Loers

- - -

## v0.10.9 - 2024-11-09
#### Bug Fixes
- Remove unnecessary flatpak permission - (2d73bb7) - Florian Loers

- - -

## v0.10.8 - 2024-11-09
#### Bug Fixes
- Bump platform version for flatpak build - (eafa102) - Florian Loers

- - -

## v0.10.7 - 2024-11-09
#### Bug Fixes
- Update gtk runtime version - (3e88ffc) - Florian Loers

- - -

## v0.10.6 - 2024-11-09
#### Bug Fixes
- Update app manifest - (1d298c2) - Florian Loers

- - -

## v0.10.5 - 2024-11-09
#### Bug Fixes
- bump version - (a8f9297) - Florian Loers
#### Miscellaneous Chores
- Update dependencies - (8fdf13a) - Florian Loers

- - -

## v0.10.4 - 2023-10-07
#### Bug Fixes
- Remove swipe gestures for navigation - (1c36c5c) - Florian Loers

- - -

## v0.10.3 - 2023-08-27
#### Bug Fixes
- Create local calendars - (67df738) - Florian Loers

- - -

## v0.10.2 - 2023-07-17
#### Bug Fixes
- Disabled calendars are not shown - (b0a7ac0) - Florian Loers
- Better app name and description - (7fc379e) - Florian Loers
- Show events for more than one days - (49cbfd5) - Florian Loers
- Show login from older karlender versions - (092760c) - Florian Loers

- - -

## v0.10.1 - 2023-07-06
#### Bug Fixes
- Correctly handle events without end date - (73a64fd) - Florian Loers

- - -

## v0.10.0 - 2023-07-05
#### Bug Fixes
- Handle DST in events - (3d2babc) - Florian Loers
- Update gtk libraries - (dd678a2) - Florian Loers
#### Features
- Support GNOME Online Accounts - (9c59cc8) - Florian Loers
- Add Yearly View - (f38b70a) - Florian Loers
#### Miscellaneous Chores
- **(version)** v0.9.2 - (7bf7e14) - gitlab ci

- - -


## v0.9.2 - 2023-04-23
#### Bug Fixes
- Update gtk libraries - (5a04fc6) - Florian Loers

- - -

## v0.9.1 - 2023-03-10
#### Bug Fixes
- Align header buttons without at start - (9050efd) - Florian Loers
- Make weeks-view vertically scrollable - (a264205) - Florian Loers
#### Documentation
- Update liberapay note - (679742b) - Florian Loers
#### Miscellaneous Chores
- Update dependencies - (bac8462) - Florian Loers

- - -

## v0.9.0 - 2022-12-28
#### Features
- Selecting a start date greated than the current end date also moves the end date - (5ece540) - Florian Loers
- Switching off full-day in editor expands the time pickers - (75071c7) - Florian Loers

- - -

## v0.8.0 - 2022-11-13
#### Features
- Allow to import ICS files via menu item - (6a45909) - Florian Loers

- - -

## v0.7.1 - 2022-11-01
#### Bug Fixes
- Release notes are updated again - (78a7c82) - Florian Loers

- - -

## v0.7.0 - 2022-10-31
#### Bug Fixes
- Update project links after move - (691d93d) - Florian Loers
#### Features
- Allow to open .ics files in Karlender - (567a2ee) - Florian Loers
- Show server calendar colors - (7077ce7) - Florian Loers

- - -

## v0.6.2 - 2022-10-03
#### Bug Fixes
- Fix broken navigation to about page - (93b1348) - Florian Loers
- Update month/week/day titles on navigation - (4104486) - Florian Loers
- Events in weekly view do not mess up the layout - (fbad800) - Florian Loers
#### Miscellaneous Chores
- Migrate to gtk-rust-app 0.9 - (186f7a3) - Florian Loers

- - -

## v0.6.1 - 2022-08-20
#### Bug Fixes
- Updating and deleting synced Events works - (827364d) - Florian Loers

- - -

## v0.6.0 - 2022-07-10
#### Bug Fixes
- Fix caldav error syncing events without end date - (047862e) - Florian Loers
#### Continuous Integration
- Fix automated versioning - (740a472) - Florian Loers
#### Documentation
- Fix broken changelogs - (4355f74) - Florian Loers
#### Features
- Add weekly page - (a2368bf) - Florian Loers

- - -

## v0.5.1 - 2022-07-03
#### Bug Fixes
- Monthly view shows correct month in title - (931622f) - Florian Loers
- Improve readability of events in month view - (a24d4c4) - Florian Loers
- Monthly view shows correct month in title - (e62d31a) - Florian Loers
- Monthly view shows selected date instead of month name - (8af1a4e) - Florian Loers
- Cargo.toml typo "apative" - (9c6ff40) - S. Knorr
#### Continuous Integration
- Run release only on main branch instead of default barnch - (47d06df) - Florian Loers
- Append git hash to version for development builds - (8608879) - Florian Loers
#### Documentation
- Improve information about merge requests and commit message in CONTRIBUTING.md - (35e66bb) - Florian Loers
- Add more details on installation/build to README - (8aca422) - langfingaz
#### Features
- Introduce feedback page - (95b68c4) - Florian Loers
#### Miscellaneous Chores
- **(version)** v0.5.1 - (fda244e) - gitlab ci
- **(version)** v0.5.0 - (76d811f) - gitlab ci
- Bump gtk-rust-app version - (278261f) - Florian Loers
- Fail arm64 build if artifact does not exist - (4ffb986) - Florian Loers
- Bump minicaldav version - (06f898d) - Florian Loers
- Bump minicaldav version - (8f5feea) - Florian Loers
- Refactor month carousel loading - (1ad3bc6) - Florian Loers

- - -

## v0.4.4 - 2022-06-18
#### Bug Fixes
- Karlender does not rely on date command for timezone detection. - (ba3c1e9) - Florian Loers
- Validate CalDAV URL on login - (f2eb349) - Florian Loers
- Show error message when saved login contains an invalid URL - (d8576f1) - Florian Loers
- Show error when sync failed - (6bdba20) - Florian Loers
- Show add account button after removing account - (57fa81e) - Florian Loers
#### Continuous Integration
- Add manual stage that builds flatpak test artifacts - (b668f94) - Florian Loers
- Fail on clippy warnings - (5b3a00b) - Florian Loers
#### Miscellaneous Chores
- Improve stability of secret-service comunication - (61ebaf7) - Florian Loers
- Bump minicaldav version - (ed84425) - Florian Loers

- - -

## v0.4.3 - 2022-05-10
#### Bug Fixes
- Fix display of repeated events for daily and weekly repetition - (d8a6c10) - Florian Loers
- - -

## v0.4.2 - 2022-05-10
#### Bug Fixes
- Create leading directories in persistence::load - (47cc5ce) - Newbyte
- - -

## v0.4.1 - 2022-05-10
#### Continuous Integration
- Fix ci builds missing code generation - (20344e4) - Florian Loers
- - -

## v0.4.0 - 2022-04-04
#### Bug Fixes
- Menu entry close and close shortcut close the app - (06591e9) - Florian Loers
#### Features
- Support local calendars - (3651d36) - Florian Loers
- Display month name instead of month number in monthly view title - (e548121) - Florian Loers
- - -

## v0.3.16 - 2022-03-31
#### Bug Fixes
- Show current month and day in title bar - (04ae3bd) - Florian Loers
- - -

## v0.3.15 - 2022-03-30
#### Documentation
- Update app description - (b2689b4) - Florian Loers
#### Miscellaneous Chores
- Update app screenshots - (1287795) - Florian Loers
- - -

## v0.3.14 - 2022-03-30
#### Bug Fixes
- Fix release notes generation - (09d2112) - Florian Loers
#### Continuous Integration
- Repair broken release job - (35ab4e8) - Florian Loers
#### Miscellaneous Chores
- **(version)** v0.3.14 - (6131e23) - gitlab ci
- - -

## v0.3.13 - 2022-03-29
#### Bug Fixes
- Fix appdata.xml due to update of cargo-gra - (486df63) - Florian Loers
#### Miscellaneous Chores
- **(version)** v0.3.12 - (ea6ac23) - gitlab ci
- - -

## v0.3.12 - 2022-03-29
#### Miscellaneous Chores
- **(version)** v0.3.11 - (763ae31) - gitlab ci
- Fix flatpak changelog generation - (f0619c9) - Florian Loers
- - -

## v0.3.11 - 2022-03-28
#### Documentation
- Improve readme, contribute and roadmap documentation - (decaac0) - Florian Loers
#### Miscellaneous Chores
- **(version)** v0.3.10 - (50b7760) - gitlab ci
- - -

## v0.3.10 - 2022-03-28
#### Miscellaneous Chores
- **(version)** v0.3.9 - (6917006) - gitlab ci
- Bump gtk-rust-app version - (02fefb9) - Florian Loers
- - -

## v0.3.9 - 2022-03-28
#### Bug Fixes
- Remove unnecessary flatpak permissions - (ebf7248) - Florian Loers
#### Miscellaneous Chores
- **(version)** v0.3.8 - (07ac574) - gitlab ci
- - -

## v0.3.8 - 2022-03-27
#### Miscellaneous Chores
- **(version)** v0.3.7 - (ae48de7) - gitlab ci
- Bump gtk-rust-app version - (f324500) - Florian Loers
- Bump gtk-rust-app version - (fd04ad0) - Florian Loers
- - -

## v0.3.7 - 2022-03-20
#### Bug Fixes
- Fix panic errors in settings and login pages - (77bbf36) - Florian Loers
#### Miscellaneous Chores
- **(version)** v0.3.6 - (9d554e6) - gitlab ci
- - -

## v0.3.6 - 2022-03-20
#### Miscellaneous Chores
- Bump several dependencies - (5db1496) - Florian Loers
- - -

## v0.3.4 - 2022-03-19
#### Miscellaneous Chores
- Use minicaldav - (5f83659) - Florian Loers
- Reduce binary size - (c00fdfb) - Florian Loers
- - -

## v0.3.3 - 2022-02-17
#### Documentation
- Update release notes - (db0cc29) - Florian Loers
- - -

## v0.3.2 - 2022-02-17
#### Bug Fixes
- Tiemzone selection works on small screens - (2c4127d) - Florian Loers
#### Miscellaneous Chores
- Improve changelog generation - (41a6736) - Florian Loers
- - -

## v0.3.1 - 2022-02-13
#### Documentation
- Add links to latest flatpak artifacts - (3eaf3a4) - Florian Loers
- - -

## v0.3.0 - 2022-02-13
#### Features
- Create new events in remote CalDAV calendars - (1d9a278) - Florian Loers
- - -

## v0.2.1 - 2022-01-20
#### Bug Fixes
- Support smaller screens - (b65d888) - Florian Loers
- - -

## v0.2.0 - 2022-01-20
#### Bug Fixes
- Swiping 12 months back no longer leads to crash - (0d48b47) - Florian Loers
#### Features
- Add settings page - (8f34548) - Florian Loers
- View events from caldav calendar - (6041951) - Florian Loers
#### Miscellaneous Chores
- Bump gtk-rust-app version - (3fc9724) - Florian Loers
- Update app icon - (7e74eda) - Florian Loers
- - -

Changelog generated by [cocogitto](https://github.com/cocogitto/cocogitto).
# Karlender Architecture

## Code overview

Karlender uses grx and gstore to implement a GTK App in a similar fashion to how React+Redux works. Also Karlender loosley uses Domain Driven Design.

There are components which might have a local state and a global state that can be mutated by `dispatching` `Actions`.

Lets check the file tree

```
assets/       # Images and icons
po/           # translation files
src/          # src code
  domain/     # all domain structs and enums. Things like `Event` and `Date` are here
  store/      # The global store slices are defined in several modules here
              #   Each slice defines a part of the global state, actions that modify this state and a middleware for async stuff to do on state changes.
  ui/         # All UI components are placed here
  main.rs     # the entry point. Initialzes GTK and does some basic app stuff.
  store.rs    # This module initializes the global store and state. All slices are aggregated here.
  styles.css  # Styles.
App.toml      # To build Karlender I use a CLI I created: cargo-gra. It allows building gtk apps in a cargo fashion. It requires this manifest file.
Cargo.toml    # The cargo manifest.
```

## App directory

Karlender is able to do CalDAV syncing. Synced events are placed in `.local/share/karlender` (or `.var/app/codes.loers.Karlender/karlener` in case of running karlender via flatpak).

The directoy has the following structure:

```
karlender/               
  calendars/             # currently there is only the calendars folder
    index                # it contains an empty file called index (see below)
    <My Calendar Name>/  # each calendar will create it's own folder
        cal.json         # The cal.json contains metadata of a calendar (url, name, whether it's the default calendar, etc.)
        Event1.ics       # All events are saved as <UID>.ics files inside the calendars folder
        Event2.ics       
        ...              
```

## Event files

Event files do not only contain the ICS content but have 3 preceeding lines that must be parsed manually:

```
<url of the calendar>
<url of the event>
<etag of the event>
<changed state: "yes" | "no" | "deleted">
<empty line>

BEGIN:VCALENDAR
<ics content>
END:VCALENDAR

```

## index file

Karlender keeps an index of the current events. The index file is always empty. Karlender will run a re-index if this file has a newer timestamp than the last known of Karlender. That means you can create a new event by placing the correct `event.ics` file in karlender directory and `touch index`.
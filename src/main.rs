use domain::{get_app_dir, Calendar, Event};
use glib::prelude::Cast;
use gtk::{
    gdk::Display,
    gio::ApplicationFlags,
    prelude::{ApplicationExt, ApplicationExtManual, FileExt, SettingsExt},
    prelude::{DialogExt, FileChooserExt, GtkApplicationExt, GtkWindowExt, WidgetExt},
    CssProvider,
};
use libadwaita as adw;
use std::path::PathBuf;
use store::store;
use ui::editor_page;

mod domain;
mod logging;
mod store;

pub(crate) mod ui;

#[allow(clippy::single_component_path_imports)]
pub(crate) use grx;

use crate::store::Action;

const APP_ID: &str = "codes.loers.Karlender";
const APP_NAME: &str = "Events";

fn main() {
    let version = env!("CARGO_PKG_VERSION");

    logging::init();
    gtk::init().unwrap();
    init_gettext("karlender");

    async_std::task::block_on(async {
        let app_dir = get_app_dir();
        if !app_dir.exists() {
            if let Err(e) = async_std::fs::create_dir_all(&app_dir).await {
                log::error!("Failed to create app dir: {e}")
            }
        }

        let app = adw::Application::builder()
            .flags(ApplicationFlags::HANDLES_OPEN)
            .application_id(APP_ID)
            .build();

        let settings = gtk::gio::Settings::new(APP_ID);

        let store = store::store();
        store::append_middlewares(&store);

        let moved_app = app.clone();
        let moved_settings = settings.clone();

        let (send_import, recv_import) = async_channel::unbounded();

        glib::MainContext::default().spawn_local(async move {
            loop {
                if let Ok(path) = recv_import.recv().await {
                    if let Err(e) = import_ics_file(path) {
                        log::error!("Failed to import ics file: {e}");
                    }
                }
            }
        });

        let send = send_import.clone();
        let chooser_app = app.clone();
        store.bind_gtk(&app, move |a, s| {
            store::reduce(a, s);
            if a == &Action::Quit {
                if let Some(w) = moved_app.windows().first() {
                    if let Err(e) = moved_settings.set_int("window-width", w.width()) {
                        log::error!("Failed to save window width: {e}");
                    }
                    if let Err(e) = moved_settings.set_int("window-height", w.height()) {
                        log::error!("Failed to save window height: {e}");
                    }
                }
                moved_app.quit();
            }
            if a == &Action::Open {
                let send_import = send.clone();
                let chooser = gtk::FileChooserDialog::new(
                    Some("Import ICS File"),
                    Some(chooser_app.windows().first().unwrap()),
                    gtk::FileChooserAction::Open,
                    &[
                        ("Cancel", gtk::ResponseType::Cancel),
                        ("Import", gtk::ResponseType::Accept),
                    ],
                );
                chooser.set_select_multiple(false);

                chooser.connect_response(move |d, r| {
                    if r == gtk::ResponseType::Cancel {
                        d.close();
                    } else if r == gtk::ResponseType::Accept {
                        let files = d
                            .files()
                            .into_iter()
                            .flatten()
                            .filter_map(|f| f.downcast::<gtk::gio::File>().ok())
                            .collect();
                        import_files(files, send_import.clone());
                        d.close()
                    }
                });
                chooser.show();
            }
        });

        load_css();
        load_resources();

        app.connect_open(move |_app, files, _s| {
            import_files(files.to_vec(), send_import.clone());
        });

        app.connect_activate(move |app| {
            let w = 0.max(settings.int("window-width"));
            let h = 0.max(settings.int("window-height"));
            if let Some(window) = app.windows().first() {
                window.set_default_width(w);
                window.set_default_height(h);

                window.show();
            }
        });

        app.connect_startup(|app| {
            // grx::gstore_debug::gstore_debug_window(app);

            let root = ui::root::root();
            let inner = root.inner();
            let w: &gtk::Widget = inner.downcast_ref().unwrap();
            let window = adw::ApplicationWindow::builder()
                .application(app)
                .title(APP_NAME)
                .content(w)
                .hide_on_close(true)
                .build();
            if version.contains("beta") {
                window.add_css_class("devel");
            }
            window.connect_close_request(|_| {
                store::store().dispatch(crate::store::sync::Action::Syncing {
                    report_errors: false,
                });
                glib::Propagation::Proceed
            });
            window.show();
            store::store().dispatch(store::Action::Init);
        });

        app.run();
    });
}

fn import_files(files: Vec<gtk::gio::File>, send_import: async_channel::Sender<PathBuf>) {
    for file in files {
        if let Some(path) = file
            .downcast::<gtk::gio::File>()
            .ok()
            .and_then(|f| f.path())
        {
            if let Err(e) = send_import.send_blocking(path) {
                log::error!("Failed to import file: {e}");
            }
        }
    }
}

fn get_default_calendar() -> std::io::Result<Option<Calendar>> {
    async_std::task::block_on(async {
        let calendars = Calendar::local().await?;
        for calendar in calendars {
            if calendar.is_default() {
                return Ok(Some(calendar));
            }
        }
        Ok(None)
    })
}

fn import_ics_file(path: PathBuf) -> std::io::Result<()> {
    let ics = std::fs::read_to_string(&path)?;
    if let Some(calendar) = get_default_calendar()? {
        let cal_url = calendar.url();
        if let Some((ical, uid)) = minicaldav::parse_ical(&ics)
            .ok()
            .as_ref()
            .and_then(|ical| ical.get("VEVENT").map(|ev| (ical, ev)))
            .and_then(|(ical, ev)| {
                ev.properties.iter().find_map(|p| {
                    if p.name == "UID" {
                        Some((ical, &p.value))
                    } else {
                        None
                    }
                })
            })
        {
            if let Ok(event_url) = cal_url.join(&format!("{}.ics", uid)) {
                let mut data = Vec::new();
                data.append(&mut format!("{}\n", cal_url.as_str()).as_bytes().to_vec());
                data.append(&mut format!("{}\n", event_url.as_str()).as_bytes().to_vec());
                data.append(&mut "\n".as_bytes().to_vec());
                data.append(&mut "yes\n".as_bytes().to_vec());
                data.append(&mut "\n".as_bytes().to_vec());
                data.append(&mut ics.as_bytes().to_vec());
                // let event_path = get_app_dir()
                //     .join(CALENDARS_PATH)
                //     .join(calendar.name())
                //     .join(format!("{}.ics", uid));

                if let Some(event) = Event::new(
                    &calendar,
                    minicaldav::Event::new(None, event_url, ical.clone()),
                ) {
                    store().dispatch(crate::store::editor::Action::Edit(event));
                    store().dispatch(crate::store::ui::Action::Push(editor_page::NAME));
                    Ok(())
                } else {
                    Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!("Can not create Event! {:?}", ics),
                    ))
                }
                // if let Err(e) = std::fs::write(event_path, data) {
                //     log::error!("Failed to write to ics file: {e}");
                // }
                // store().dispatch(crate::store::index::Action::Index);
                // store().dispatch(crate::store::sync::Action::Syncing { report_errors: true });
            } else {
                Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    format!("Can not create URL for event {:?}!", path.as_os_str()),
                ))
            }
        } else {
            Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                format!("Event {:?} has no UID!", path.as_os_str()),
            ))
        }
    } else {
        Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "No default calendar set!".to_string(),
        ))
    }
}

fn load_css() {
    // Load the CSS file and add it to the provider
    let provider = CssProvider::new();
    provider.load_from_data(include_str!("styles.css"));

    // Add the provider to the default screen
    let display = Display::default().expect("Could not connect to a display.");
    gtk::style_context_add_provider_for_display(
        &display,
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );
}

pub(crate) fn load_resources() {
    let resource_bytes = include_bytes!("../target/gra-gen/compiled.gresource");
    let res = gtk::gio::Resource::from_data(&resource_bytes.into())
        .expect("Could not load gresource file");
    gtk::gio::resources_register(&res);
}

pub(crate) fn init_gettext(domain: &str) {
    let textdomain = match std::env::var("TEXT_DOMAIN") {
        Ok(path) => gettextrs::TextDomain::new(domain)
            .skip_system_data_paths()
            .push(path)
            .init(),
        Err(_) => gettextrs::TextDomain::new(domain).init(),
    };
    match &textdomain {
        Ok(locale) => match locale {
            Some(_locale) => {
                // nothing to do
            },
            None => eprintln!("Warning: No locale was set! Probably /usr/share/locale/*/LC_MESSAGES does not contain a .mo file.")
        },
        Err(e) => match e {
            gettextrs::TextDomainError::InvalidLocale(locale) => eprintln!("Warning: Invalid locale {:?}", locale),
            gettextrs::TextDomainError::TranslationNotFound(locale) => {
                log::warn!("Could not find messages for locale {:?}, text domain: {:?}, TEXT_DOMAIN: {:?}", 
                    locale,
                    textdomain,
                    std::env::var("TEXT_DOMAIN"))
            },
            e => {
                log::error!("Text domain error: {}", e);
            }
        }
    };
}

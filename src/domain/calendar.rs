use std::{
    collections::HashMap,
    path::{Path, PathBuf},
    str::FromStr,
};

use async_std::{
    fs::File,
    io::{ReadExt, WriteExt},
    stream::StreamExt,
};
use serde::{Deserialize, Serialize};
use url::Url;
use uuid::Uuid;

use super::{get_app_dir, Event, Login, CALENDARS_PATH, CAL_FILE_NAME};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) enum RemoteOrLocal {
    Remote(minicaldav::Calendar),
    Local(LocalCalendar),
}

impl PartialEq for RemoteOrLocal {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Remote(l0), Self::Remote(r0)) => l0.url() == r0.url(),
            (Self::Local(l0), Self::Local(r0)) => l0 == r0,
            _ => false,
        }
    }
}

impl RemoteOrLocal {
    pub(crate) fn name(&self) -> &String {
        match self {
            RemoteOrLocal::Remote(r) => r.name(),
            RemoteOrLocal::Local(l) => &l.name,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub(crate) struct LocalCalendar {
    name: String,
    color: String,
    url: Url,
    events: HashMap<String, Event>,
}
impl LocalCalendar {
    pub(crate) fn new(name: &str) -> Self {
        Self {
            name: name.into(),
            color: "#EFEFEF".into(),
            url: Url::from_str(&format!("http://karlender.local/{}", Uuid::new_v4())).unwrap(),
            events: Default::default(),
        }
    }

    fn color(&self) -> Option<&String> {
        Some(&self.color)
    }

    fn url(&self) -> &Url {
        &self.url
    }

    pub(crate) fn set_name(&mut self, name: &str) {
        self.name = name.into();
    }

    pub(crate) fn set_color(&mut self, color: &str) {
        self.color = color.into()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct Calendar {
    login_url: Option<Url>,
    local_path: PathBuf,
    default: bool,
    remote: bool,
    removed: bool,
    enabled: bool,
    cal: RemoteOrLocal,
}

impl PartialEq for Calendar {
    fn eq(&self, other: &Self) -> bool {
        self.local_path == other.local_path && self.cal == other.cal
    }
}

impl Eq for Calendar {}

impl Calendar {
    pub(crate) fn new(login: &Login, calendar: minicaldav::Calendar) -> Self {
        let name = calendar.name();
        let local_path = get_app_dir().join(CALENDARS_PATH).join(name);
        Self {
            login_url: Some(login.url.clone()),
            local_path,
            cal: RemoteOrLocal::Remote(calendar),
            remote: true,
            enabled: true,
            removed: false,
            default: false,
        }
    }

    pub(crate) fn new_local(name: &str) -> Self {
        let local_path = get_app_dir().join(CALENDARS_PATH).join(name);
        Self {
            login_url: None,
            local_path,
            cal: RemoteOrLocal::Local(LocalCalendar::new(name)),
            remote: false,
            enabled: true,
            removed: false,
            default: false,
        }
    }

    pub(crate) fn login_url(&self) -> Option<&Url> {
        self.login_url.as_ref()
    }

    pub(crate) fn name(&self) -> &str {
        self.cal.name()
    }

    pub(crate) fn color(&self) -> Option<&String> {
        match &self.cal {
            RemoteOrLocal::Remote(r) => r.color(),
            RemoteOrLocal::Local(l) => l.color(),
        }
    }

    pub(crate) fn removed(&self) -> bool {
        self.removed
    }

    pub(crate) fn local_path(&self) -> &Path {
        self.local_path.as_path()
    }

    pub(crate) fn url(&self) -> &Url {
        match &self.cal {
            RemoteOrLocal::Remote(r) => r.url(),
            RemoteOrLocal::Local(l) => l.url(),
        }
    }

    pub(crate) fn inner_mut(&mut self) -> &mut RemoteOrLocal {
        &mut self.cal
    }

    pub(crate) fn is_remote(&self) -> bool {
        match self.cal {
            RemoteOrLocal::Remote(_) => true,
            RemoteOrLocal::Local(_) => false,
        }
    }

    pub(crate) fn cal(&self) -> Option<&minicaldav::Calendar> {
        match &self.cal {
            RemoteOrLocal::Remote(rem) => Some(rem),
            RemoteOrLocal::Local(_) => None,
        }
    }

    pub(crate) fn is_default(&self) -> bool {
        self.default
    }

    pub(crate) fn set_default(&mut self, default: bool) {
        self.default = default;
    }

    pub(crate) fn remove(&mut self) {
        self.removed = true;
    }

    pub(crate) fn enabled(&self) -> bool {
        self.enabled
    }

    pub(crate) fn set_enabled(&mut self, enabled: bool) {
        self.enabled = enabled;
    }
}

// persistence

impl Calendar {
    pub(crate) async fn persist(&self) -> async_std::io::Result<()> {
        let calendars_path = get_app_dir().join(CALENDARS_PATH);
        if !calendars_path.exists() {
            async_std::fs::create_dir(calendars_path).await?;
        }
        let local_path = &self.local_path;
        if !local_path.exists() {
            async_std::fs::create_dir(local_path).await?;
        }
        let cal_path = local_path.join(CAL_FILE_NAME);

        let mut cal_file = File::create(cal_path).await?;

        let json = serde_json::to_string(&self).map_err(|e| {
            async_std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                format!("Could not serialize calendar to json: {e}"),
            )
        })?;

        cal_file.write_all(json.as_bytes()).await?;

        Ok(())
    }

    pub(crate) async fn try_remove(&self) {
        let calendars_path = get_app_dir().join(CALENDARS_PATH);
        let archive_path = calendars_path.join(Uuid::new_v4().to_string());
        if let Err(e) = async_std::fs::rename(&self.local_path, archive_path).await {
            log::error!(
                "Failed to archive calendar {:?}: {e}",
                self.local_path().as_os_str()
            );
        }
    }

    pub(crate) async fn local() -> std::io::Result<Vec<Calendar>> {
        let calendars_path = get_app_dir().join(CALENDARS_PATH);
        if !calendars_path.exists() {
            return Ok(Vec::new());
        }
        let mut entries = async_std::fs::read_dir(&calendars_path).await?;
        let mut calendars = Vec::new();
        while let Some(res) = entries.next().await {
            let entry = res?;
            if entry.file_type().await?.is_dir() {
                let cal_file = entry.path().join(CAL_FILE_NAME);
                let mut f = File::open(&cal_file).await?;
                let mut s = String::new();
                f.read_to_string(&mut s).await?;
                let calendar: Calendar = serde_json::from_str(&s).map_err(move |e| {
                    std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!("Failed to parse cal file {:?}: {}", cal_file.as_os_str(), e),
                    )
                })?;
                if !calendar.removed() {
                    calendars.push(calendar);
                }
            }
        }
        Ok(calendars)
    }
}

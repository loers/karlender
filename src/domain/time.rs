use serde::{Deserialize, Serialize};
use std::{
    backtrace::Backtrace,
    collections::HashMap,
    convert::TryFrom,
    fs::read_link,
    ops::{Add, Sub},
    process::Command,
};
use time::{format_description::well_known::Rfc3339, macros::format_description, UtcOffset};

const _PREFIXES: [&str; 15] = [
    "Africa",
    "America",
    "Antarctica",
    "Arctic",
    "Asia",
    "Atlantic",
    "Australia",
    "Brazil",
    "Canada",
    "Chile",
    "Europe",
    "Indian",
    "Mexico",
    "Pacific",
    "US",
];

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct KarlenderDate {
    inner: time::Date,
}

impl KarlenderDate {
    pub fn year(&self) -> i32 {
        self.inner.year()
    }

    pub fn iso_week(&self) -> u8 {
        self.inner.iso_week()
    }

    pub fn day(&self) -> u8 {
        self.inner.day()
    }

    pub fn month(&self) -> u8 {
        self.inner.month().into()
    }

    pub fn month0(&self) -> u8 {
        self.month() - 1
    }

    pub fn with_day(&self, day: u8) -> Result<Self, String> {
        if let Ok(inner) =
            time::Date::from_calendar_date(self.inner.year(), self.inner.month(), day)
        {
            Ok(Self { inner })
        } else {
            Err("Wrong day".into())
        }
    }

    pub fn with_day0(&self, day: u8) -> Result<Self, String> {
        self.with_day(day + 1)
    }

    pub fn and_hms(
        &self,
        hour: u8,
        minute: u8,
        second: u8,
        tz: Option<&KarlenderTimezone>,
    ) -> KarlenderDateTime {
        if let Some(tz) = tz {
            KarlenderDateTime {
                inner: self
                    .inner
                    .with_hms(hour, minute, second)
                    .unwrap_or_else(|_| panic!("Wrong date input: {}.{}.{}", hour, minute, second))
                    .assume_offset(UtcOffset::from_whole_seconds(tz.offset).unwrap()),
            }
        } else {
            KarlenderDateTime {
                inner: self
                    .inner
                    .with_hms(hour, minute, second)
                    .unwrap_or_else(|_| panic!("Wrong date input: {}.{}.{}", hour, minute, second))
                    .assume_utc(),
            }
        }
    }

    pub fn weekday(&self) -> Weekday {
        Weekday(self.inner.weekday())
    }

    pub fn format(&self, f: DatetimeFormat) -> String {
        format(self.inner.with_hms(0, 0, 0).unwrap().assume_utc(), f)
    }

    pub fn from_ymd(year: i32, month: u8, day: u8) -> Self {
        let date = time::Date::from_calendar_date(
            year,
            time::Month::try_from(month).unwrap_or_else(|_| panic!("Wrong month value: {}", month)),
            day,
        )
        .unwrap_or_else(|_| panic!("Wrong date input: {}.{}.{}", year, month, day));
        Self { inner: date }
    }

    pub fn signed_duration_since(&self, other: Self) -> KarlenderDateTimeDuration {
        let a = self.inner;
        let b = other.inner;
        let d = a - b;
        KarlenderDateTimeDuration(d)
    }
}

impl std::fmt::Debug for KarlenderDate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "KarlenderDateTime({})",
            self.format(DatetimeFormat::Debug)
        )
    }
}

impl Add<KarlenderDateTimeDuration> for KarlenderDate {
    type Output = Self;

    fn add(self, rhs: KarlenderDateTimeDuration) -> Self::Output {
        let inner = self.inner + rhs.0;
        Self { inner }
    }
}

impl Sub<KarlenderDateTimeDuration> for KarlenderDate {
    type Output = Self;

    fn sub(self, rhs: KarlenderDateTimeDuration) -> Self::Output {
        let inner = self.inner - rhs.0;
        Self { inner }
    }
}

pub struct Weekday(time::Weekday);

impl Weekday {
    /// number form monday: 0-6
    pub fn number_from_monday(&self) -> u8 {
        self.0.number_days_from_monday()
    }
}

impl std::fmt::Display for KarlenderDate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let format = self
            .inner
            .format(&time::format_description::well_known::Rfc3339);
        if format.is_err() {
            log::error!("{}", Backtrace::force_capture());
        }
        let s = format.unwrap();
        write!(f, "{}", s)
    }
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct KarlenderDateTime {
    inner: time::OffsetDateTime,
}

impl KarlenderDateTime {
    pub fn _naive_utc(&self) -> Self {
        Self {
            inner: time::OffsetDateTime::now_utc(),
        }
    }
    pub fn day(&self) -> u8 {
        self.inner.day()
    }
    pub fn month(&self) -> u8 {
        self.inner.month().into()
    }
    pub fn year(&self) -> i32 {
        self.inner.year()
    }
    pub fn hour(&self) -> u8 {
        self.inner.hour()
    }
    pub fn minute(&self) -> u8 {
        self.inner.minute()
    }
    pub fn _second(&self) -> u8 {
        self.inner.second()
    }
    pub fn date(&self) -> KarlenderDate {
        KarlenderDate {
            inner: self.inner.date(),
        }
    }
    pub fn format(&self, f: DatetimeFormat) -> String {
        format(self.inner, f)
    }

    pub fn _weekday(&self) -> Weekday {
        Weekday(self.inner.weekday())
    }

    /// from Rfc3339
    #[allow(clippy::wrong_self_convention)]
    pub fn _from_str(string: &str) -> Result<Self, String> {
        time::OffsetDateTime::parse(string, &Rfc3339)
            .map(|inner| Self { inner })
            .map_err(|e| e.to_string())
    }

    #[allow(clippy::wrong_self_convention)]
    pub fn from_zdump(string: &str) -> Result<Self, String> {
        time::PrimitiveDateTime::parse(string, DatetimeFormat::Zdump.as_format())
            .map(|inner| Self {
                inner: inner.assume_utc(),
            })
            .map_err(|e| e.to_string())
    }

    pub fn from_iso8601(
        string: &str,
        timezone: Option<&KarlenderTimezone>,
    ) -> Result<(Self, bool), String> {
        let mut result =
            time::PrimitiveDateTime::parse(string, DatetimeFormat::ISO8601.as_format())
                .map_err(|e| e.to_string())
                .map(|inner| {
                    if let Some(tz) = timezone {
                        Self {
                            inner: inner.assume_offset(
                                time::UtcOffset::from_whole_seconds(tz.offset).unwrap(),
                            ),
                        }
                    } else {
                        Self {
                            inner: inner.assume_utc(),
                        }
                    }
                })
                .map(|d| (d, false));
        if result.is_err() {
            result = time::PrimitiveDateTime::parse(string, DatetimeFormat::ISO8601Z.as_format())
                .map(|inner| Self {
                    inner: inner.assume_utc(),
                })
                .map_err(|e| e.to_string())
                .map(|d| (d, false));
        }
        if result.is_err() {
            result = time::Date::parse(string, DatetimeFormat::ISO8601DATE.as_format())
                .map(|inner| Self {
                    inner: inner.with_hms(0, 0, 0).unwrap().assume_utc(),
                })
                .map_err(|e| e.to_string())
                .map(|d| (d, true));
        }
        result
    }
    pub fn signed_duration_since(&self, other: Self) -> KarlenderDateTimeDuration {
        let a = self.inner;
        let b = other.inner;
        let d = a - b;
        KarlenderDateTimeDuration(d)
    }
}

impl std::fmt::Debug for KarlenderDateTime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "KarlenderDateTime({})",
            self.format(DatetimeFormat::Debug)
        )
    }
}

fn format(datetime: time::OffsetDateTime, format: DatetimeFormat) -> String {
    let fmt = format.as_format();
    datetime
        .format(&fmt)
        .unwrap_or_else(|_| "invalid".to_string())
}

impl Add<KarlenderDateTimeDuration> for KarlenderDateTime {
    type Output = Self;

    fn add(self, rhs: KarlenderDateTimeDuration) -> Self::Output {
        let inner = self.inner + rhs.0;
        Self { inner }
    }
}

impl Sub<KarlenderDateTimeDuration> for KarlenderDateTime {
    type Output = Self;

    fn sub(self, rhs: KarlenderDateTimeDuration) -> Self::Output {
        let inner = self.inner - rhs.0;
        Self { inner }
    }
}

impl std::cmp::PartialOrd for KarlenderDateTime {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.inner.cmp(&other.inner))
    }
}

impl std::cmp::Ord for KarlenderDateTime {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.inner.cmp(&other.inner)
    }
}

impl std::fmt::Display for KarlenderDateTime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Ok(s) = self
            .inner
            .format(&time::format_description::well_known::Rfc3339)
        {
            write!(f, "{}", s)
        } else {
            write!(f, "{:?}", self)
        }
    }
}

type DstCache =
    once_cell::sync::Lazy<std::sync::Mutex<HashMap<(String, i32), Vec<(KarlenderDateTime, i32)>>>>;

static DST_CACHE: DstCache = once_cell::sync::Lazy::new(Default::default);

static LOCAL_TZ_CACHE: once_cell::sync::Lazy<std::sync::Mutex<Option<KarlenderTimezone>>> =
    once_cell::sync::Lazy::new(Default::default);

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct KarlenderTimezone {
    name: String,
    /// Offset in whole seconds
    offset: i32,
}

impl KarlenderTimezone {
    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn _offset(&self) -> i32 {
        self.offset
    }

    fn get_utc_offset(timezone: &str) -> i32 {
        if let Ok(now) = glib::DateTime::now_local() {
            let tz = glib::TimeZone::new(Some(timezone));
            let interval = tz.find_interval(glib::TimeType::Universal, now.to_unix());
            tz.offset(interval)
        } else {
            0
        }
    }

    pub fn _list() -> std::io::Result<Vec<Self>> {
        let path = "/usr/share/zoneinfo/";
        let mut list = Vec::new();
        for prefix_entry in std::fs::read_dir(path)? {
            let prefix_entry = prefix_entry?;
            let prefix = prefix_entry.path().to_str().unwrap().replace(path, "");
            if !_PREFIXES.contains(&prefix.as_str()) {
                continue;
            }
            for city_entry in std::fs::read_dir(prefix_entry.path())? {
                let city_entry = city_entry?;
                let city = city_entry.file_name().to_string_lossy().to_string();

                let name = format!("{}/{}", prefix, city);
                let offset = Self::get_utc_offset(&name);
                list.push(Self { name, offset })
            }
        }
        Ok(list)
    }

    pub fn get_offset(&self, created_at: &KarlenderDateTime) -> i32 {
        if self.offset == 0 {
            return 0;
        }
        if created_at.year() == 1970 {
            return self.offset;
        }
        let year = KarlenderTimezone::local().today().year();
        let key = (self.name().clone(), year);

        let offset = match DST_CACHE.lock() {
            Ok(mut map) => match map.get(&key) {
                Some(dst_breakpoints) => {
                    self.get_offset_from_dst_brekapoints(dst_breakpoints, created_at)
                }
                None => match Command::new("zdump").arg("-V").arg(&self.name).output() {
                    Ok(output) => match String::from_utf8(output.stdout) {
                        Ok(output) => {
                            let dst_breakpoints = self.get_dst_breakpoints(output, created_at);
                            let offset =
                                self.get_offset_from_dst_brekapoints(&dst_breakpoints, created_at);
                            map.insert(key, dst_breakpoints);
                            offset
                        }
                        Err(_) => todo!(),
                    },
                    Err(e) => {
                        log::error!("Failed to run zdump to get DST support: {e}");
                        return self.offset;
                    }
                },
            },
            Err(e) => {
                log::error!("Failed to run access zdump cache: {e}");
                return self.offset;
            }
        };

        offset
    }

    fn get_offset_from_dst_brekapoints(
        &self,
        dst_breakpoints: &[(KarlenderDateTime, i32)],
        created_at: &KarlenderDateTime,
    ) -> i32 {
        let mut offset = self.offset;
        if dst_breakpoints.len() == 4 {
            // handle DST
            if created_at < &dst_breakpoints[0].0 {
                offset = dst_breakpoints[0].1;
            }
            if created_at >= &dst_breakpoints[1].0 && created_at < &dst_breakpoints[2].0 {
                offset = dst_breakpoints[1].1;
            }
            if created_at > &dst_breakpoints[3].0 {
                offset = dst_breakpoints[3].1;
            }
        } else {
            log::error!(
                "Failed to find 4 entries in zdump for {}, {}. Got {}.",
                self.name(),
                created_at,
                dst_breakpoints.len(),
            );
        }
        offset
    }

    fn get_dst_breakpoints(
        &self,
        output: String,
        created_at: &KarlenderDateTime,
    ) -> Vec<(KarlenderDateTime, i32)> {
        let mut dst_breakpoints = Vec::new();
        for line in output.split('\n') {
            let line = line.replace(&self.name, "");
            let line = line.trim().replace("  ", " 0");
            if let Some((d, rest)) = line.split_once(" = ") {
                let d = d.trim();
                if let Some((_, offset)) = rest.split_once("gmtoff=") {
                    if let Ok(offset) = offset.parse::<i32>() {
                        match KarlenderDateTime::from_zdump(d) {
                            Ok(d) => {
                                if d.year() == created_at.year() {
                                    dst_breakpoints.push((d, offset));
                                }
                            }
                            Err(e) => log::error!("Failed to parse date in zdump for {:?}: {e}", d),
                        }
                    } else {
                        log::error!("No offset")
                    }
                } else {
                    log::error!("no 'gmtoff=' to split on")
                }
            }
        }
        dst_breakpoints
    }

    pub fn local() -> Self {
        if let Ok(tz) = LOCAL_TZ_CACHE.lock() {
            if let Some(tz) = tz.as_ref() {
                return tz.clone();
            }
        }
        if let Ok(timezone_path) = read_link("/etc/localtime") {
            if let Some((_, zone)) = timezone_path
                .as_os_str()
                .to_str()
                .and_then(|s| s.split_once("zoneinfo"))
            {
                let timezone = &zone[1..];

                let tz = KarlenderTimezone {
                    name: timezone.into(),
                    offset: Self::get_utc_offset(timezone),
                };
                if let Ok(mut lock) = LOCAL_TZ_CACHE.lock() {
                    *lock = Some(tz);
                }
            }
        }
        Self::utc()
    }

    pub fn from_str(timezone: &str) -> Self {
        let check_for_utc_info = regex::Regex::new(r"UTC([-+])(\d\d):(\d\d)").unwrap();
        if let Some(captures) = check_for_utc_info.captures(timezone) {
            let positive = captures.get(1).unwrap().as_str() == "+";
            let hours = captures.get(2).unwrap().as_str().parse::<i32>().unwrap();
            let minutes = captures.get(3).unwrap().as_str().parse::<i32>().unwrap();
            let mut offset = hours * 60 * 60 + minutes * 60;
            if !positive {
                offset = -offset;
            }
            KarlenderTimezone {
                name: timezone.into(),
                offset,
            }
        } else {
            KarlenderTimezone {
                name: timezone.into(),
                offset: Self::get_utc_offset(timezone),
            }
        }
    }

    pub fn utc() -> Self {
        Self {
            name: "UTC".into(),
            offset: 0,
        }
    }

    pub fn today(&self) -> KarlenderDate {
        self.now().date()
    }

    pub fn now(&self) -> KarlenderDateTime {
        self.from_utc(&KarlenderDateTime {
            inner: time::OffsetDateTime::now_utc(),
        })
    }

    #[allow(clippy::wrong_self_convention)]
    pub fn from_utc(&self, datetime: &KarlenderDateTime) -> KarlenderDateTime {
        KarlenderDateTime {
            inner: datetime
                .inner
                .to_offset(time::UtcOffset::from_whole_seconds(self.offset).unwrap()),
        }
    }

    #[allow(clippy::wrong_self_convention)]
    pub fn from_utc_created_at(
        &self,
        datetime: &KarlenderDateTime,
        created_at: &KarlenderDateTime,
    ) -> KarlenderDateTime {
        let offset = self.get_offset(created_at);
        KarlenderDateTime {
            inner: datetime
                .inner
                .to_offset(time::UtcOffset::from_whole_seconds(offset).unwrap()),
        }
    }

    /// Returns a tz with offset this timezone has (or had) at the given date (In case there are DST).
    pub fn to_tz_at(&self, datetime: &KarlenderDateTime) -> KarlenderTimezone {
        if self.offset == 0 {
            return self.clone();
        }
        let offset = self.get_offset(datetime);
        Self {
            offset,
            name: self.name.clone(),
        }
    }

    pub fn to_utc(&self, datetime: &KarlenderDateTime) -> KarlenderDateTime {
        KarlenderDateTime {
            inner: datetime
                .inner
                .to_offset(time::UtcOffset::from_whole_seconds(0).unwrap()),
        }
    }
}

#[derive(Debug, Clone)]
pub struct KarlenderDateTimeDuration(time::Duration);

impl KarlenderDateTimeDuration {
    pub fn days(num: i64) -> Self {
        Self(time::Duration::days(num))
    }
    pub fn weeks(num: i64) -> Self {
        Self(time::Duration::weeks(num))
    }

    pub fn minutes(num: i64) -> Self {
        Self(time::Duration::minutes(num))
    }

    pub fn num_days(&self) -> i64 {
        self.0.whole_days()
    }

    pub fn num_minutes(&self) -> i64 {
        self.0.whole_minutes()
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub enum DatetimeFormat {
    Debug,
    ISO8601,
    ISO8601Z,
    ISO8601DATE,
    Year,
    DateBottomUpAndTime24,
    DateBottomUpAndTime12,
    DateTopDownAndTime24,
    DateTopDownAndTime12,
    WrongTime,
    Wrong,
    DateBottomUp,
    DateTopDown,
    Time24,
    Time12,
    Zdump,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum FormatKey {
    BottomUp12,
    BottomUp24,
    TopDown12,
    TopDown24,
    Wrong,
}

impl Default for FormatKey {
    fn default() -> Self {
        Self::BottomUp24
    }
}

impl FormatKey {
    pub fn from_str(s: &str) -> Option<Self> {
        match s {
            "bu12" => Some(Self::BottomUp12),
            "bu24" => Some(Self::BottomUp24),
            "td12" => Some(Self::TopDown12),
            "td24" => Some(Self::TopDown24),
            "wrong" => Some(Self::Wrong),
            _ => None,
        }
    }

    pub fn get_time_format(&self) -> DatetimeFormat {
        match self {
            FormatKey::BottomUp12 => DatetimeFormat::DateBottomUpAndTime12,
            FormatKey::BottomUp24 => DatetimeFormat::DateBottomUpAndTime24,
            FormatKey::TopDown12 => DatetimeFormat::DateTopDownAndTime12,
            FormatKey::TopDown24 => DatetimeFormat::DateTopDownAndTime24,
            FormatKey::Wrong => DatetimeFormat::WrongTime,
        }
    }
    pub fn _get_date_format(&self) -> DatetimeFormat {
        match self {
            FormatKey::BottomUp12 => DatetimeFormat::DateBottomUp,
            FormatKey::BottomUp24 => DatetimeFormat::DateBottomUp,
            FormatKey::TopDown12 => DatetimeFormat::DateTopDown,
            FormatKey::TopDown24 => DatetimeFormat::DateTopDown,
            FormatKey::Wrong => DatetimeFormat::Wrong,
        }
    }

    pub fn as_str(&self) -> &str {
        match self {
            FormatKey::BottomUp12 => "bu12",
            FormatKey::BottomUp24 => "bu24",
            FormatKey::TopDown12 => "td12",
            FormatKey::TopDown24 => "td24",
            FormatKey::Wrong => "wrong",
        }
    }
}

impl Default for DatetimeFormat {
    fn default() -> Self {
        Self::ISO8601
    }
}

impl DatetimeFormat {
    fn as_format<'a>(&self) -> &'a [time::format_description::FormatItem<'a>] {
        match self {
            DatetimeFormat::Zdump => {
                format_description!(
                    "[weekday repr:short] [month repr:short] [day] [hour repr:24]:[minute]:[second] [year] UT"
                )
            }
            DatetimeFormat::Debug => {
                format_description!("[year].[month].[day] [hour repr:24]:[minute]:[second] - [offset_hour]:[offset_minute]")
            }
            DatetimeFormat::ISO8601 => {
                format_description!("[year][month][day]T[hour repr:24][minute][second]")
            }
            DatetimeFormat::ISO8601Z => {
                format_description!("[year][month][day]T[hour repr:24][minute][second]Z")
            }
            DatetimeFormat::ISO8601DATE => {
                format_description!("[year][month][day]")
            }
            DatetimeFormat::Year => format_description!("[year]"),
            DatetimeFormat::DateBottomUpAndTime24 => {
                format_description!("[day].[month].[year] [hour repr:24]:[minute]")
            }
            DatetimeFormat::DateBottomUpAndTime12 => {
                format_description!("[day].[month].[year] [hour repr:12]:[minute]")
            }
            DatetimeFormat::DateTopDownAndTime24 => {
                format_description!("[year].[month].[day] [hour repr:24]:[minute]")
            }
            DatetimeFormat::DateTopDownAndTime12 => {
                format_description!("[year].[month].[day] [hour repr:12]:[minute]")
            }
            DatetimeFormat::WrongTime => {
                format_description!("[month].[day].[year] [hour repr:12]:[minute]")
            }
            DatetimeFormat::Wrong => {
                format_description!("[month].[day].[year]")
            }
            DatetimeFormat::DateBottomUp => format_description!("[day].[month].[year]"),
            DatetimeFormat::DateTopDown => format_description!("[year].[month].[day]"),
            DatetimeFormat::Time24 => format_description!("[hour repr:24]:[minute]"),
            DatetimeFormat::Time12 => format_description!("[hour repr:12]:[minute]"),
        }
    }
}

pub fn get_month_by_delta(date: &KarlenderDate, month_delta: i64) -> KarlenderDate {
    let current_month = date.month0() as i64;
    let month_sum = current_month + month_delta;
    let mut new_month = month_sum.rem_euclid(12);
    let year_delta = (month_sum - new_month) / 12;
    new_month += 1;
    KarlenderDate::from_ymd(date.year() + year_delta as i32, new_month as u8, 1)
}

pub fn get_month_len(date: &KarlenderDate) -> u8 {
    let year: i32 = date.year();
    let month: u8 = date.month();
    let first_day_next_month = get_month_by_delta(date, 1);
    let duration =
        first_day_next_month.signed_duration_since(KarlenderDate::from_ymd(year, month, 1));
    duration.num_days() as u8
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_parse_iso8601() {
        let date =
            KarlenderDateTime::from_iso8601("20220102T030405", Some(&KarlenderTimezone::utc()));
        assert!(date.is_ok());
        let date = date.unwrap().0;
        assert_eq!(date.year(), 2022);
        assert_eq!(date.month(), 1);
        assert_eq!(date.day(), 2);
        assert_eq!(date.hour(), 3);
        assert_eq!(date.minute(), 4);
    }

    #[test]
    fn test_timezone_mapping() {
        let timezone = KarlenderTimezone {
            name: "Europe/Berlin".to_string(),
            offset: 7200,
        };
        let date = KarlenderDateTime::from_iso8601("20220102T153000", Some(&timezone))
            .unwrap()
            .0;

        assert_eq!(date.year(), 2022);
        assert_eq!(date.month(), 1);
        assert_eq!(date.day(), 2);
        assert_eq!(date.hour(), 15);
        assert_eq!(date.minute(), 30);

        assert_eq!(timezone.to_utc(&date).year(), 2022);
        assert_eq!(timezone.to_utc(&date).month(), 1);
        assert_eq!(timezone.to_utc(&date).day(), 2);
        assert_eq!(timezone.to_utc(&date).hour(), 13);
        assert_eq!(timezone.to_utc(&date).minute(), 30);
    }

    #[test]
    fn test_timezone_parse_pacific_formatted() {
        let timezone = KarlenderTimezone::from_str("(UTC-08:00) Pacific Time (US & Canada)");
        assert_eq!(timezone.offset, -28800)
    }

    #[test]
    fn test_datetime_parse_zdump() {
        let d = KarlenderDateTime::from_zdump("Sun Mar 26 00:59:59 2023 UT").unwrap();
        assert_eq!(d.day(), 26);
        assert_eq!(d.year(), 2023);
        assert_eq!(d.month(), 3);
        let d = KarlenderDateTime::from_zdump("Mon Apr 01 00:59:59 1940 UT").unwrap();
        assert_eq!(d.day(), 1);
        assert_eq!(d.month(), 4);
        assert_eq!(d.year(), 1940);
    }

    #[test]
    fn test_get_correct_offset_for_dst() {
        let tz = KarlenderTimezone::from_str("Europe/Berlin");
        let created_pre = KarlenderDate::from_ymd(2023, 03, 25).and_hms(12, 00, 00, Some(&tz));
        let created_post = KarlenderDate::from_ymd(2023, 03, 28).and_hms(12, 00, 00, Some(&tz));

        assert_eq!(tz.get_offset(&created_pre), 3600);
        assert_eq!(tz.get_offset(&created_post), 7200);
    }

    #[test]
    fn test_timezone_parsepositive_utc() {
        let timezone = KarlenderTimezone::from_str("asdf UTC+01:00) asda sda asd UTC+123:432");
        assert_eq!(timezone.offset, 3600)
    }

    #[test]
    fn test_weekday() {
        assert_eq!(
            KarlenderDate::from_ymd(2022, 3, 1)
                .weekday()
                .number_from_monday(),
            1
        );
        assert_eq!(
            KarlenderDate::from_ymd(2022, 4, 1)
                .weekday()
                .number_from_monday(),
            4
        );
    }

    #[test]
    fn test_format_date() {
        let dt = KarlenderDate::from_ymd(2022, 3, 8);
        assert_eq!(dt.format(DatetimeFormat::ISO8601), "20220308T000000");
        assert_eq!(dt.format(DatetimeFormat::Year), "2022");
    }

    #[test]
    fn test_get_month_len() {
        assert_eq!(get_month_len(&KarlenderDate::from_ymd(2021, 11, 1)), 30);
        assert_eq!(get_month_len(&KarlenderDate::from_ymd(2022, 2, 1)), 28);
        assert_eq!(get_month_len(&KarlenderDate::from_ymd(2022, 3, 1)), 31);
        assert_eq!(get_month_len(&KarlenderDate::from_ymd(2020, 2, 1)), 29);
    }

    #[test]
    fn test_get_month_by_delta() {
        let test_date = KarlenderDate::from_ymd(2022, 1, 19);
        let result = get_month_by_delta(&test_date, -24);
        assert_eq!(result.month0(), 0);
        assert_eq!(result.year(), 2020);

        let today_month: KarlenderDate = KarlenderDate::from_ymd(2022, 1, 1);
        for day in 0..600 {
            for month_delta in -26..26 {
                let test_date = today_month.clone() + KarlenderDateTimeDuration::days(day);

                println!(
                    "Test delta month calculation for {:?} {}",
                    test_date, month_delta
                );
                let result = get_month_by_delta(&test_date, month_delta);
                let month = result.month0();
                let year = result.year();

                println!(" => X.{}.{}", month, year);

                match (test_date.month0(), month_delta) {
                    // january
                    (0, 0) => {
                        assert_eq!(month, test_date.month0());
                        assert_eq!(year, test_date.year());
                    }
                    (0, -13) => {
                        assert_eq!(month, 11);
                        assert_eq!(year, test_date.year() - 2);
                    }
                    (0, -12) => {
                        assert_eq!(month, 0);
                        assert_eq!(year, test_date.year() - 1);
                    }
                    (0, -11) => {
                        assert_eq!(month, 1);
                        assert_eq!(year, test_date.year() - 1);
                    }
                    (0, 5) => {
                        assert_eq!(month, 5);
                        assert_eq!(year, test_date.year());
                    }
                    (0, 11) => {
                        assert_eq!(month, 11);
                        assert_eq!(year, test_date.year());
                    }
                    (0, 12) => {
                        assert_eq!(month, test_date.month0());
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (0, 13) => {
                        assert_eq!(month, test_date.month0() + 1);
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (0, 23) => {
                        assert_eq!(month, 11);
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (0, 24) => {
                        assert_eq!(month, test_date.month0());
                        assert_eq!(year, test_date.year() + 2);
                    }
                    (0, 25) => {
                        assert_eq!(month, test_date.month0() + 1);
                        assert_eq!(year, test_date.year() + 2);
                    }
                    //march
                    (2, 0) => {
                        assert_eq!(month, test_date.month0());
                        assert_eq!(year, test_date.year());
                    }
                    (2, -13) => {
                        assert_eq!(month, 1);
                        assert_eq!(year, test_date.year() - 1);
                    }
                    (2, -12) => {
                        assert_eq!(month, 2);
                        assert_eq!(year, test_date.year() - 1);
                    }
                    (2, -11) => {
                        assert_eq!(month, 3);
                        assert_eq!(year, test_date.year() - 1);
                    }
                    (2, 5) => {
                        assert_eq!(month, 7);
                        assert_eq!(year, test_date.year());
                    }
                    (2, 11) => {
                        assert_eq!(month, 1);
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (2, 12) => {
                        assert_eq!(month, test_date.month0());
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (2, 13) => {
                        assert_eq!(month, test_date.month0() + 1);
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (2, 21) => {
                        assert_eq!(month, 11);
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (2, 22) => {
                        assert_eq!(month, 0);
                        assert_eq!(year, test_date.year() + 2);
                    }
                    (2, 25) => {
                        assert_eq!(month, test_date.month0() + 1);
                        assert_eq!(year, test_date.year() + 2);
                    }

                    // //december
                    (11, 0) => {
                        assert_eq!(month, test_date.month0());
                        assert_eq!(year, test_date.year());
                    }
                    (11, -13) => {
                        assert_eq!(month, test_date.month0() - 1);
                        assert_eq!(year, test_date.year() - 1);
                    }
                    (11, -12) => {
                        assert_eq!(month, test_date.month0());
                        assert_eq!(year, test_date.year() - 1);
                    }
                    (11, -11) => {
                        assert_eq!(month, 0);
                        assert_eq!(year, test_date.year());
                    }
                    (11, 5) => {
                        assert_eq!(month, 4);
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (11, 11) => {
                        assert_eq!(month, 10);
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (11, 12) => {
                        assert_eq!(month, test_date.month0());
                        assert_eq!(year, test_date.year() + 1);
                    }
                    (11, 13) => {
                        assert_eq!(month, 0);
                        assert_eq!(year, test_date.year() + 2);
                    }
                    (11, 21) => {
                        assert_eq!(month, 8);
                        assert_eq!(year, test_date.year() + 2);
                    }
                    (11, 24) => {
                        assert_eq!(month, 11);
                        assert_eq!(year, test_date.year() + 2);
                    }
                    (11, 25) => {
                        assert_eq!(month, 0);
                        assert_eq!(year, test_date.year() + 3);
                    }
                    _ => {}
                }
            }
        }
    }
}

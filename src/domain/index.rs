use std::{
    collections::{HashMap, HashSet},
    ffi::OsString,
    time::SystemTime,
};

use async_std::path::Path;

use crate::{domain::time::KarlenderDateTimeDuration, store::dispatch_err};

use super::{
    error::to_io, get_app_dir, time::KarlenderDate, Calendar, Event, CALENDARS_PATH, INDEX_FILE,
};

#[derive(Default, Clone, Eq)]
pub(crate) struct Index {
    pub(crate) timestamp: u128,
    pub(crate) by_day: HashMap<KarlenderDate, HashSet<OsString>>,
    pub(crate) recurring: Vec<Event>,
}

impl std::fmt::Debug for Index {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Index")
            .field("timestamp", &self.timestamp)
            .field("simple", &self.by_day.len())
            .field("recurring", &self.recurring.len())
            .finish()
    }
}

impl PartialEq for Index {
    fn eq(&self, other: &Self) -> bool {
        self.timestamp == other.timestamp
    }
}

impl Index {
    pub(crate) async fn init() -> async_std::io::Result<(Self, Vec<Calendar>)> {
        let calendars_path = get_app_dir().join(CALENDARS_PATH);

        let mut by_day: HashMap<KarlenderDate, HashSet<OsString>> = HashMap::new();
        let mut recurring: Vec<Event> = Vec::new();

        let calendars = Calendar::local().await?;
        for calendar in calendars.iter() {
            if calendar.enabled() {
                Self::index_cal(calendar, &mut by_day, &mut recurring).await?;
            }
        }

        let index_file_path = calendars_path.join(INDEX_FILE);
        let f = async_std::fs::File::create(index_file_path).await?;
        let timestamp = f
            .metadata()
            .await?
            .modified()?
            .duration_since(SystemTime::UNIX_EPOCH)
            .map_err(to_io)?
            .as_millis();

        Ok((
            Self {
                timestamp,
                by_day,
                recurring,
            },
            calendars,
        ))
    }

    pub(crate) fn remove(&mut self, path: &OsString) {
        for (_, paths) in self.by_day.iter_mut() {
            paths.remove(path);
            self.timestamp += 1;
        }
    }

    pub(crate) fn get_events(&self, day: &KarlenderDate) -> Vec<Event> {
        let mut evs = Vec::new();
        if let Some(events) = self.by_day.get(day) {
            for event in events {
                let event = async_std::task::block_on(async {
                    match Event::parse(Path::new(event)).await {
                        Ok(event) => Some(event),
                        Err(e) => {
                            dispatch_err(format!("Failed to parse event: {e}"));
                            None
                        }
                    }
                });
                if let Some(ev) = event {
                    evs.push(ev)
                }
            }
        }
        for ev in &self.recurring {
            if ev.contains_date(day) {
                evs.push(ev.clone())
            }
        }
        evs
    }

    pub(crate) fn has_events(&self, day: &KarlenderDate) -> bool {
        if let Some(events) = self.by_day.get(day) {
            if !events.is_empty() {
                return true;
            }
        }
        for ev in &self.recurring {
            if ev.contains_date(day) {
                return true;
            }
        }
        false
    }

    async fn index_cal(
        calendar: &Calendar,
        by_day: &mut HashMap<KarlenderDate, HashSet<OsString>>,
        recurring: &mut Vec<Event>,
    ) -> async_std::io::Result<()> {
        let events = Event::local(calendar).await?;
        for event in events.into_iter() {
            log::trace!("Indexing event {:#?}", event);
            if event.rrule().is_some() {
                recurring.push(event);
            } else {
                let start = event.start().map_err(|e| {
                    async_std::io::Error::new(
                        async_std::io::ErrorKind::InvalidData,
                        format!(
                            "Failed to get start date from {:?}: {e}",
                            event.local_path()
                        ),
                    )
                })?;
                let end = event
                    .end()
                    .map_err(|e| {
                        async_std::io::Error::new(
                            async_std::io::ErrorKind::InvalidData,
                            format!(
                                "Failed to get start date from {:?}: {e}",
                                event.local_path()
                            ),
                        )
                    })
                    .unwrap_or_else(|_| start.clone());

                if end > start {
                    let days = end.clone().signed_duration_since(start.clone()).num_days();
                    if days == 0 {
                        push_event_at_date(by_day, start.date(), event.clone());
                    } else {
                        for i in 0..days {
                            let date = start.clone() + KarlenderDateTimeDuration::days(i);
                            push_event_at_date(by_day, date.date(), event.clone());
                        }
                    }
                } else {
                    let date = start.date();
                    push_event_at_date(by_day, date, event);
                }
            }
        }
        Ok(())
    }
}

fn push_event_at_date(
    by_day: &mut HashMap<KarlenderDate, HashSet<OsString>>,
    date: KarlenderDate,
    event: Event,
) {
    if let Some(events) = by_day.get_mut(&date) {
        events.insert(event.local_path().clone().into_os_string());
    } else {
        let mut set = HashSet::new();
        set.insert(event.local_path().clone().into_os_string());
        by_day.insert(date, set);
    }
}

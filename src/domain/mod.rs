mod calendar;
mod color;
mod event;
mod login;

pub(crate) mod index;
pub(crate) mod time;

pub(crate) mod error;

pub(crate) use calendar::*;
pub(crate) use color::*;
pub(crate) use event::*;
pub(crate) use login::*;
// pub(crate) use rrule::*;

pub(crate) const APP_DIR: &str = "karlender";

pub(crate) const CALENDARS_PATH: &str = "calendars";
pub(crate) const CAL_FILE_NAME: &str = "cal.json";
pub(crate) const INDEX_FILE: &str = "index";

pub(crate) fn get_app_dir() -> std::path::PathBuf {
    let mut app_dir = glib::user_data_dir();
    app_dir.push(APP_DIR);
    app_dir
}

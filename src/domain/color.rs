use hex_color::HexColor;

pub(crate) fn _adjust_color(input_color: &str) -> String {
    if let Ok(color) = HexColor::parse(input_color) {
        // 96 dec = 60 hex
        if color.a > 96 {
            return format!("{}60", &input_color[..7]);
        }
    }
    input_color.to_string()
}

pub(crate) fn get_text_color(bg_color: &str) -> String {
    if let Ok(color) = HexColor::parse(bg_color) {
        return get_contrast_color(
            color.r as f64 / 255.0,
            color.g as f64 / 255.0,
            color.b as f64 / 255.0,
            color.a as f64 / 255.0,
        );
    }
    "black".into()
}

fn get_contrast_color(r: f64, g: f64, b: f64, a: f64) -> String {
    let alpha = 1.0 - a;
    let brightness = (r * 0.299 + g * 0.587 + b * 0.114 + (alpha)) * 255.0;
    if brightness > 186.0 {
        "black".into()
    } else {
        "white".into()
    }
}

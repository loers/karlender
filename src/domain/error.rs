use std::error::Error;

pub(crate) fn to_io(e: impl Error) -> std::io::Error {
    std::io::Error::new(std::io::ErrorKind::InvalidData, e.to_string())
}

#[derive(Debug)]
pub enum DomainError {
    #[allow(dead_code)]
    InvalidICAL(String),
}

impl std::error::Error for DomainError {}

impl std::fmt::Display for DomainError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

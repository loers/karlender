use std::{cmp::Ordering, collections::HashMap, io::Error, path::PathBuf};

use async_std::{fs::File, io::WriteExt, path::Path, stream::StreamExt};
use serde::{Deserialize, Serialize};
use url::Url;
use uuid::Uuid;

use super::{
    error::DomainError,
    time::{
        DatetimeFormat, KarlenderDate, KarlenderDateTime, KarlenderDateTimeDuration,
        KarlenderTimezone,
    },
    Calendar, CAL_FILE_NAME,
};

pub const DEFAULT_COLOR: &str = "#c0c0c0";

pub const _RRULE_FIELD_FREQ: &str = "FREQ";
pub const RRULE_VALUE_YEARLY: &str = "YEARLY";
pub const RRULE_VALUE_MONTHLY: &str = "MONTHLY";
pub const RRULE_VALUE_WEEKLY: &str = "WEEKLY";
pub const RRULE_VALUE_DAILY: &str = "DAILY";
pub const _RRULE_VALUE_HOURLY: &str = "HOURLY";

pub const RRULE_FIELD_BYDAY: &str = "BYDAY";
pub const RRULE_VALUE_BYDAY_MONDAY: &str = "MO";
pub const RRULE_VALUE_BYDAY_TUESDAY: &str = "TU";
pub const RRULE_VALUE_BYDAY_WEDNESDAY: &str = "WE";
pub const RRULE_VALUE_BYDAY_THURSDAY: &str = "TH";
pub const RRULE_VALUE_BYDAY_FRIDAY: &str = "FR";
pub const RRULE_VALUE_BYDAY_SATURDAY: &str = "SA";
pub const RRULE_VALUE_BYDAY_SUNDAY: &str = "SU";

pub const RRULE_FIELD_INTERVAL: &str = "INTERVAL";

pub const RRULE_FIELD_COUNT: &str = "COUNT";
pub const RRULE_FIELD_UNTIL: &str = "UNTIL";

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Field {
    Summary(String),
    Location(String),
    Start(DateField, u32),
    End(DateField, u32),
    FullDay(bool),
    Calendar(String),
    Notes(String),
    RRule(String),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum DateField {
    Day,
    Month,
    Year,
    Hour,
    Minute,
}

impl Field {
    pub(crate) fn as_str(&self) -> &str {
        match self {
            Self::Summary(_) => "SUMMARY",
            Self::Location(_) => "LOCATION",
            Self::Start(_, _) => "DTSTART",
            Self::End(_, _) => "DTEND",
            Self::RRule(_) => "RRULE",
            Self::Notes(_) => "DESCRIPTION",
            Self::FullDay(_) => "fullday",
            Self::Calendar(_) => "calendar",
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub(crate) struct Event {
    calendar_url: Url,
    #[serde(skip)]
    local_path: PathBuf,
    #[serde(skip)]
    ical: Option<minicaldav::Event>,
    #[serde(skip)]
    changed: Changed,
}

impl Event {
    pub(crate) fn new(calendar: &Calendar, ical: minicaldav::Event) -> Option<Self> {
        match ical.get("UID") {
            Some(uid) => {
                let local_path = calendar.local_path().join(format!("{}.ics", uid));
                Some(Self {
                    local_path,
                    calendar_url: calendar.url().clone(),
                    ical: Some(ical),
                    changed: Changed::No,
                })
            }
            None => {
                log::warn!("Failed to parse event: No UID in ical file;");
                None
            }
        }
    }

    pub(crate) fn with_day(cal: &Calendar, day: &KarlenderDate) -> Option<Event> {
        let tz = &KarlenderTimezone::local();
        Self::with_datetime(cal, &day.and_hms(0, 0, 0, Some(tz)))
    }
    pub(crate) fn with_datetime(cal: &Calendar, datetime: &KarlenderDateTime) -> Option<Event> {
        let uuid = Uuid::new_v4().to_string();
        match cal.url().join(&format!("{}.ics", uuid)) {
            Ok(url) => {
                let mut mdav_event = minicaldav::Event::builder(url).build();
                mdav_event.set("UID", &uuid);
                let tz = &KarlenderTimezone::local();
                let now = tz.to_utc(&tz.now());
                mdav_event.set(
                    "DTSTART",
                    &datetime.format(super::time::DatetimeFormat::ISO8601Z),
                );
                // mdav_event.set_property_attribute("DTSTART", "TZID", tz.name());
                mdav_event.set(
                    "DTEND",
                    &(datetime.clone() + KarlenderDateTimeDuration::minutes(60))
                        .format(super::time::DatetimeFormat::ISO8601Z),
                );
                // mdav_event.set_property_attribute("DTEND", "TZID", tz.name());

                mdav_event.set(
                    "DTSTAMP",
                    &tz.to_utc(&now)
                        .format(super::time::DatetimeFormat::ISO8601Z),
                );

                Event::new(cal, mdav_event)
            }
            Err(e) => {
                log::error!("Failed to create empty event: {e}");
                None
            }
        }
    }

    pub(crate) fn set(&mut self, calendars: &[Calendar], field: &Field) {
        self.changed = Changed::Yes;

        let field_name = field.as_str();
        let start = self.start_local();
        let end = self.end_local();
        let fullday = self.is_full_day();

        let tz = if let Some(created_at) = self.created_at() {
            KarlenderTimezone::local().to_tz_at(&created_at)
        } else {
            KarlenderTimezone::local()
        };
        if let Some(ical) = self.ical.as_mut() {
            match field {
                Field::Summary(val) => ical.set(field_name, val),
                Field::Notes(val) => ical.set(field_name, val),
                Field::Location(val) => ical.set(field_name, val),
                Field::FullDay(fd) => {
                    self.set_full_day(*fd);
                }
                Field::Calendar(url) => {
                    if let Ok(url) = Url::parse(url) {
                        let filename = self
                            .local_path()
                            .file_name()
                            .and_then(|n| n.to_str())
                            .map(|s| s.to_string())
                            .unwrap_or_else(|| {
                                let uuid = Uuid::new_v4().to_string();
                                format!("{}.ics", uuid)
                            });
                        self.calendar_url = url.clone();
                        if let Some(calendar) = calendars.iter().find(|c| c.url() == &url) {
                            if let Some(ical) = self.ical.as_mut() {
                                match url.join(&filename) {
                                    Ok(url) => {
                                        *ical = minicaldav::Event::new(
                                            ical.etag().cloned(),
                                            url,
                                            ical.ical().clone(),
                                        );
                                        self.local_path = calendar.local_path().join(filename);
                                    }
                                    Err(e) => log::error!("Failed to construct event url: {e}"),
                                }
                            }
                        }
                    }
                }
                Field::Start(date_field, value) => {
                    if let Ok(date) = start {
                        let new_date = get_new_date(date_field, &date, value, &tz);
                        if date != new_date {
                            let new_date_in_utc = tz.to_utc(&new_date);
                            if fullday {
                                ical.set(
                                    field_name,
                                    &new_date_in_utc
                                        .format(super::time::DatetimeFormat::ISO8601DATE),
                                )
                            } else {
                                ical.set(
                                    field_name,
                                    &new_date_in_utc.format(super::time::DatetimeFormat::ISO8601Z),
                                )
                            }
                        }
                    }
                }
                Field::End(date_field, value) => {
                    if let Ok(date) = end {
                        let new_date = get_new_date(date_field, &date, value, &tz);
                        if date != new_date {
                            let new_date_in_utc = tz.to_utc(&new_date);
                            if fullday {
                                ical.set(
                                    field_name,
                                    &new_date_in_utc
                                        .format(super::time::DatetimeFormat::ISO8601DATE),
                                )
                            } else {
                                ical.set(
                                    field_name,
                                    &new_date_in_utc.format(super::time::DatetimeFormat::ISO8601Z),
                                )
                            }
                        }
                    }
                }
                Field::RRule(rrule) => {
                    ical.set("RRULE", rrule);
                }
            }
        }
    }

    pub(crate) fn url(&self) -> Option<&Url> {
        if let Some(ical) = self.ical.as_ref() {
            Some(ical.url())
        } else {
            None
        }
    }

    pub(crate) fn etag(&self) -> Option<&String> {
        if let Some(ical) = self.ical.as_ref() {
            ical.etag()
        } else {
            None
        }
    }

    pub(crate) fn created_at(&self) -> Option<KarlenderDateTime> {
        if let Some(ical) = self.ical.as_ref() {
            match ical.get("DTSTAMP") {
                Some(stamp) => {
                    let tz = self.timezone().unwrap_or_else(KarlenderTimezone::utc);
                    match KarlenderDateTime::from_iso8601(stamp, Some(&tz)) {
                        Ok((kdt, _full_day)) => Some(kdt),
                        Err(_) => None,
                    }
                }
                None => None,
            }
        } else {
            None
        }
    }

    pub(crate) fn timezone(&self) -> Option<KarlenderTimezone> {
        if let Some(ical) = self.ical.as_ref() {
            let tzid = ical
                .property("DTSTART")
                .and_then(|p| p.attribute("TZID").cloned());
            tzid.map(|tz| KarlenderTimezone::from_str(&tz))
        } else {
            None
        }
    }

    pub(crate) fn changed(&self) -> &Changed {
        &self.changed
    }

    pub(crate) fn calendar_url(&self) -> &Url {
        &self.calendar_url
    }

    pub(crate) fn location(&self) -> Option<&String> {
        if let Some(ical) = self.ical.as_ref() {
            ical.get("LOCATION")
        } else {
            None
        }
    }

    pub(crate) fn summary(&self) -> Option<&String> {
        if let Some(ical) = self.ical.as_ref() {
            ical.get("SUMMARY")
        } else {
            None
        }
    }

    pub(crate) fn description(&self) -> Option<&String> {
        if let Some(ical) = self.ical.as_ref() {
            ical.get("DESCRIPTION")
        } else {
            None
        }
    }

    pub(crate) fn into_inner(self) -> Option<minicaldav::Event> {
        self.ical
    }

    /// get start datetime in UTC
    pub(crate) fn start(&self) -> Result<KarlenderDateTime, DomainError> {
        if let Some(ical) = self.ical.as_ref() {
            let tz = self.timezone().unwrap_or_else(KarlenderTimezone::utc);
            match ical.get("DTSTART") {
                None => Err(DomainError::InvalidICAL("Missing DTSTART".into())),
                Some(start) => match KarlenderDateTime::from_iso8601(start, Some(&tz)) {
                    Ok((datetime, _full_day)) => Ok(tz.to_utc(&datetime)),
                    Err(e) => Err(DomainError::InvalidICAL(e)),
                },
            }
        } else {
            Err(DomainError::InvalidICAL("Missing ical".into()))
        }
    }
    pub(crate) fn start_local(&self) -> Result<KarlenderDateTime, DomainError> {
        let tz = self.timezone().unwrap_or_else(KarlenderTimezone::local);
        let created_at = self.created_at().unwrap_or_else(|| tz.now());
        let start = self.start()?;
        Ok(tz.from_utc_created_at(&start, &created_at))
    }

    /// get end datetime in UTC
    pub(crate) fn end(&self) -> Result<KarlenderDateTime, DomainError> {
        if let Some(ical) = self.ical.as_ref() {
            let tz = self.timezone().unwrap_or_else(KarlenderTimezone::utc);
            match ical.get("DTEND") {
                None => Err(DomainError::InvalidICAL("Missing DTEND".into())),
                Some(start) => match KarlenderDateTime::from_iso8601(start, Some(&tz)) {
                    Ok((datetime, _full_day)) => Ok(tz.to_utc(&datetime)),
                    Err(e) => Err(DomainError::InvalidICAL(e)),
                },
            }
        } else {
            Err(DomainError::InvalidICAL("Missing ical".into()))
        }
    }
    pub(crate) fn end_local(&self) -> Result<KarlenderDateTime, DomainError> {
        let tz = self.timezone().unwrap_or_else(KarlenderTimezone::local);
        let created_at = self.created_at().unwrap_or_else(|| tz.now());
        let end = self.end()?;
        Ok(tz.from_utc_created_at(&end, &created_at))
    }

    pub(crate) fn is_full_day(&self) -> bool {
        if let Some(ical) = self.ical.as_ref() {
            match ical.get("DTSTART") {
                None => false,
                Some(start) => {
                    let tz = self.timezone();
                    match KarlenderDateTime::from_iso8601(start, tz.as_ref()) {
                        Ok((_kdt, full_day)) => full_day,
                        Err(_e) => false,
                    }
                }
            }
        } else {
            false
        }
    }

    pub(crate) fn set_full_day(&mut self, full_day: bool) {
        let tz = self.timezone();
        if let Some(ical) = self.ical.as_mut() {
            if full_day {
                if let Some(start) = ical.get("DTSTART").cloned() {
                    if let Some(end) = ical.get("DTEND").cloned() {
                        if let Ok((start, _)) = KarlenderDateTime::from_iso8601(&start, tz.as_ref())
                        {
                            if let Ok((end, _)) = KarlenderDateTime::from_iso8601(&end, tz.as_ref())
                            {
                                ical.set("DTSTART", &start.format(DatetimeFormat::ISO8601DATE));
                                ical.set("DTEND", &end.format(DatetimeFormat::ISO8601DATE));
                            }
                        }
                    }
                }
            } else if let Some(start) = ical.get("DTSTART").cloned() {
                if let Some(end) = ical.get("DTEND").cloned() {
                    if let Ok((start, _)) = KarlenderDateTime::from_iso8601(&start, tz.as_ref()) {
                        if let Ok((end, _)) = KarlenderDateTime::from_iso8601(&end, tz.as_ref()) {
                            ical.set("DTSTART", &start.format(DatetimeFormat::ISO8601Z));
                            ical.set("DTEND", &end.format(DatetimeFormat::ISO8601Z));
                        }
                    }
                }
            }
        }
    }

    pub(crate) fn rrule(&self) -> Option<&String> {
        if let Some(ical) = self.ical.as_ref() {
            ical.get("RRULE")
        } else {
            None
        }
    }

    pub(crate) fn _properties(&self) -> Vec<(&String, &String)> {
        if let Some(ical) = self.ical.as_ref() {
            ical.properties()
        } else {
            Vec::new()
        }
    }

    pub(crate) fn local_path(&self) -> &PathBuf {
        &self.local_path
    }

    fn repeat_value<'a>(
        &self,
        prop: &str,
        repeat: &'a Option<HashMap<String, String>>,
    ) -> Option<&'a String> {
        match repeat {
            Some(r) => r.get(prop),
            None => None,
        }
    }

    fn repeats(&self, freq: &str, repeat: &Option<HashMap<String, String>>) -> bool {
        match repeat {
            Some(r) => match r.get("FREQ") {
                Some(v) => v.as_str() == freq,
                None => false,
            },
            None => false,
        }
    }

    fn repeat(&self) -> Option<HashMap<String, String>> {
        if let Some(s) = self.rrule() {
            let mut map = HashMap::new();
            for kv in s.split(';') {
                if let Some((k, v)) = kv.split_once('=') {
                    map.insert(k.into(), v.into());
                }
            }
            Some(map)
        } else {
            None
        }
    }

    pub(crate) fn contains_date(&self, date: &KarlenderDate) -> bool {
        let start = self.start();
        let end = self.end();
        let (start, end) = match (start, end) {
            (Ok(s), Ok(e)) => (s, e),
            _ => return false,
        };
        let start_date = start.date();
        if date < &start_date {
            return false;
        }
        let repeat = self.repeat();

        if self.repeats(RRULE_VALUE_DAILY, &repeat) {
            let mut interval: i64 = 1;
            let mut is_in_interval = true;
            let mut is_before_end_date = true;
            let mut is_before_count_end = true;
            if let Some(interv) = self.repeat_value(RRULE_FIELD_INTERVAL, &repeat) {
                if let Ok(i) = interv.parse::<i64>() {
                    interval = i;
                } else {
                    log::error!(
                        "Event {:?} has invalid interval: {interv}",
                        self.local_path()
                    );
                }
                let delta = date.signed_duration_since(start_date.clone());
                is_in_interval = delta.num_days().rem_euclid(interval) == 0;
            }
            if let Some(end_date) = self.repeat_value(RRULE_FIELD_UNTIL, &repeat) {
                if let Ok((end_date, _)) = KarlenderDateTime::from_iso8601(end_date, None) {
                    is_before_end_date = date <= &end_date.date();
                }
            }
            if let Some(count) = self.repeat_value(RRULE_FIELD_COUNT, &repeat) {
                if let Ok(count) = count.parse::<i64>() {
                    let delta = date.signed_duration_since(start_date);
                    is_before_count_end = (count * interval) - delta.num_days() > 0;
                } else {
                    log::error!("Event {:?} has invalid count: {count}", self.local_path());
                }
            }
            return is_in_interval && is_before_count_end && is_before_end_date;
        }

        if self.repeats(RRULE_VALUE_WEEKLY, &repeat) {
            let mut is_weekday = true;
            let mut interval = 1;
            let mut is_in_interval = true;
            let mut is_before_end_date = true;
            let mut is_before_count_end = true;

            if let Some(weekdays) = self.repeat_value(RRULE_FIELD_BYDAY, &repeat) {
                let weekdays: Vec<&str> = weekdays.split(',').collect();
                is_weekday = match date.weekday().number_from_monday() {
                    0 => weekdays.contains(&RRULE_VALUE_BYDAY_MONDAY),
                    1 => weekdays.contains(&RRULE_VALUE_BYDAY_TUESDAY),
                    2 => weekdays.contains(&RRULE_VALUE_BYDAY_WEDNESDAY),
                    3 => weekdays.contains(&RRULE_VALUE_BYDAY_THURSDAY),
                    4 => weekdays.contains(&RRULE_VALUE_BYDAY_FRIDAY),
                    5 => weekdays.contains(&RRULE_VALUE_BYDAY_SATURDAY),
                    6 => weekdays.contains(&RRULE_VALUE_BYDAY_SUNDAY),
                    _ => false,
                };
            }
            if let Some(interv) = self.repeat_value(RRULE_FIELD_INTERVAL, &repeat) {
                if let Ok(i) = interv.parse::<i64>() {
                    interval = i;
                } else {
                    log::error!(
                        "Event {:?} has invalid interval: {interv}",
                        self.local_path()
                    );
                }
                let delta = date.signed_duration_since(start_date.clone());
                let is_after_n_weeks = delta.num_days().rem_euclid(interval * 7) == 0;
                let n_weeks = delta.num_days() / 7;
                is_in_interval = is_after_n_weeks && n_weeks.rem_euclid(interval) == 0;
            }
            if let Some(end_date) = self.repeat_value(RRULE_FIELD_UNTIL, &repeat) {
                if let Ok((end_date, _)) = KarlenderDateTime::from_iso8601(end_date, None) {
                    is_before_end_date = date <= &end_date.date();
                }
            }
            if let Some(count) = self.repeat_value(RRULE_FIELD_COUNT, &repeat) {
                if let Ok(count) = count.parse::<i64>() {
                    let delta = date.signed_duration_since(start_date);
                    is_before_count_end = (count * interval * 7) - delta.num_days() > 0;
                } else {
                    log::error!("Event {:?} has invalid count: {count}", self.local_path());
                }
            }
            return is_weekday && is_in_interval && is_before_count_end && is_before_end_date;
        }

        if self.repeats(RRULE_VALUE_MONTHLY, &repeat) {
            let mut interval = 1;
            let mut is_in_interval = true;
            let mut is_before_end_date = true;
            let mut is_before_count_end = true;

            if let Some(interv) = self.repeat_value(RRULE_FIELD_INTERVAL, &repeat) {
                if let Ok(i) = interv.parse::<i32>() {
                    interval = i;
                } else {
                    log::error!(
                        "Event {:?} has invalid interval: {interv}",
                        self.local_path()
                    );
                }
                let event_month = start_date.month() as i32;
                let check_month = date.month() as i32;
                is_in_interval = event_month == check_month
                    || (check_month - event_month).rem_euclid(interval) == 0
            }
            if let Some(end_date) = self.repeat_value(RRULE_FIELD_UNTIL, &repeat) {
                if let Ok((end_date, _)) = KarlenderDateTime::from_iso8601(end_date, None) {
                    is_before_end_date = date <= &end_date.date();
                }
            }
            if let Some(count) = self.repeat_value(RRULE_FIELD_COUNT, &repeat) {
                if let Ok(count) = count.parse::<i32>() {
                    let event_year = start_date.year();
                    let check_year = date.year();
                    let event_month = start_date.month() as i32;
                    let check_month = date.month() as i32;
                    match event_year.cmp(&check_year) {
                        Ordering::Equal => {
                            is_before_count_end = event_month == check_month
                                || (check_month - event_month) / interval < count;
                        }
                        Ordering::Less => {
                            let months_in_first_year = 12 - event_month;
                            let monhts_of_full_years = (check_year - event_year - 1) * 12;
                            let overall_months =
                                months_in_first_year + monhts_of_full_years + check_month;
                            is_before_count_end = overall_months / interval < count;
                        }
                        Ordering::Greater => {
                            // nothing
                        }
                    }
                } else {
                    log::error!("Event {:?} has invalid count: {count}", self.local_path());
                }
            }
            return is_in_interval
                && is_before_count_end
                && is_before_end_date
                && date.day() == start.day();
        }
        if self.repeats(RRULE_VALUE_YEARLY, &repeat) {
            return date.year() >= start.year()
                && date.month() >= start.month()
                && date.month() <= end.month()
                && date.day() >= start.day()
                && date.day() < end.day();
        }

        if (date.year() >= start.year() && date.year() <= end.year())
            && date.month() >= start.month()
            && date.month() <= end.month()
        {
            if start.day() == end.day() && date.day() == start.day() {
                return true;
            }
            if date.day() >= start.day() && date.day() < end.day() {
                return true;
            }
        }

        false
    }

    pub(crate) async fn undo(&mut self) -> async_std::io::Result<()> {
        self.changed = Changed::No;
        self.persist().await?;
        Ok(())
    }
}

fn get_new_date(
    date_field: &DateField,
    date: &KarlenderDateTime,
    value: &u32,
    tz: &KarlenderTimezone,
) -> KarlenderDateTime {
    match date_field {
        DateField::Day => {
            //
            KarlenderDate::from_ymd(date.year(), date.month(), *value as u8).and_hms(
                date.hour(),
                date.minute(),
                0,
                Some(tz),
            )
        }
        DateField::Month => {
            //
            KarlenderDate::from_ymd(date.year(), *value as u8, date.day()).and_hms(
                date.hour(),
                date.minute(),
                0,
                Some(tz),
            )
        }
        DateField::Year => {
            //
            KarlenderDate::from_ymd(*value as i32, date.month(), date.day()).and_hms(
                date.hour(),
                date.minute(),
                0,
                Some(tz),
            )
        }
        DateField::Hour => {
            //
            KarlenderDate::from_ymd(date.year(), date.month(), date.day()).and_hms(
                *value as u8,
                date.minute(),
                0,
                Some(tz),
            )
        }
        DateField::Minute => {
            //
            KarlenderDate::from_ymd(date.year(), date.month(), date.day()).and_hms(
                date.hour(),
                *value as u8,
                0,
                Some(tz),
            )
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Changed {
    No,
    Deleted,
    Yes,
}

impl Default for Changed {
    fn default() -> Self {
        Self::No
    }
}

impl std::fmt::Display for Changed {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Changed::No => {
                write!(f, "no")
            }
            Changed::Deleted => {
                write!(f, "deleted")
            }
            Changed::Yes => {
                write!(f, "yes")
            }
        }
    }
}

// persistence

impl Event {
    pub(crate) async fn persist(&self) -> async_std::io::Result<()> {
        let mut event_file = File::create(&self.local_path).await?;
        let etag = self.ical.as_ref().and_then(|ical| ical.etag().cloned());
        let evurl = match self.url() {
            Some(u) => u.as_str(),
            None => {
                let msg = format!(
                    "Failed to persist event {:?}: No event URL found!",
                    self.local_path()
                );
                return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, msg));
            }
        };
        let calurl = self.calendar_url.as_str();

        let mut content = Vec::new();

        content.append(&mut format!("{}\n", calurl).as_bytes().to_vec());
        content.append(&mut format!("{}\n", evurl).as_bytes().to_vec());
        content.append(
            &mut format!("{}\n", etag.unwrap_or_default())
                .as_bytes()
                .to_vec(),
        );
        content.append(&mut format!("{}\n", self.changed).as_bytes().to_vec());
        content.append(&mut "\n".as_bytes().to_vec());
        if let Some(ical) = self.ical.as_ref() {
            let ical = ical.ical().serialize();
            content.append(&mut ical.as_bytes().to_vec());
            event_file.write_all(&content).await?;
            Ok(())
        } else {
            Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                "No ical data in event",
            ))
        }
    }

    pub(crate) async fn remove(&mut self) -> async_std::io::Result<()> {
        self.changed = Changed::Deleted;
        self.persist().await?;
        Ok(())
    }

    pub(crate) async fn parse(path: &Path) -> async_std::io::Result<Self> {
        let data = async_std::fs::read_to_string(path).await?;
        let mut cal_url = String::new();
        let mut ev_url = String::new();
        let mut etag = None;
        let mut ical = String::new();
        let mut changed = Changed::No;

        for (i, line) in data.lines().enumerate() {
            if i == 0 {
                cal_url = line.into();
            }
            if i == 1 {
                ev_url = line.into();
            }
            if i == 2 && !line.is_empty() {
                etag = Some(line.to_string());
            }
            if i == 3 {
                changed = match line {
                    "yes" => Changed::Yes,
                    "deleted" => Changed::Deleted,
                    _ => Changed::No,
                };
            }
            if i > 4 {
                ical.push_str(line);
                ical.push('\n');
            }
        }
        match minicaldav::parse_ical(&ical) {
            Ok(ical) => {
                if let Ok(cal_url) = url::Url::parse(&cal_url) {
                    if let Ok(ev_url) = url::Url::parse(&ev_url) {
                        let event = minicaldav::Event::new(etag, ev_url, ical);
                        return Ok(Self {
                            calendar_url: cal_url,
                            local_path: path.into(),
                            ical: Some(event),
                            changed,
                        });
                    }
                }
                log::trace!("ICAL that could not be parsed: {}", ical.serialize());
                Err(Error::new(
                    std::io::ErrorKind::InvalidData,
                    format!(
                        "Failed to parse ical data '{:?}': No VEVENT found.",
                        path.as_os_str()
                    ),
                ))
            }
            Err(e) => {
                log::error!("Failed to parse event {:?}: {e}", path.as_os_str());
                Err(Error::new(std::io::ErrorKind::InvalidData, e))
            }
        }
    }

    pub(crate) async fn local(calendar: &Calendar) -> std::io::Result<Vec<Event>> {
        let mut entries = async_std::fs::read_dir(calendar.local_path()).await?;
        let mut events = Vec::new();
        while let Some(res) = entries.next().await {
            let entry = res?;
            if entry.file_type().await?.is_file() && entry.file_name() != CAL_FILE_NAME {
                let event = Event::parse(entry.path().as_path()).await?;
                if event.changed != Changed::Deleted {
                    events.push(event);
                }
            }
        }
        Ok(events)
    }
}

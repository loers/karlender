use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq)]
pub(crate) struct Login {
    pub(crate) url: Url,
    pub(crate) email: Email,
    pub(crate) is_karlender_login: bool,
    pub(crate) has_token: bool,
}

impl std::fmt::Debug for Login {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Login")
            .field("url", &self.url.to_string())
            .field("email", &self.email)
            .finish()
    }
}

impl Login {
    pub(crate) fn new(url: &str, email: &str) -> Result<Self, String> {
        let url = Url::parse(url).map_err(|e| e.to_string())?;
        let email = Email(email.into());
        Ok(Self {
            email,
            url,
            is_karlender_login: false,
            has_token: false,
        })
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub(crate) struct Email(String);
impl Email {
    pub(crate) fn as_str(&self) -> &str {
        &self.0
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub(crate) struct Password {
    value: String,
    token: bool,
}
impl From<String> for Password {
    fn from(value: String) -> Self {
        Self {
            value,
            token: false,
        }
    }
}
impl Password {
    pub(crate) fn token(token: String) -> Self {
        Self {
            value: token,
            token: true,
        }
    }
    pub(crate) fn as_str(&self) -> &str {
        &self.value
    }
    pub(crate) fn is_token(&self) -> bool {
        self.token
    }
}

use std::sync::Arc;

use async_trait::async_trait;
use gettextrs::gettext;

use crate::{
    domain::time::{
        self, KarlenderDate, KarlenderDateTime, KarlenderDateTimeDuration, KarlenderTimezone,
    },
    APP_NAME,
};

use super::{
    store,
    ui::{DAY_PAGE_NAME, MONTH_PAGE_NAME, WEEK_PAGE_NAME, YEAR_PAGE_NAME},
    Selector,
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Action {
    SelectDay(KarlenderDate),
    SelectTimeslot(KarlenderDateTime),
    ClearSelection,

    SetMonth(KarlenderDate),

    NextYear,
    NextMonth,
    NextWeek,
    PrevYear,
    PrevMonth,
    PrevWeek,
    NextDay,
    PrevDay,
}
impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        Self::Selection(value)
    }
}

#[derive(Clone, Debug)]
pub(crate) struct Slice {
    pub(crate) current_day: KarlenderDate,
    pub(crate) current_week: KarlenderDate,
    pub(crate) current_month: KarlenderDate,
    pub(crate) current_year: KarlenderDate,
    pub(crate) selection: Option<Selection>,
}

impl Default for Slice {
    fn default() -> Self {
        Self {
            current_day: KarlenderTimezone::local().today(),
            current_week: KarlenderTimezone::local().today(),
            current_month: KarlenderTimezone::local().today(),
            current_year: KarlenderTimezone::local().today(),
            selection: Default::default(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Selection {
    Day(KarlenderDate),
    Timeslot(KarlenderDateTime),
}

pub(super) fn reduce(action: &super::Action, state: &mut super::State) {
    match action {
        super::Action::Selection(action) => match action {
            Action::SetMonth(month) => {
                state.selection.selection = Some(Selection::Day(month.clone()));
                state.selection.current_month = month.clone();
            }
            Action::SelectDay(day) => {
                state.selection.selection = Some(Selection::Day(day.clone()));
                state.selection.current_day = day.clone();
            }
            Action::SelectTimeslot(timeslot) => {
                state.selection.selection = Some(Selection::Timeslot(timeslot.clone()));
                state.selection.current_day = timeslot.date();
            }
            Action::ClearSelection => {
                state.selection.selection = None;
            }
            Action::NextYear => {
                state.selection.current_year = KarlenderDate::from_ymd(
                    state.selection.current_year.year() + 1,
                    state.selection.current_year.month(),
                    state.selection.current_year.day(),
                )
            }
            Action::PrevYear => {
                state.selection.current_year = KarlenderDate::from_ymd(
                    state.selection.current_year.year() - 1,
                    state.selection.current_year.month(),
                    state.selection.current_year.day(),
                )
            }
            Action::NextMonth => {
                state.selection.current_month =
                    time::get_month_by_delta(&state.selection.current_month, 1);
            }
            Action::PrevMonth => {
                state.selection.current_month =
                    time::get_month_by_delta(&state.selection.current_month, -1);
            }
            Action::NextWeek => {
                state.selection.current_week =
                    state.selection.current_week.clone() + KarlenderDateTimeDuration::weeks(1);
            }
            Action::PrevWeek => {
                state.selection.current_week =
                    state.selection.current_week.clone() - KarlenderDateTimeDuration::weeks(1);
            }
            Action::NextDay => {
                state.selection.current_day =
                    state.selection.current_day.clone() + KarlenderDateTimeDuration::days(1);
            }
            Action::PrevDay => {
                state.selection.current_day =
                    state.selection.current_day.clone() - KarlenderDateTimeDuration::days(1);
            }
        },
        super::Action::UI(super::ui::Action::GotoNow) => {
            state.selection.selection = None;
        }
        _ => (),
    }
}

pub(super) struct Middleware;
#[async_trait]
impl gstore::Middleware<super::Action, super::State> for Middleware {
    async fn pre_reduce(&self, a: Arc<super::Action>, state: Arc<super::State>) {
        if let super::Action::Selection(action) = a.as_ref() {
            match action {
                Action::SelectDay(day) => {
                    if let Some(Selection::Day(selected_day)) = state.selection.selection.as_ref() {
                        if day == selected_day {
                            if state.ui.mobile.is_mobile() {
                                store().dispatch(super::ui::Action::Replace(DAY_PAGE_NAME))
                            } else {
                                store().dispatch(Action::ClearSelection)
                            }
                        }
                    }
                }
                Action::SelectTimeslot(ts) => {
                    if let Some(Selection::Timeslot(selected_ts)) =
                        state.selection.selection.as_ref()
                    {
                        if ts == selected_ts {
                            if state.ui.mobile.is_mobile() {
                                store().dispatch(super::editor::Action::NewEvent);
                            } else {
                                store().dispatch(Action::ClearSelection)
                            }
                        }
                    }
                }
                _ => {
                    // ignore
                }
            }
        }
    }

    async fn post_reduce(&self, action: Arc<super::Action>, state: Arc<super::State>) {
        match action.as_ref() {
            super::Action::Init => store().dispatch(super::ui::Action::SetTitle(update_title(
                &state,
                &Action::NextMonth,
            ))),
            super::Action::Selection(a) => {
                match a {
                    crate::store::selection::Action::NextYear => {
                        store().dispatch(super::ui::Action::SetTitle(update_title(&state, a)))
                    }
                    crate::store::selection::Action::PrevYear => {
                        store().dispatch(super::ui::Action::SetTitle(update_title(&state, a)))
                    }
                    crate::store::selection::Action::NextMonth => {
                        store().dispatch(super::ui::Action::SetTitle(update_title(&state, a)))
                    }
                    crate::store::selection::Action::PrevMonth => {
                        store().dispatch(super::ui::Action::SetTitle(update_title(&state, a)))
                    }

                    crate::store::selection::Action::NextWeek => {
                        store().dispatch(super::ui::Action::SetTitle(update_title(&state, a)))
                    }
                    crate::store::selection::Action::PrevWeek => {
                        store().dispatch(super::ui::Action::SetTitle(update_title(&state, a)))
                    }

                    crate::store::selection::Action::NextDay => {
                        store().dispatch(super::ui::Action::SetTitle(update_title(&state, a)))
                    }
                    crate::store::selection::Action::PrevDay => {
                        store().dispatch(super::ui::Action::SetTitle(update_title(&state, a)))
                    }
                    _ => {
                        // ignore
                    }
                }
            }

            _ => {
                // ignore
            }
        }
    }
}

pub(crate) fn update_title(state: &crate::store::State, action: &Action) -> String {
    let mut title = APP_NAME.to_string();

    let date = match action {
        Action::NextYear => &state.selection.current_year,
        Action::NextMonth => &state.selection.current_month,
        Action::NextWeek => &state.selection.current_week,
        Action::PrevYear => &state.selection.current_year,
        Action::PrevMonth => &state.selection.current_month,
        Action::PrevWeek => &state.selection.current_week,
        Action::NextDay => &state.selection.current_day,
        Action::PrevDay => &state.selection.current_day,
        _ => &state.selection.current_month,
    };

    let year = date.year();
    let mon = match date.month0() {
        0 => gettext("January"),
        1 => gettext("Febrary"),
        2 => gettext("March"),
        3 => gettext("April"),
        4 => gettext("May"),
        5 => gettext("June"),
        6 => gettext("July"),
        7 => gettext("August"),
        8 => gettext("September"),
        9 => gettext("October"),
        10 => gettext("November"),
        _ => gettext("December"),
    };
    if let Some(page) = state.ui.history.back() {
        match *page {
            YEAR_PAGE_NAME => {
                title = format!("{}", year);
            }
            MONTH_PAGE_NAME => {
                title = format!("{} {}", mon, year);
            }
            WEEK_PAGE_NAME => {
                let week = date.iso_week();
                let cw = gettext("CW");
                title = format!("{} {} {} {}", mon, year, cw, week);
            }
            DAY_PAGE_NAME => {
                title = format!("{} {} {}", date.day(), mon, year);
            }
            _ => {
                // ignore
            }
        }
    }
    title
}

pub(crate) const SELECT_SELECTION_AND_INDEX: &Selector =
    &|a, b| a.selection.selection != b.selection.selection || a.index.index != b.index.index;

pub(crate) const SELECT_CURRENT_DAY_AND_INDEX: &Selector =
    &|a, b| a.selection.current_day != b.selection.current_day || a.index.index != b.index.index;

pub(crate) const SELECT_HISTORY_AND_SELECTION_AND_MOBILE: &Selector = &|a, b| {
    a.selection.selection != b.selection.selection
        || a.ui.history != b.ui.history
        || a.ui.mobile != b.ui.mobile
};

pub(crate) const SELECT_CURRENT_YEAR_AND_INDEX: &Selector =
    &|a, b| a.selection.current_year != b.selection.current_year || a.index.index != b.index.index;

pub(crate) const SELECT_CURRENT_MONTH: &Selector =
    &|a, b| a.selection.current_month != b.selection.current_month;

pub(crate) const SELECT_CURRENT_WEEK: &Selector =
    &|a, b| a.selection.current_week != b.selection.current_week;

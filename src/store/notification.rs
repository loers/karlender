use crate::{APP_ID, APP_NAME};

use super::{store, Selector, State};
use async_trait::async_trait;
use grx::toast_overlay::Toast;
use std::sync::Arc;

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Action {
    Show(String),
    InApp {
        toast: Toast,
        dismiss: Option<Box<super::Action>>,
        timeout: Option<Box<super::Action>>,
    },
    Dismiss,
    Hide,
}
impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        super::Action::Notification(value)
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub(crate) struct Slice {
    pub(crate) message: Option<String>,
    pub(crate) in_app: Option<(Toast, Option<super::Action>, Option<super::Action>)>,
}

pub(super) fn reduce(action: &super::Action, state: &mut State) {
    if let super::Action::Notification(action) = action {
        match &action {
            Action::Show(message) => {
                state.notification.message = Some(message.clone());
            }
            Action::Hide => {
                state.notification.message = None;
            }
            Action::InApp {
                toast,
                dismiss,
                timeout,
            } => {
                state.notification.in_app = Some((
                    toast.clone(),
                    dismiss.as_deref().cloned(),
                    timeout.as_deref().cloned(),
                ));
            }
            Action::Dismiss => {
                state.notification.in_app = None;
            }
        }
    }
}

pub(super) struct Middleware;
#[async_trait]
impl gstore::Middleware<super::Action, State> for Middleware {
    async fn pre_reduce(&self, _a: Arc<super::Action>, _: Arc<State>) {
        // ignore
    }

    async fn post_reduce(&self, a: Arc<super::Action>, _: Arc<State>) {
        if let super::Action::Notification(Action::Show(msg)) = a.as_ref() {
            async_std::task::spawn(async {
                async_std::task::sleep(std::time::Duration::from_secs(4)).await;
                store().dispatch(Action::Hide);
            });
            let _ = notify_rust::Notification::new()
                .summary(APP_NAME)
                .body(msg)
                .icon(APP_ID)
                .show()
                .map(|nh| {
                    nh.wait_for_action(|_| {
                        store().dispatch(Action::Hide);
                    });
                });
        }
        if let super::Action::Notification(Action::InApp {
            toast,
            dismiss: _,
            timeout: timeout_action,
        }) = a.as_ref()
        {
            if let Some(timeout) = toast.timeout() {
                if let Some(ta) = timeout_action.as_deref().cloned() {
                    async_std::task::spawn(async move {
                        async_std::task::sleep(timeout).await;
                        store().dispatch(ta);
                    });
                }
            }
        }
    }
}

pub(crate) const SELECT_IN_APP_NOTIFICATION: &Selector =
    &|a, b| a.notification.in_app != b.notification.in_app;

use crate::domain::{get_app_dir, time::KarlenderTimezone, Event, Field};

use super::{dispatch_err, selection::Selection, store, ui::EDITOR_PAGE_NAME, Selector, State};
use async_trait::async_trait;
use gettextrs::gettext;
use grx::toast_overlay::Toast;
use serde::{Deserialize, Serialize};
use std::{sync::Arc, time::Duration};

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Action {
    Save,
    NewEvent,
    Edit(Event),
    Remove(Event),
    RemoveTimeout,
    Update(Field),
    Undo(Event),
    OpenFolder,
    OpenLogs,
}
impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        super::Action::Editor(value)
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq, Serialize, Deserialize)]
pub(crate) struct Slice {
    pub(crate) event: Option<Event>,
    do_remove: bool,
}

pub(super) fn reduce(action: &super::Action, state: &mut State) {
    if let super::Action::Editor(action) = action {
        match action {
            Action::Save => {
                state.editor.event = None;
            }
            Action::Edit(event) => {
                state.editor.event = Some(event.clone());
            }
            Action::NewEvent => {
                let calendars = &state.calendar.calendars;
                let cal = calendars
                    .iter()
                    .find(|c| c.is_default())
                    .or_else(|| calendars.first());
                if let Some(cal) = cal {
                    match &state.selection.selection {
                        Some(Selection::Day(day)) => {
                            state.editor.event = Event::with_day(cal, day);
                        }
                        Some(Selection::Timeslot(day)) => {
                            let day = KarlenderTimezone::local().to_utc(day); // TODO
                            state.editor.event = Event::with_datetime(cal, &day);
                        }
                        _ => (),
                    }
                } else {
                    log::error!("Could init new event: No calendar found!");
                }
            }
            Action::Update(field) => {
                if let Some(event) = state.editor.event.as_mut() {
                    event.set(&state.calendar.calendars, field);
                }
            }
            _ => {
                // nothing to do
            }
        }
    }
}

pub(super) struct Middleware;
#[async_trait]
impl gstore::Middleware<super::Action, State> for Middleware {
    async fn pre_reduce(&self, action: Arc<super::Action>, state: Arc<State>) {
        if let super::Action::Editor(action) = &*action {
            match action {
                Action::Save => {
                    if let Some(event) = state.editor.event.clone() {
                        if let Err(e) = event.persist().await {
                            dispatch_err(format!("Failed to save event: {e}"))
                        } else {
                            store().dispatch(crate::store::index::Action::Index);
                            store().dispatch(crate::store::ui::Action::Back);
                            store().dispatch(super::sync::Action::Syncing {
                                report_errors: true,
                            })
                        }
                    }
                }
                Action::RemoveTimeout => store().dispatch(super::sync::Action::Syncing {
                    report_errors: true,
                }),
                Action::OpenFolder => {
                    if let Err(e) = std::process::Command::new("xdg-open")
                        .arg(get_app_dir())
                        .output()
                    {
                        dispatch_err(format!("Can not open karlender app directory: {e}"));
                    }
                }
                Action::OpenLogs => {
                    if let Err(e) = std::process::Command::new("xdg-open")
                        .arg(get_app_dir().join("karlender.log"))
                        .output()
                    {
                        dispatch_err(format!("Can not open karlender app directory: {e}"));
                    }
                }
                _ => {
                    // ignore
                }
            }
        }
    }

    async fn post_reduce(&self, a: Arc<super::Action>, state: Arc<State>) {
        if let super::Action::Editor(action) = &*a {
            match action {
                Action::NewEvent => {
                    store().dispatch(super::ui::Action::Push(EDITOR_PAGE_NAME));
                }
                Action::Edit(_) => {
                    store().dispatch(super::selection::Action::ClearSelection);
                }
                Action::Undo(event) => {
                    if !state.sync.syncing {
                        if let Err(e) = event.clone().undo().await {
                            dispatch_err(format!("Failed to undo: {e}"));
                        } else {
                            store().dispatch(crate::store::sync::Action::Syncing {
                                report_errors: true,
                            });
                        }
                    }
                }
                Action::Remove(event) => {
                    if let Some(cal) = state
                        .calendar
                        .calendars
                        .iter()
                        .find(|c| c.url() == event.calendar_url())
                    {
                        if !cal.is_remote() {
                            if let Err(e) = async_std::fs::remove_file(event.local_path()).await {
                                dispatch_err(format!(
                                    "Failed to remove local event: {:?} {e}",
                                    event.url()
                                ))
                            } else {
                                store().dispatch(super::index::Action::Index);
                            }
                        } else {
                            if let Err(e) = event.clone().remove().await {
                                dispatch_err(format!("Failed to mark event for removal: {e}"));
                            }
                            store().dispatch(super::index::Action::Remove(
                                event.local_path().as_os_str().to_os_string(),
                            ));

                            let msg = gettext("Removed");
                            let undo = gettext("Undo");
                            let id = format!(
                                "remove-{}",
                                event.url().map(|u| u.as_str()).unwrap_or_default()
                            );
                            let msg = format!(
                                "{}: {}",
                                msg,
                                event.summary().cloned().unwrap_or_default()
                            );
                            store().dispatch(super::notification::Action::InApp {
                                toast: Toast::new(&msg)
                                    .with_id(Some(id))
                                    .with_button(Some(undo))
                                    .with_timeout(Some(Duration::from_secs(4))),
                                dismiss: Some(Box::new(Action::Undo(event.clone()).into())),
                                timeout: Some(Box::new(Action::RemoveTimeout.into())),
                            })
                        }
                    }
                }
                _ => {
                    // ignore
                }
            }
        }
    }
}

pub(crate) const SELECT_EDIT_EVENT_AND_SELECTION: &Selector =
    &|a, b| a.editor.event != b.editor.event || a.selection.selection != b.selection.selection;

use std::{ffi::OsString, sync::Arc, time::SystemTime};

use async_trait::async_trait;

use crate::domain::{get_app_dir, index::Index, Calendar, CALENDARS_PATH, INDEX_FILE};

use super::{dispatch_err, store};

#[derive(Debug, Clone, Eq, PartialEq)]
pub(crate) enum Action {
    Index,
    Indexed(Index, Vec<Calendar>),
    Remove(OsString),
}

impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        Self::Index(value)
    }
}

#[derive(Debug, Default, Clone)]
pub(crate) struct Slice {
    pub(crate) index: Index,
}

pub(super) fn reduce(action: &super::Action, state: &mut super::State) {
    if let super::Action::Index(action) = action {
        match action {
            Action::Indexed(index, _calendars) => {
                state.index.index = index.clone();
            }
            Action::Remove(path) => {
                state.index.index.remove(path);
            }
            _ => {
                // ignore
            }
        }
    }
}

pub(crate) struct Middleware;
#[async_trait]
impl gstore::Middleware<super::Action, super::State> for Middleware {
    async fn pre_reduce(&self, action: Arc<super::Action>, state: Arc<super::State>) {
        if action.as_ref() == &super::Action::Init {
            let calpath: async_std::path::PathBuf = get_app_dir().join(CALENDARS_PATH).into();
            if calpath.exists().await {
                if let Err(e) = async_std::fs::File::create(calpath.join(INDEX_FILE)).await {
                    dispatch_err(format!("Failed to create index file: {:?}", e));
                }
            }
        }
        if action.as_ref() == &super::Action::Index(Action::Index) {
            let calpath: async_std::path::PathBuf = get_app_dir().join(CALENDARS_PATH).into();
            if calpath.exists().await {
                log::debug!("Indexing events");
                match Index::init().await {
                    Ok((index, calendars)) => store().dispatch(Action::Indexed(index, calendars)),
                    Err(e) => dispatch_err(format!("Failed to initialize index: {e}")),
                }
            }
        } else if action.as_ref() == &super::Action::Calendar(super::calendar::Action::Tick) {
            let index_file_path: async_std::path::PathBuf =
                get_app_dir().join(CALENDARS_PATH).join(INDEX_FILE).into();
            if index_file_path.exists().await {
                match index_file_path.metadata().await.and_then(|m| m.modified()) {
                    Ok(m) => match m.duration_since(SystemTime::UNIX_EPOCH) {
                        Ok(timestamp) => {
                            log::debug!(
                                "index timestamp: {}, last known: {}",
                                timestamp.as_millis(),
                                state.index.index.timestamp
                            );
                            if state.index.index.timestamp < timestamp.as_millis() {
                                store().dispatch(Action::Index);
                            }
                        }
                        Err(e) => {
                            dispatch_err(format!("Failed to get index file timestamp: {e}"));
                        }
                    },
                    Err(e) => {
                        log::error!("Failed to index. Could not read metadata: {e}");
                    }
                }
            }
        }
    }
    async fn post_reduce(&self, a: Arc<super::Action>, _: Arc<super::State>) {
        if let super::Action::Index(Action::Indexed(_, _)) = a.as_ref() {
            store().dispatch(super::sync::Action::SyncingDone);
        }
    }
}

pub(crate) const SELECT_INDEX: &super::Selector = &|a, b| a.index.index != b.index.index;

use super::{dispatch_err, store, Selector};
use crate::{
    domain::{
        get_app_dir, time::KarlenderDate, Calendar, Changed, Event, CALENDARS_PATH, CAL_FILE_NAME,
        INDEX_FILE,
    },
    store::login::get_logins,
};
use async_recursion::async_recursion;
use async_trait::async_trait;
use std::{
    collections::{HashMap, HashSet, LinkedList},
    sync::Arc,
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Action {
    Syncing { report_errors: bool },
    SyncingDone,
}
impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        Self::Sync(value)
    }
}
#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub(crate) struct Slice {
    pub(crate) index: HashMap<KarlenderDate, Event>,
    pub(crate) recurring: LinkedList<Event>,
    pub(crate) syncing: bool,
}

pub(super) fn reduce(action: &super::Action, state: &mut super::State) {
    match action {
        super::Action::Sync(Action::Syncing { report_errors: _ }) => {
            state.sync.syncing = true;
        }
        super::Action::Sync(Action::SyncingDone) => {
            state.sync.syncing = false;
        }
        _ => {
            // ignore
        }
    }
}

pub(super) struct Middleware;

#[async_trait]
impl gstore::Middleware<super::Action, super::State> for Middleware {
    async fn pre_reduce(&self, a: Arc<super::Action>, state: Arc<super::State>) {
        if &super::Action::Init == a.as_ref() {
            if state.calendar.calendars.iter().any(|c| c.is_remote()) {
                store().dispatch(Action::Syncing {
                    report_errors: false,
                });
            } else {
                store().dispatch(super::index::Action::Index);
            }
        }
    }

    async fn post_reduce(&self, action: Arc<super::Action>, _: Arc<super::State>) {
        if let super::Action::Sync(action) = action.as_ref() {
            match action {
                Action::Syncing { report_errors } => {
                    sync(*report_errors).await;
                }
                Action::SyncingDone => {
                    // todo
                }
            }
        }
    }
}

async fn save_event(calendar: &Calendar, event: &Event) -> Result<Event, String> {
    let mut login = None;
    if let Ok(logins) = get_logins().await {
        for (l, password) in logins {
            if calendar.login_url() == Some(&l.url) {
                login = Some((l, password));
            }
        }
    }
    if let Some((login, password)) = login {
        if let Some(event) = event.clone().into_inner() {
            if let Some(r) = asyncify(move || {
                let agent = ureq::Agent::new();
                let credentials = to_credentials(&login, &password);
                minicaldav::save_event(agent, &credentials, event.clone())
            })
            .await
            {
                let inner_event = r.map_err(|e| format!("Failed to save event: {e}"))?;
                if let Some(event) = Event::new(calendar, inner_event) {
                    Ok(event)
                } else {
                    Err("Failed to save event: Could not construct event".into())
                }
            } else {
                Err("Failed to call minicaldav".into())
            }
        } else {
            Err("Failed to save event: Event is invalid!".into())
        }
    } else {
        Err("Failed to save event: No login found!".into())
    }
}

async fn remove_event(calendar: &Arc<Calendar>, event: &Event) -> Result<(), String> {
    let mut login = None;
    if let Ok(logins) = get_logins().await {
        for (l, password) in logins {
            if calendar.login_url() == Some(&l.url) {
                login = Some((l, password));
            }
        }
    }
    if let Some((login, password)) = login {
        let local_path = event.local_path().clone();
        if let Some(event) = event.clone().into_inner() {
            if let Some(r) = asyncify(move || {
                let agent = ureq::Agent::new();
                let credentials = to_credentials(&login, &password);
                minicaldav::remove_event(agent, &credentials, event.clone())
            })
            .await
            {
                match r {
                    Ok(_) => {
                        // ok
                        if let Err(e) = async_std::fs::remove_file(local_path).await {
                            log::error!("Failed to remove local event file: {e}");
                        }
                        Ok(())
                    }
                    Err(e) => Err(format!("Failed to remove event: {e}")),
                }
            } else {
                Err("Failed to remove event: No result from server.".into())
            }
        } else {
            Err(format!(
                "Event {:?} has no valid ical data",
                event.local_path()
            ))
        }
    } else {
        Err("Failed to save event: No login found!".into())
    }
}

#[async_recursion]
pub(crate) async fn sync(report_errors: bool) {
    log::debug!("Start syncing.");
    let agent = ureq::Agent::new();

    let mut saved_events = false;

    match crate::store::login::get_logins().await {
        Ok(logins) => {
            for (login, password) in logins {
                let login = Arc::new(login);
                let password = Arc::new(password);

                let calendars =
                    fetch_calendars(agent.clone(), login.clone(), password.clone()).await;
                for calendar in calendars {
                    let mut calendar = crate::domain::Calendar::new(&login, calendar);
                    let cal_path = calendar.local_path().join(CAL_FILE_NAME);
                    if cal_path.exists() {
                        if let Ok(s) = async_std::fs::read_to_string(&cal_path).await {
                            if let Ok(persisted_calendar) = serde_json::from_str::<Calendar>(&s) {
                                // always keep these fields
                                calendar.set_default(persisted_calendar.is_default());
                                calendar.set_enabled(persisted_calendar.enabled());
                            }
                        }
                    }

                    if let Err(e) = calendar.persist().await {
                        if report_errors {
                            dispatch_err(format!("Failed to persist calendar: {e}"));
                        }
                    }
                    let calendar = Arc::new(calendar);

                    let mut handled_remote_events = HashSet::new();
                    if let Some((events, errors)) = fetch_events(
                        agent.clone(),
                        login.clone(),
                        password.clone(),
                        calendar.clone(),
                    )
                    .await
                    {
                        for error in errors {
                            dispatch_err(format!("Failed to sync event: {error}"));
                        }
                        for event in events {
                            handled_remote_events.insert(event.url().to_string());
                            saved_events |= sync_event(&calendar, event).await;
                        }
                    } else if report_errors {
                        dispatch_err(format!("Found no events in calendar {}", calendar.url()));
                    }
                    if let Ok(local_events) = Event::local(&calendar).await {
                        for event in local_events {
                            if let Some(url) = event.url() {
                                if !handled_remote_events.contains(url.as_str()) {
                                    if let Some(ical) = event.clone().into_inner() {
                                        saved_events |= sync_event(&calendar, ical).await;
                                    }
                                }
                            }
                        }
                    }
                }
                let index_file_path = get_app_dir().join(CALENDARS_PATH).join(INDEX_FILE);
                if !index_file_path.exists() {
                    if let Err(e) = async_std::fs::File::create(index_file_path).await {
                        if report_errors {
                            dispatch_err(format!("Could not create index file: {e}"));
                        }
                    }
                }
                if saved_events {
                    // get correct etags via new sync

                    // give server some time to process
                    async_std::task::sleep(std::time::Duration::from_secs(2)).await;

                    sync(report_errors).await
                } else {
                    store().dispatch(crate::store::index::Action::Index);
                }
            }
        }
        Err(e) => {
            if report_errors {
                dispatch_err(format!("Could not sync: {e}"));
            }
        }
    }
}

async fn sync_event(calendar: &Arc<Calendar>, event: minicaldav::Event) -> bool {
    match crate::domain::Event::new(calendar.as_ref(), event.clone()) {
        Some(event) => {
            let local_path: async_std::path::PathBuf = event.local_path().into();
            if local_path.exists().await {
                match Event::parse(&local_path).await {
                    Ok(local_event) => {
                        log::debug!(
                            "Sync Event. local: {:?}, changed: {}, remote: {:?}, name: {:?}",
                            local_event.etag(),
                            local_event.changed(),
                            event.etag(),
                            event.summary(),
                        );
                        match local_event.changed() {
                            Changed::No => {
                                if let Err(e) = event.persist().await {
                                    dispatch_err(format!("Failed to persist event: {e}"));
                                }
                            }
                            Changed::Deleted => {
                                if let Err(e) = remove_event(calendar, &event).await {
                                    dispatch_err(e);
                                }
                            }
                            Changed::Yes => {
                                if local_event.etag() != event.etag() {
                                    dispatch_err(format!(
                                        "Can not sync event '{}': There is a conflict with the server.",
                                        local_event.summary().cloned().unwrap_or_default()
                                    ))
                                } else {
                                    // event is locally changed
                                    log::debug!(
                                        "Updating event on server: {:?} {:?}",
                                        event.summary(),
                                        event.etag()
                                    );
                                    match save_event(calendar, &local_event).await {
                                        Err(e) => dispatch_err(format!(
                                            "Failed to update event on server: {e}"
                                        )),
                                        Ok(event) => {
                                            if let Err(e) = event.persist().await {
                                                dispatch_err(format!(
                                                    "Failed to persist updated event: {e}"
                                                ))
                                            }
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Err(e) => dispatch_err(format!("Failed to parse local event: {e}")),
                }
            } else if let Err(e) = event.persist().await {
                dispatch_err(format!("Failed to persist event: {e}"));
            }
        }
        None => {
            log::warn!("Failed to parse event. It's likely a VTODO and okay to be ignored because this is a calendar app.")
        }
    }
    false
}

async fn fetch_events(
    agent: ureq::Agent,
    login: Arc<crate::domain::Login>,
    password: Arc<crate::domain::Password>,
    calendar: Arc<crate::domain::Calendar>,
) -> Option<(Vec<minicaldav::Event>, Vec<minicaldav::Error>)> {
    asyncify(move || {
        let email = login.email.as_str();
        let credentials = if password.is_token() {
            minicaldav::Credentials::Bearer(password.as_str().to_string())
        } else {
            minicaldav::Credentials::Basic(email.into(), password.as_str().into())
        };
        if let Some(cal) = calendar.cal() {
            match minicaldav::get_events(agent.clone(), &credentials, cal) {
                Ok(r) => Some(r),
                Err(e) => {
                    dispatch_err(format!("Failed to fetch events: {e}"));
                    None
                }
            }
        } else {
            None
        }
    })
    .await
    .flatten()
}

async fn fetch_calendars(
    agent: ureq::Agent,
    login: Arc<crate::domain::Login>,
    password: Arc<crate::domain::Password>,
) -> Vec<minicaldav::Calendar> {
    asyncify(move || {
        let credentials = to_credentials(login.as_ref(), password.as_ref());
        match minicaldav::get_calendars(agent.clone(), &credentials, &login.url) {
            Ok(c) => Some(c),
            Err(e) => {
                dispatch_err(format!("Failed to fetch calendard: {e}"));
                None
            }
        }
    })
    .await
    .flatten()
    .unwrap_or_default()
}

fn to_credentials(
    login: &crate::domain::Login,
    password: &crate::domain::Password,
) -> minicaldav::Credentials {
    let credentials = if password.is_token() {
        minicaldav::Credentials::Bearer(password.as_str().to_string())
    } else {
        minicaldav::Credentials::Basic(login.email.as_str().into(), password.as_str().into())
    };
    credentials
}

async fn asyncify<T: Send + 'static>(sync: impl Fn() -> T + Send + 'static) -> Option<T> {
    let (s, r) = async_std::channel::unbounded::<T>();
    std::thread::spawn(move || {
        let result = sync();
        if let Err(e) = s.send_blocking(result) {
            dispatch_err(format!("Failed to send sync update: {e}"));
        }
    });
    match r.recv().await {
        Ok(r) => Some(r),
        Err(e) => {
            dispatch_err(format!("Failed to receive sync update: {e}"));
            None
        }
    }
}

pub(crate) const SELECT_SYNCING: &Selector = &|a, b| a.sync.syncing != b.sync.syncing;

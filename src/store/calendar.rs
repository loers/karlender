use std::sync::Arc;

use async_trait::async_trait;
use gettextrs::gettext;
use url::Url;

use crate::domain::{
    time::{KarlenderDateTime, KarlenderTimezone},
    Calendar, RemoteOrLocal,
};

use super::{dispatch_err, store, Selector};

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Action {
    Tick,
    New,
    SetDefault(String),
    UpdateName(Url, String),
    UpdateEnable(Url, bool),
    UpdateColor(Url, String),
    Remove(Url),
}
impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        Self::Calendar(value)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub(crate) struct Slice {
    pub(crate) now: KarlenderDateTime,
    pub(crate) calendars: Vec<Calendar>,
}

impl Default for Slice {
    fn default() -> Self {
        Self {
            now: KarlenderTimezone::local().now(),
            calendars: Default::default(),
        }
    }
}

pub(super) fn reduce(action: &super::Action, state: &mut super::State) {
    match action {
        super::Action::Calendar(action) => match action {
            Action::Tick => {
                state.calendar.now = KarlenderTimezone::local().now();
            }
            Action::SetDefault(url) => {
                for cal in state.calendar.calendars.iter_mut() {
                    cal.set_default(cal.url().as_str() == url);
                }
            }
            Action::New => {
                state
                    .calendar
                    .calendars
                    .push(Calendar::new_local(&gettext("New Calendar")));
            }
            Action::UpdateEnable(url, enabled) => {
                for cal in state.calendar.calendars.iter_mut() {
                    if cal.url() == url {
                        cal.set_enabled(*enabled);
                    }
                }
            }
            Action::UpdateName(url, name) => {
                for cal in state.calendar.calendars.iter_mut() {
                    if cal.url() == url {
                        if let RemoteOrLocal::Local(local) = cal.inner_mut() {
                            local.set_name(name);
                        }
                    }
                }
            }
            Action::UpdateColor(url, color) => {
                for cal in state.calendar.calendars.iter_mut() {
                    if cal.url() == url {
                        if let RemoteOrLocal::Local(local) = cal.inner_mut() {
                            local.set_color(color);
                        }
                    }
                }
            }
            Action::Remove(url) => {
                for cal in state.calendar.calendars.iter_mut() {
                    if cal.url() == url {
                        cal.remove();
                    }
                }
            }
        },
        super::Action::Index(super::index::Action::Indexed(_, calendars)) => {
            state.calendar.calendars = calendars.clone();
        }
        _ => (),
    }
}

pub(crate) struct Middleware;
#[async_trait]
impl gstore::Middleware<super::Action, super::State> for Middleware {
    async fn pre_reduce(&self, a: Arc<super::Action>, _: Arc<super::State>) {
        if a.as_ref() == &super::Action::Init {
            async_std::task::spawn(async {
                loop {
                    async_std::task::sleep(std::time::Duration::from_secs(5)).await;
                    store().dispatch(Action::Tick);
                }
            });
        }
    }
    async fn post_reduce(&self, a: Arc<super::Action>, s: Arc<super::State>) {
        if let super::Action::Calendar(action) = a.as_ref() {
            match action {
                Action::SetDefault(_) => {
                    for cal in s.calendar.calendars.iter() {
                        if let Err(e) = cal.persist().await {
                            dispatch_err(format!("Failed to save calendar {}: {}", cal.url(), e));
                        }
                    }
                }
                Action::UpdateEnable(url, _) => {
                    for cal in s.calendar.calendars.iter() {
                        if cal.url() == url {
                            if let Err(e) = cal.persist().await {
                                dispatch_err(format!("Failed to save calendar disable: {e}"))
                            }
                        }
                    }
                }
                Action::Remove(url) => {
                    for cal in s.calendar.calendars.iter() {
                        if cal.url() == url {
                            if let Err(e) = cal.persist().await {
                                dispatch_err(format!(
                                    "Failed to remove calendar {}: {}",
                                    cal.url(),
                                    e
                                ))
                            }
                            cal.try_remove().await;
                        }
                    }
                    store().dispatch(super::index::Action::Index);
                }
                _ => {
                    // ignore
                }
            }
        }
    }
}

pub(crate) const SELECT_CALENDARS: &Selector = &|a, b| a.calendar.calendars != b.calendar.calendars;
pub(crate) const SELECT_NOW: &Selector = &|a, b| a.calendar.now != b.calendar.now;

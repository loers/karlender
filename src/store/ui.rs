use std::{collections::LinkedList, sync::Arc};

use async_trait::async_trait;

use super::{
    selection::{update_title, Selection},
    store, Selector, State,
};

pub(crate) const ABOUT_PAGE_NAME: &str = "about";
pub(crate) const FEEDBACK_PAGE_NAME: &str = "feedback";
pub(crate) const SETTINGS_PAGE_NAME: &str = "settings";
pub(crate) const EDITOR_PAGE_NAME: &str = "editor";
pub(crate) const DAY_PAGE_NAME: &str = "day";
pub(crate) const WEEK_PAGE_NAME: &str = "week";
pub(crate) const MONTH_PAGE_NAME: &str = "month";
pub(crate) const YEAR_PAGE_NAME: &str = "year";

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Action {
    SetMobile(Mobile),
    NavigateSettings,
    NavigateFeedback,
    NavigateAbout,
    Replace(&'static str),
    Push(&'static str),
    Next,
    Back,
    GotoNow,
    GotoNowReset,
    SetTitle(String),
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Mobile {
    Landscape,
    Portrait,
    NoLandscape,
    NoPortrait,
}

impl Mobile {
    pub fn is_mobile(&self) -> bool {
        match self {
            Mobile::Landscape => true,
            Mobile::Portrait => true,
            Mobile::NoLandscape => false,
            Mobile::NoPortrait => false,
        }
    }
}

impl Default for Mobile {
    fn default() -> Self {
        Self::NoPortrait
    }
}

impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        super::Action::UI(value)
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub(crate) struct Slice {
    pub(crate) mobile: Mobile,
    pub(crate) history: LinkedList<&'static str>,
    pub(crate) title: Option<String>,

    pub(crate) goto_now: bool,
}

pub(super) fn reduce(action: &super::Action, state: &mut State) {
    if action == &super::Action::Init {
        state.ui.history.push_back(MONTH_PAGE_NAME);
    }
    if let super::Action::UI(action) = action {
        match action {
            Action::Replace(page) => {
                state.ui.history.clear();
                state.ui.history.push_back(*page);

                match *page {
                    YEAR_PAGE_NAME => {
                        state.ui.title =
                            Some(update_title(state, &super::selection::Action::NextYear));
                    }
                    MONTH_PAGE_NAME => {
                        state.ui.title =
                            Some(update_title(state, &super::selection::Action::NextMonth));
                    }
                    WEEK_PAGE_NAME => {
                        state.ui.title =
                            Some(update_title(state, &super::selection::Action::NextWeek));
                    }
                    DAY_PAGE_NAME => {
                        state.ui.title =
                            Some(update_title(state, &super::selection::Action::NextDay));
                    }
                    _ => {
                        // ignore
                    }
                }

                if *page == DAY_PAGE_NAME && state.selection.selection.is_none() {
                    state.selection.selection =
                        Some(Selection::Day(state.selection.current_day.clone()));
                }
            }
            Action::Push(page) => {
                state.ui.history.push_back(*page);
                state.ui.title = Some(update_title(state, &super::selection::Action::NextMonth));
            }
            Action::SetMobile(mobile) => {
                if state.ui.mobile == Mobile::Portrait && mobile == &Mobile::NoLandscape {
                    return;
                }
                state.ui.mobile = mobile.clone();
            }
            Action::NavigateSettings => {
                let is_already_settings_page = state
                    .ui
                    .history
                    .back()
                    .map(|page| *page == SETTINGS_PAGE_NAME)
                    .unwrap_or_default();
                if !is_already_settings_page {
                    state.ui.history.push_back(SETTINGS_PAGE_NAME);
                }
            }
            Action::NavigateFeedback => {
                let is_already_settings_page = state
                    .ui
                    .history
                    .back()
                    .map(|page| *page == FEEDBACK_PAGE_NAME)
                    .unwrap_or_default();
                if !is_already_settings_page {
                    state.ui.history.push_back(FEEDBACK_PAGE_NAME);
                }
            }
            Action::NavigateAbout => {
                let is_already_about_page = state
                    .ui
                    .history
                    .back()
                    .map(|page| *page == ABOUT_PAGE_NAME)
                    .unwrap_or_default();
                if !is_already_about_page {
                    state.ui.history.push_back(ABOUT_PAGE_NAME);
                }
            }
            Action::Back => {
                if state.ui.history.len() > 1 {
                    state.ui.history.pop_back();
                    state.ui.title =
                        Some(update_title(state, &super::selection::Action::PrevMonth));
                }
            }
            Action::GotoNow => {
                state.ui.goto_now = true;
            }
            Action::GotoNowReset => {
                state.ui.goto_now = false;
            }
            Action::SetTitle(title) => {
                state.ui.title = Some(title.clone());
            }
            _ => {
                // ignore
            }
        }
    }
}

pub(super) struct Middleware;
#[async_trait]
impl gstore::Middleware<super::Action, super::State> for Middleware {
    async fn pre_reduce(&self, action: Arc<super::Action>, state: Arc<super::State>) {
        if let super::Action::UI(action) = action.as_ref() {
            match action {
                Action::Next => {
                    if state.ui.history.back() == Some(&YEAR_PAGE_NAME) {
                        store().dispatch(crate::store::selection::Action::NextYear);
                    } else if state.ui.history.back() == Some(&MONTH_PAGE_NAME) {
                        store().dispatch(crate::store::selection::Action::NextMonth);
                    } else if state.ui.history.back() == Some(&WEEK_PAGE_NAME) {
                        store().dispatch(crate::store::selection::Action::NextWeek);
                    } else if state.ui.history.back() == Some(&DAY_PAGE_NAME) {
                        store().dispatch(crate::store::selection::Action::NextDay);
                    }
                }
                Action::Back => {
                    if state.ui.history.back() == Some(&YEAR_PAGE_NAME) {
                        store().dispatch(crate::store::selection::Action::PrevYear);
                    } else if state.ui.history.back() == Some(&MONTH_PAGE_NAME) {
                        store().dispatch(crate::store::selection::Action::PrevMonth);
                    } else if state.ui.history.back() == Some(&WEEK_PAGE_NAME) {
                        store().dispatch(crate::store::selection::Action::PrevWeek);
                    } else if state.ui.history.back() == Some(&DAY_PAGE_NAME) {
                        store().dispatch(crate::store::selection::Action::PrevDay);
                    }
                }
                _ => {
                    //ignore
                }
            }
        }
    }
    async fn post_reduce(&self, a: Arc<super::Action>, _state: Arc<super::State>) {
        match &*a {
            super::Action::Error(msg) => {
                log::error!("{}", msg);
                store().dispatch(super::notification::Action::Show(msg.clone()));
            }
            super::Action::UI(action) => {
                match action {
                    Action::Replace(WEEK_PAGE_NAME) => {
                        async_std::task::spawn(async {
                            async_std::task::sleep(std::time::Duration::from_millis(100)).await;
                            store().dispatch(super::calendar::Action::Tick)
                        });
                    }
                    Action::GotoNow => store().dispatch(Action::GotoNowReset),
                    _ => {
                        // ignore
                    }
                }
            }
            _ => {
                // ignore
            }
        }
    }
}

pub(crate) const SELECT_MOBILE: &Selector = &|a, b| a.ui.mobile != b.ui.mobile;
pub(crate) const SELECT_HISTORY: &Selector = &|a, b| a.ui.history != b.ui.history;
pub(crate) const SELECT_TITLE: &Selector = &|a, b| a.ui.title != b.ui.title;

pub(crate) const SELECT_HISTORY_AND_MOBILE: &Selector =
    &|a, b| a.ui.mobile != b.ui.mobile || a.ui.history != b.ui.history;

pub(crate) const SELECT_GOTO_NOW: &Selector = &|a, _| a.ui.goto_now;

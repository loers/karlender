use crate::domain::{Login, Password};

use super::{dispatch_err, store, Selector, State};
use async_std::sync::Mutex;
use async_trait::async_trait;
use secret_service::SecretService;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, sync::Arc};

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Action {
    Login(Login),
    LoggedIn(Login),
    Logout,
}
impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        super::Action::Login(value)
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq, Serialize, Deserialize)]
pub(crate) struct Slice {
    pub(crate) login: Option<Login>,
}

static SECRET_SERVICE: gstore::once_cell::sync::Lazy<Mutex<Option<SecretService>>> =
    gstore::once_cell::sync::Lazy::new(Default::default);

pub(crate) static PASSWORD: gstore::once_cell::sync::Lazy<Mutex<Option<String>>> =
    gstore::once_cell::sync::Lazy::new(Default::default);

pub(super) fn reduce(action: &super::Action, state: &mut State) {
    if action == &super::Action::Init {
        // todo
    }
    if let super::Action::Login(action) = action {
        match action {
            Action::LoggedIn(login) => {
                state.login.login = Some(login.clone());
            }
            Action::Logout => {
                state.login.login = None;
            }
            _ => {
                // nothing to do
            }
        }
    }
}

pub(super) struct Middleware;
#[async_trait]
impl gstore::Middleware<super::Action, State> for Middleware {
    async fn pre_reduce(&self, action: Arc<super::Action>, _: Arc<State>) {
        if super::Action::Init == *action {
            match SecretService::connect(secret_service::EncryptionType::Dh).await {
                Ok(serv) => {
                    let mut lock = SECRET_SERVICE.lock().await;
                    *lock = Some(serv);
                }
                Err(e) => dispatch_err(format!("Failed to initialize secret store: {e}")),
            }

            if let Ok(logins) = get_logins().await {
                for (login, _) in logins {
                    if login.is_karlender_login {
                        store().dispatch(Action::LoggedIn(login))
                    }
                }
            }
        }

        if let super::Action::Login(action) = &*action {
            match action {
                Action::Login(login) => {
                    let mut lock = SECRET_SERVICE.lock().await;

                    if lock.is_none() {
                        match SecretService::connect(secret_service::EncryptionType::Dh).await {
                            Ok(serv) => {
                                *lock = Some(serv);
                            }
                            Err(e) => {
                                dispatch_err(format!("Failed to initialize secret store: {e}"))
                            }
                        }
                    }

                    match lock.as_ref() {
                        Some(serv) => {
                            let password = PASSWORD.lock().await.take();
                            if let Some(password) = password {
                                if save_login_and_secret(serv, login, password.as_bytes()).await {
                                    store().dispatch(Action::LoggedIn(login.clone()));
                                }
                            } else {
                                dispatch_err("Internal error while processing login.");
                            }
                        }
                        None => dispatch_err("Failed to login: Keyring not initialized."),
                    }
                }
                Action::Logout => {
                    let mut lock = SECRET_SERVICE.lock().await;
                    match lock.as_ref() {
                        Some(serv) => {
                            remove_login(serv).await;
                        }
                        None => dispatch_err("Failed to logout: Keyring not initialized."),
                    }
                    *lock = None;
                }
                _ => (),
            }
        }
    }

    async fn post_reduce(&self, _a: Arc<super::Action>, _s: Arc<State>) {
        // nothing to do
    }
}

pub(crate) const SELECT_LOGIN: &Selector = &|a, b| a.login.login != b.login.login;

const SECRET_ENTRY_NAME: &str = "karlender";
const LABEL_KEY: &str = "label";
const URL_KEY: &str = "url";
const EMAIL_KEY: &str = "email";

async fn save_login_and_secret<'a>(
    service: &SecretService<'a>,
    login: &Login,
    secret: &[u8],
) -> bool {
    let mut attributes = HashMap::new();
    attributes.insert(LABEL_KEY, SECRET_ENTRY_NAME);
    attributes.insert(URL_KEY, login.url.as_str());
    attributes.insert(EMAIL_KEY, login.email.as_str());

    match service.get_default_collection().await {
        Ok(c) => {
            match c
                .create_item(SECRET_ENTRY_NAME, attributes, secret, true, "Password")
                .await
            {
                Ok(_) => return true,
                Err(e) => dispatch_err(format!("Failed to save secret in store: {e}")),
            }
        }
        Err(e) => dispatch_err(format!("Failed to get secrects collection: {e}")),
    }
    #[allow(clippy::needless_return)]
    return false;
}

async fn remove_login<'a>(service: &SecretService<'a>) {
    if let Ok(c) = service.get_default_collection().await {
        let mut attributes = HashMap::new();
        attributes.insert("label", "karlender");
        if let Ok(items) = c.search_items(attributes).await {
            for item in items {
                match item.delete().await {
                    Ok(_) => {
                        // Ok
                    }
                    Err(e) => dispatch_err(format!("Failed to clear secret store: {e}")),
                }
            }
        }
    }
}

pub(super) async fn get_logins() -> Result<Vec<(Login, Password)>, String> {
    let mut errors = Vec::new();
    let mut logins = Vec::new();

    match get_login_from_secret_service().await {
        Ok(login) => {
            logins.push(login);
        }
        Err(e) => {
            errors.push(e);
            // log::error!("Failed to get login from secret service", e);
        }
    }

    if let Ok(gnome_logins) = get_logins_from_gnome().await {
        logins.extend(gnome_logins);
    }

    Ok(logins)
}

async fn get_logins_from_gnome() -> Result<Vec<(Login, Password)>, String> {
    let accounts = gnome_online_accounts_rs::get_accounts();
    let mut logins: Vec<(Login, Password)> = Vec::new();
    for account in accounts {
        if let Some(calendar) = &account.calendar {
            match Login::new(&calendar.uri, &account.identity) {
                Ok(login) => {
                    if account.oauth2 {
                        if let Some(token) = gnome_online_accounts_rs::get_token(&account.id) {
                            let password = Password::token(token);
                            logins.push((login, password))
                        } else {
                            log::error!(
                                "gnome online account {} has no calendar information.",
                                &account.id
                            );
                        }
                    }
                }
                Err(e) => log::error!("Failed to construct login from gnome online accounts: {e}"),
            }
        }
    }

    Ok(logins)
}

pub(super) async fn get_login_from_secret_service() -> Result<(Login, Password), String> {
    match SECRET_SERVICE.lock().await.as_ref() {
        Some(service) => match service.get_default_collection().await {
            Ok(c) => {
                let mut attributes = HashMap::new();
                attributes.insert("label", "karlender");
                match c
                    .search_items(attributes)
                    .await
                    .map(|items| items.into_iter().next())
                {
                    Ok(Some(item)) => match item.get_attributes().await {
                        Ok(attr) => {
                            let email = attr.get(EMAIL_KEY).or(attr.get("login"));
                            let url = attr.get(URL_KEY);
                            let password = item
                                .get_secret()
                                .await
                                .ok()
                                .and_then(|p| String::from_utf8(p).ok());
                            if email.is_none() || url.is_none() || password.is_none() {
                                Err("Invalid content in karlender secret entry".into())
                            } else {
                                #[allow(clippy::unnecessary_unwrap)]
                                let password = password.unwrap().into();
                                #[allow(clippy::unnecessary_unwrap)]
                                match Login::new(url.unwrap(), email.unwrap()) {
                                    Ok(mut login) => {
                                        login.is_karlender_login = true;
                                        Ok((login, password))
                                    }
                                    Err(e) => Err(e),
                                }
                            }
                        }
                        Err(e) => Err(format!("Failed to read attributes from secret item: {e}")),
                    },
                    Ok(None) => Err("No login saved in keyring.".into()),
                    Err(e) => Err(format!("Failed to read secret store: {e}")),
                }
            }
            Err(e) => Err(format!(
                "Could not read default collection in secret store: {e}"
            )),
        },
        None => Err("Can not access secret service.".into()),
    }
}

pub(super) async fn _logout() {
    if let Some(service) = SECRET_SERVICE.lock().await.as_ref() {
        match service.get_default_collection().await {
            Ok(c) => {
                let mut attributes = HashMap::new();
                attributes.insert("label", "karlender");
                match c
                    .search_items(attributes)
                    .await
                    .map(|items| items.into_iter().next())
                {
                    Ok(Some(item)) => {
                        if let Err(e) = item.delete().await {
                            dispatch_err(format!("Failed to delete login data: {e}"));
                        }
                    }
                    Ok(None) => {
                        log::warn!("Could not delete login: No login in keystore.")
                    }
                    Err(e) => {
                        dispatch_err(format!(
                            "Could not delete login. Failed to read secret store: {e}"
                        ));
                    }
                }
            }
            Err(e) => {
                dispatch_err(format!("Could not delete login. No collection found: {e}"));
            }
        }
    }
}

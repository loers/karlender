use crate::domain::time::FormatKey;
use async_trait::async_trait;
use std::sync::Arc;

#[derive(Debug, Clone, Eq, PartialEq)]
pub(crate) enum Action {
    SetFormat(FormatKey),
}

impl From<Action> for super::Action {
    fn from(value: Action) -> Self {
        Self::Settings(value)
    }
}

#[derive(Debug, Default, Clone)]
pub(crate) struct Slice {
    pub(crate) date_format: FormatKey,
}

pub(super) fn reduce(action: &super::Action, state: &mut super::State) {
    if let super::Action::Settings(action) = action {
        match action {
            Action::SetFormat(fmt) => {
                state.settings.date_format = fmt.clone();
            }
        }
    }
}

pub(crate) struct Middleware;
#[async_trait]
impl gstore::Middleware<super::Action, super::State> for Middleware {
    async fn pre_reduce(&self, _: Arc<super::Action>, _: Arc<super::State>) {
        // ignore
    }
    async fn post_reduce(&self, _: Arc<super::Action>, _: Arc<super::State>) {
        // ignore
    }
}

// pub(crate) const SELECT_DATE_FORMAT: &'static super::Selector = &|a, b| a.settings.date_format != b.settings.date_format;

use std::rc::Rc;

use gettextrs::gettext;
use glib::clone::Downgrade;
use grx::combo_row::ComboRow;
use grx::components::*;
use grx::entry::Entry;
use grx::expander_row::ExpanderRow;
use grx::item::Item;
use grx::spin_button::SpinButton;
use grx::switch::Switch;
use grx::{component, grx, props};

use crate::domain::time::{KarlenderDateTime, KarlenderDateTimeDuration, KarlenderTimezone};
use crate::domain::{get_app_dir, DateField, Field};
use crate::store::calendar::SELECT_CALENDARS;
use crate::store::editor::{Action, SELECT_EDIT_EVENT_AND_SELECTION};
use crate::store::store;
use crate::store::ui::EDITOR_PAGE_NAME;
use crate::ui::repeat_row;

use super::repeat_row::RepeatRow;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct EditorPage {}

pub(crate) const NAME: &str = EDITOR_PAGE_NAME;

pub(crate) fn editor_page(props: Props) -> Rc<EditorPage> {
    let calendars_row: Rc<ComboRow>;
    let start_row: Rc<ExpanderRow>;
    let end_row: Rc<ExpanderRow>;
    let repeat_expander_row: Rc<ExpanderRow>;
    let repeat_rows: Rc<RepeatRow>;

    let name = gettext("Summary");
    let start = gettext("Start");
    let end = gettext("End");
    let location = gettext("Location");
    let filepath = gettext("Path");
    let calendar = gettext("Calendar");
    let schedule = gettext("Schedule");
    let fullday = gettext("Full day");
    let repeat = gettext("Repeat");
    let save = gettext("Save");

    let filepath_text: Rc<Text>;
    let summary_entry: Rc<Entry>;
    let location_entry: Rc<Entry>;

    let start_day_sb: Rc<SpinButton>;
    let start_month_sb: Rc<SpinButton>;
    let start_year_sb: Rc<SpinButton>;
    let start_hour_sb: Rc<SpinButton>;
    let start_minute_sb: Rc<SpinButton>;

    let end_day_sb: Rc<SpinButton>;
    let end_month_sb: Rc<SpinButton>;
    let end_year_sb: Rc<SpinButton>;
    let end_hour_sb: Rc<SpinButton>;
    let end_minute_sb: Rc<SpinButton>;

    let full_day_switch: Rc<Switch>;
    let repeat_switch: Rc<Switch>;

    let notes: Rc<TextArea>;

    let start_time_container: Rc<Container>;
    let end_time_container: Rc<Container>;

    let component = grx! {
        container (styles=[vexpand, hexpand, vertical]) [
            scroll (styles=[vexpand, hexpand]) [
                clamp (styles=[vexpand, hexpand, m(16)]) [
                    container (styles=[hexpand, vertical, spacing(8)]) [
                        list (classes="boxed-list".into()) [

                            action_row(title=name, selectable=Some(false), activatable=Some(false), expand_suffix=Some(true)) [
                                entry (id=summary_entry, styles=[valign(center), hexpand], on_blur=|e| {
                                    let text = e.text();
                                    async_std::task::spawn(async {
                                        async_std::task::sleep(std::time::Duration::from_millis(100)).await;
                                        store().dispatch(Action::Update(Field::Summary(text)));
                                    });
                                })
                            ],

                            action_row(title=location, selectable=Some(false), activatable=Some(false), expand_suffix=Some(true)) [
                                entry (id=location_entry, styles=[valign(center), hexpand], on_blur=|e| {
                                    let text = e.text();
                                    async_std::task::spawn(async {
                                        async_std::task::sleep(std::time::Duration::from_millis(100)).await;
                                        store().dispatch(Action::Update(Field::Location(text)));
                                    });
                                })
                            ],

                            combo_row(
                                id=calendars_row,
                                title=calendar,
                                selectable=Some(false),
                                activatable=Some(false)
                            ),

                            action_row(
                                title=filepath,
                                activatable=Some(false),
                                selectable=Some(false),
                                expand_suffix=Some(true)
                            ) [
                                text(id=filepath_text, classes="caption".into(), styles=[wrap(word_char), hexpand, selectable])
                            ]
                        ],

                        text(text=schedule, styles=[mt(8), halign(start), text_size("l".into())]),

                        list (classes="boxed-list".into()) [

                            action_row(title=fullday, selectable=Some(false), activatable=Some(false)) [
                                switch (id=full_day_switch, styles=[valign(center)])
                            ],
                            expander_row(id=start_row, title=start, selectable=Some(false), activatable=Some(false), expanded=Some(true)) [
                                container (styles=[horizontal, halign(end), spacing(4)]) [
                                    spin_button (id=start_day_sb, styles=[vertical, halign(end), w(40)], value=1.0, upper=31.0, lower=1.0, step=1.0, page=10.0,
                                        on_change=|sb| {
                                            store().dispatch(crate::store::editor::Action::Update(Field::Start(DateField::Day, sb.value() as u32)));
                                        }
                                    ),
                                    spin_button (id=start_month_sb, styles=[vertical, halign(end), w(40)], value=1.0, upper=12.0, lower=1.0, step=1.0, page=4.0,
                                        value_mapper=Some((
                                            Rc::new(get_month_str),
                                            Rc::new(get_month_val),
                                        )),
                                        on_change=|sb| {
                                            store().dispatch(crate::store::editor::Action::Update(Field::Start(DateField::Month, sb.value() as u32)));
                                        }
                                    ),
                                    spin_button (id=start_year_sb, styles=[vertical, halign(end), w(60)], value=2023.0, upper=9999.0, lower=1960.0, step=1.0, page=10.0,
                                        on_change=|sb| {
                                            store().dispatch(crate::store::editor::Action::Update(Field::Start(DateField::Year, sb.value() as u32)));
                                        }
                                    ),

                                    container (id=start_time_container, styles=[spacing(4)]) [
                                        spin_button (id=start_hour_sb, styles=[vertical, halign(end), w(40), ml(12)], value=0.0, upper=23.0, lower=0.0, step=1.0, page=10.0,
                                            on_change=|sb| {
                                                store().dispatch(crate::store::editor::Action::Update(Field::Start(DateField::Hour, sb.value() as u32)));
                                            }
                                        ),
                                        ":",
                                        spin_button (id=start_minute_sb, styles=[vertical, halign(end), w(40)], value=0.0, upper=59.0, lower=0.0, step=1.0, page=10.0,
                                            on_change=|sb| {
                                                store().dispatch(crate::store::editor::Action::Update(Field::Start(DateField::Minute, sb.value() as u32)));
                                            }
                                        )
                                    ]
                                ]
                            ],
                            expander_row(id=end_row,  title=end, selectable=Some(false), activatable=Some(false), expanded=Some(false)) [
                                container (styles=[horizontal, halign(end), spacing(4)]) [
                                    spin_button (id=end_day_sb, styles=[vertical, halign(end), w(40)], value=1.0, upper=31.0, lower=1.0, step=1.0, page=10.0,
                                        on_change=|sb| {
                                            store().dispatch(crate::store::editor::Action::Update(Field::End(DateField::Day, sb.value() as u32)));
                                        }
                                    ),
                                    spin_button (id=end_month_sb, styles=[vertical, halign(end), w(40)], value=1.0, upper=12.0, lower=1.0, step=1.0, page=4.0,
                                        value_mapper=Some((
                                            Rc::new(get_month_str),
                                            Rc::new(get_month_val),
                                        )),
                                        on_change=|sb| {
                                            store().dispatch(crate::store::editor::Action::Update(Field::End(DateField::Month, sb.value() as u32)));
                                        }
                                    ),
                                    spin_button (id=end_year_sb, styles=[vertical, halign(end), w(60)], value=2023.0, upper=9999.0, lower=1960.0, step=1.0, page=10.0,
                                        on_change=|sb| {
                                            store().dispatch(crate::store::editor::Action::Update(Field::End(DateField::Year, sb.value() as u32)));
                                        }
                                    ),

                                    container (id=end_time_container, styles=[spacing(4)]) [
                                        spin_button (id=end_hour_sb, styles=[vertical, halign(end), w(40), ml(12)], value=0.0, upper=23.0, lower=0.0, step=1.0, page=10.0,
                                            on_change=|sb| {
                                                store().dispatch(crate::store::editor::Action::Update(Field::End(DateField::Hour, sb.value() as u32)));
                                            }
                                        ),
                                        ":",
                                        spin_button (id=end_minute_sb, styles=[vertical, halign(end), w(40)], value=0.0, upper=59.0, lower=0.0, step=1.0, page=10.0,
                                            on_change=|sb| {
                                                store().dispatch(crate::store::editor::Action::Update(Field::End(DateField::Minute, sb.value() as u32)));
                                            }
                                        )

                                    ]
                                ]
                            ],

                            expander_row(id=repeat_expander_row, title=repeat, selectable=Some(false), activatable=Some(false), expanded=Some(false)) [
                                #[suffix=true]
                                switch (id=repeat_switch, styles=[valign(center)]),
                                repeat_row (id=repeat_rows)
                            ]
                        ],

                        text_area (id=notes, disabled=true),

                        button (on_click=|| {
                            let _a = async_std::task::spawn(async {
                                async_std::task::sleep(std::time::Duration::from_millis(101)).await;
                                store().dispatch(crate::store::editor::Action::Save)
                            });
                        }) [
                            text(text=save)
                        ]

                    ]
                ]
            ]
        ]
    };

    let rer = repeat_expander_row.downgrade();
    let rr = repeat_rows.downgrade();
    repeat_switch.on_change(move |s| {
        if let Some(repeat_rows) = rr.upgrade() {
            if let Some(repeat_expander_row) = rer.upgrade() {
                repeat_expander_row.set_expanded(s.state());
                repeat_expander_row.set_subtitle(&repeat_rows.get_subtitle());
                if !s.state() {
                    repeat_expander_row.set_subtitle("");
                }
            }
            repeat_rows.notify();
        }
    });

    let s = repeat_switch.downgrade();
    let rer = repeat_expander_row.downgrade();
    repeat_rows.on_change(move |r| {
        if let Some(repeat_switch) = s.upgrade() {
            if let Some(repeat_expander_row) = rer.upgrade() {
                if repeat_switch.state() {
                    repeat_expander_row.set_subtitle(&r.get_subtitle())
                }
            }
        }
        let value = r.value();
        store().dispatch(Action::Update(Field::RRule(value)));
    });

    let weak_er = end_row.downgrade();
    full_day_switch.on_change(move |s| {
        if let Some(end_row) = weak_er.upgrade() {
            start_time_container.set_visible(!s.state());
            end_time_container.set_visible(!s.state());
            end_row.set_expanded(!s.state());
            store().dispatch(Action::Update(Field::FullDay(s.state())));
        }
    });

    let default = String::new();
    let cal_row = calendars_row.clone();

    notes.on_change(|c| store().dispatch(Action::Update(Field::Notes(c.text()))));

    let page = EditorPage::new(props, component);

    page.later_drop(store().select_dbg(
        "editor_page.select(SELECT_EDIT_EVENT_AND_SELECTION)",
        SELECT_EDIT_EVENT_AND_SELECTION,
        move |state| {
            if let Some(event) = state.editor.event.as_ref() {
                let summary = event.summary().unwrap_or(&default);
                if summary_entry.text().as_str() != summary {
                    summary_entry.set_text(summary);
                }

                let desc = event.description().unwrap_or(&default);
                if notes.text().as_str() != desc {
                    notes.set_text(desc);
                }

                if let Some(path) = event
                    .local_path()
                    .strip_prefix(get_app_dir())
                    .ok()
                    .and_then(|p| p.to_str())
                {
                    filepath_text.set_text(path);
                }

                let location = event.location().unwrap_or(&default);
                if location_entry.text().as_str() != location {
                    location_entry.set_text(location);
                }

                cal_row.select(event.calendar_url().as_str());

                let day = state
                    .selection
                    .selection
                    .as_ref()
                    .map(|d| match d {
                        crate::store::selection::Selection::Day(day) => day.clone(),
                        crate::store::selection::Selection::Timeslot(day) => day.date(),
                    })
                    .unwrap_or_else(|| KarlenderTimezone::local().today());

                let now = KarlenderTimezone::local().now();

                let default_start =
                    day.and_hms(now.hour(), 0, 0, Some(&KarlenderTimezone::local()));

                let default_end = day.and_hms(now.hour(), 0, 0, Some(&KarlenderTimezone::local()))
                    + KarlenderDateTimeDuration::minutes(60);

                full_day_switch.set_state(event.is_full_day());

                let mut start = event.start_local().unwrap_or(default_start);
                let mut end = event.end_local().unwrap_or(default_end);

                if start >= end {
                    if start_day_sb.has_focus() {
                        end = start.date().and_hms(
                            end.hour(),
                            end.minute(),
                            0,
                            Some(&KarlenderTimezone::local()),
                        ) + KarlenderDateTimeDuration::days(1);
                    } else if end_day_sb.has_focus() {
                        start = end.date().and_hms(
                            start.hour(),
                            start.minute(),
                            0,
                            Some(&KarlenderTimezone::local()),
                        ) - KarlenderDateTimeDuration::days(1);
                    } else if start_month_sb.has_focus() {
                        end = start.date().and_hms(
                            end.hour(),
                            end.minute(),
                            0,
                            Some(&KarlenderTimezone::local()),
                        ) + KarlenderDateTimeDuration::days(31);
                    } else if end_month_sb.has_focus() {
                        start = end.date().and_hms(
                            start.hour(),
                            start.minute(),
                            0,
                            Some(&KarlenderTimezone::local()),
                        ) - KarlenderDateTimeDuration::days(31);
                    } else if start_year_sb.has_focus() {
                        end = start.date().and_hms(
                            end.hour(),
                            end.minute(),
                            0,
                            Some(&KarlenderTimezone::local()),
                        ) + KarlenderDateTimeDuration::days(365);
                    } else if end_year_sb.has_focus() {
                        start = end.date().and_hms(
                            start.hour(),
                            start.minute(),
                            0,
                            Some(&KarlenderTimezone::local()),
                        ) - KarlenderDateTimeDuration::days(365);
                    }
                }

                if full_day_switch.state() {
                    start_row.set_subtitle(
                        &start.format(crate::domain::time::DatetimeFormat::DateBottomUp),
                    );
                    end_row.set_subtitle(
                        &end.format(crate::domain::time::DatetimeFormat::DateBottomUp),
                    );
                } else {
                    start_row.set_subtitle(
                        &start.format(crate::domain::time::DatetimeFormat::DateBottomUpAndTime24),
                    );
                    end_row.set_subtitle(
                        &end.format(crate::domain::time::DatetimeFormat::DateBottomUpAndTime24),
                    );
                }

                update_date_controls(
                    start,
                    end,
                    &start_day_sb,
                    &start_month_sb,
                    &start_year_sb,
                    &start_hour_sb,
                    &start_minute_sb,
                    &end_day_sb,
                    &end_month_sb,
                    &end_year_sb,
                    &end_hour_sb,
                    &end_minute_sb,
                );
            }
        },
    ));

    calendars_row.on_change(|c| {
        if let Some(s) = c.selected() {
            store().dispatch(Action::Update(Field::Calendar(s.id())))
        }
    });

    page.later_drop(store().select_dbg(
        "editor_page.selecet(SELECT_CALENDARS)",
        SELECT_CALENDARS,
        move |state| {
            calendars_row.clear();
            for calendar in state.calendar.calendars.iter() {
                let item = Rc::new(Item::new(calendar.url().as_str(), calendar.name()));
                calendars_row.append(item);
                if calendar.is_default() {
                    calendars_row.select(calendar.url().as_str());
                }
            }
        },
    ));

    page
}

#[allow(clippy::too_many_arguments)]
fn update_date_controls(
    start: KarlenderDateTime,
    end: KarlenderDateTime,
    start_day_sb: &Rc<SpinButton>,
    start_month_sb: &Rc<SpinButton>,
    start_year_sb: &Rc<SpinButton>,
    start_hour_sb: &Rc<SpinButton>,
    start_minute_sb: &Rc<SpinButton>,
    end_day_sb: &Rc<SpinButton>,
    end_month_sb: &Rc<SpinButton>,
    end_year_sb: &Rc<SpinButton>,
    end_hour_sb: &Rc<SpinButton>,
    end_minute_sb: &Rc<SpinButton>,
) {
    let zoned_start = start;

    if start_day_sb.value() != zoned_start.day() as f64 {
        start_day_sb.set_value(zoned_start.day() as f64);
    }
    if start_month_sb.value() != zoned_start.month() as f64 {
        start_month_sb.set_value(zoned_start.month() as f64);
    }
    if start_year_sb.value() != zoned_start.year() as f64 {
        start_year_sb.set_value(zoned_start.year() as f64);
    }

    if start_hour_sb.value() != zoned_start.hour() as f64 {
        start_hour_sb.set_value(zoned_start.hour() as f64);
    }
    if start_minute_sb.value() != zoned_start.minute() as f64 {
        start_minute_sb.set_value(zoned_start.minute() as f64);
    }
    let zoned_end = end;

    if end_day_sb.value() != zoned_end.day() as f64 {
        end_day_sb.set_value(zoned_end.day() as f64);
    }
    if end_month_sb.value() != zoned_end.month() as f64 {
        end_month_sb.set_value(zoned_end.month() as f64);
    }
    if end_year_sb.value() != zoned_end.year() as f64 {
        end_year_sb.set_value(zoned_end.year() as f64);
    }

    if end_hour_sb.value() != zoned_end.hour() as f64 {
        end_hour_sb.set_value(zoned_end.hour() as f64);
    }
    if end_minute_sb.value() != zoned_end.minute() as f64 {
        end_minute_sb.set_value(zoned_end.minute() as f64);
    }
}

pub(super) fn get_month_str(value: f64) -> String {
    match value as u32 {
        1 => gettext("Jan"),
        2 => gettext("Feb"),
        3 => gettext("Mar"),
        4 => gettext("Apr"),
        5 => gettext("May"),
        6 => gettext("Jun"),
        7 => gettext("Jul"),
        8 => gettext("Aug"),
        9 => gettext("Sep"),
        10 => gettext("Oct"),
        11 => gettext("Nov"),
        12 => gettext("Dec"),
        _ => "".into(),
    }
}

pub(super) fn get_month_val(month: String) -> Option<f64> {
    if month == gettext("Jan") {
        return Some(1.0);
    } else if month == gettext("Feb") {
        return Some(2.0);
    } else if month == gettext("Mar") {
        return Some(3.0);
    } else if month == gettext("Apr") {
        return Some(4.0);
    } else if month == gettext("May") {
        return Some(5.0);
    } else if month == gettext("Jun") {
        return Some(6.0);
    } else if month == gettext("Jul") {
        return Some(7.0);
    } else if month == gettext("Aug") {
        return Some(8.0);
    } else if month == gettext("Sep") {
        return Some(9.0);
    } else if month == gettext("Oct") {
        return Some(10.0);
    } else if month == gettext("Nov") {
        return Some(11.0);
    } else if month == gettext("Dev") {
        return Some(12.0);
    }
    #[allow(clippy::needless_return)]
    return None;
}

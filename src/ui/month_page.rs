use std::rc::Rc;

use grx::components::*;
use grx::{component, grx, props};

use crate::store::selection::SELECT_CURRENT_MONTH;
use crate::store::store;
use crate::store::ui::MONTH_PAGE_NAME;
use crate::ui::month_grid::{self, MonthGrid};

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct MonthPage {}

pub(crate) const NAME: &str = MONTH_PAGE_NAME;

pub(crate) fn month_page(props: Props) -> Rc<MonthPage> {
    let month_grid: Rc<MonthGrid>;
    let component = grx! {
        container (styles=[vertical, spacing(10)]) [
            month_grid (id=month_grid)
        ]
    };

    let page = MonthPage::new(props, component);

    page.later_drop(store().select_dbg(
        "month_page.select(SELECT_CURRENT_MONTH)",
        SELECT_CURRENT_MONTH,
        move |state| {
            month_grid.set_month(state.selection.current_month.clone());
        },
    ));

    page
}

use grx::{component, container, grx, props, Component};

use crate::ui::{header, main};

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct Root {}

pub(crate) fn root() -> Component {
    grx! {
        container (styles=[h(100), vertical]) [
            header,
            main
        ]
    }
}

use std::rc::Rc;

use crate::store::selection::SELECT_SELECTION_AND_INDEX;
use crate::store::ui::{Action, Mobile, SELECT_HISTORY, SELECT_MOBILE};
use crate::ui::{day_page, editor_page, month_page, week_page};
use crate::{
    store::{selection::Selection, store},
    ui::list_event,
};
use gettextrs::gettext;
use grx::clamp::Clamp;
use grx::{component, components::*, container, grx, props, Style};

use super::year_page;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct Sidebar {}

const WIDTH_BIG: u32 = 260;
const WIDTH_SMALL: u32 = 160;

pub(crate) fn sidebar(props: Props) -> Rc<Sidebar> {
    let sidebar_root: Rc<Container>;
    let text: Rc<Text>;
    let clamp: Rc<Clamp>;
    let list: Rc<Container>;
    let month_row: Rc<ActionRow>;
    let week_row: Rc<ActionRow>;
    let day_row: Rc<ActionRow>;
    let year_row: Rc<ActionRow>;

    let month = gettext("Month");
    let week = gettext("Week");
    let day = gettext("Day");
    let year = gettext("Year");

    let c = grx! {
        container(id=sidebar_root, styles=[vertical, w(WIDTH_BIG)]) [
            clamp (id=clamp, max_width=WIDTH_BIG) [
                leaflet (
                    styles=[vertical],
                    fold_threshold_natural=true,
                    on_fold=|l| {
                        if l.is_folded() {
                            store().dispatch(Action::SetMobile(Mobile::Landscape))
                        } else {
                            store().dispatch(Action::SetMobile(Mobile::NoLandscape))
                        }
                    }
                ) [
                    list (styles=[h(200)],  classes="navigation-sidebar".into()) [
                        action_row (id=month_row, title=month, selectable=Some(false), activatable=Some(true), on_click=|| {
                            store().dispatch(crate::store::ui::Action::Replace(month_page::NAME));
                        }) [
                            #[prefix=true]
                            icon("month-symbolic")
                        ],
                        action_row (id=week_row, title=week, selectable=Some(false), activatable=Some(true), on_click=|| {
                            store().dispatch(crate::store::ui::Action::Replace(week_page::NAME));
                        }) [
                            #[prefix=true]
                            icon("week-symbolic")
                        ],
                        action_row (id=day_row, title=day, selectable=Some(false), activatable=Some(true), on_click=|| {
                            store().dispatch(crate::store::ui::Action::Replace(day_page::NAME));
                        }) [
                            #[prefix=true]
                            icon("view-list-bullet-symbolic")
                        ],
                        action_row (id=year_row, title=year, selectable=Some(false), activatable=Some(true), on_click=|| {
                            store().dispatch(crate::store::ui::Action::Replace(year_page::NAME));
                        }) [
                            #[prefix=true]
                            icon("year-symbolic")
                        ]

                    ],
                    text(id=text, styles=[border_t(1), pt(4)]),
                    container (id=list, styles=[h(200), spacing(8), m(8), vexpand, vertical])
                ]
            ]
        ]
    };
    let sidebar = Sidebar::new(props, c);

    sidebar.later_drop(store().select_dbg(
        "sidebar.select(SELECT_SELECTION_AND_INDEX)",
        SELECT_SELECTION_AND_INDEX,
        move |state| {
            if let Some(Selection::Day(day)) = state.selection.selection.as_ref() {
                text.set_text(&day.format(crate::domain::time::DatetimeFormat::DateBottomUp));
                list.clear();
                for ev in state.index.index.get_events(day) {
                    list.append(
                    grx! { list_event (event={Some(ev)}, on_click=Some(Rc::new(|e| {
                            if let Some(event) = e.event() {
                                store().dispatch(crate::store::editor::Action::Edit(event.clone()));
                            }
                            store().dispatch(crate::store::ui::Action::Push(editor_page::NAME))
                        })
                    ))},
                )
                }
            } else {
                text.set_text("");
                list.clear();
            }
        },
    ));

    sidebar.later_drop(store().select_dbg(
        "sidebar.select(SELECT_MOBILE)",
        SELECT_MOBILE,
        move |state| {
            if state.ui.mobile == Mobile::Landscape {
                clamp.set_max_witdth(WIDTH_SMALL);
                sidebar_root.set_styles(vec![Style::w(grx::styles::Modifier::none, WIDTH_SMALL)]);
            } else {
                clamp.set_max_witdth(WIDTH_BIG);
                sidebar_root.set_styles(vec![Style::w(grx::styles::Modifier::none, WIDTH_BIG)]);
            }
        },
    ));

    sidebar.later_drop(store().select_dbg(
        "sidebar.select(SELECT_HISTORY)",
        SELECT_HISTORY,
        move |state| {
            if let Some(page) = state.ui.history.back() {
                match *page {
                    month_page::NAME => {
                        month_row.add_class("selected");
                        week_row.remove_class("selected");
                        day_row.remove_class("selected");
                        year_row.remove_class("selected");
                    }
                    week_page::NAME => {
                        month_row.remove_class("selected");
                        week_row.add_class("selected");
                        day_row.remove_class("selected");
                        year_row.remove_class("selected");
                    }
                    day_page::NAME => {
                        month_row.remove_class("selected");
                        week_row.remove_class("selected");
                        day_row.add_class("selected");
                        year_row.remove_class("selected");
                    }
                    year_page::NAME => {
                        month_row.remove_class("selected");
                        week_row.remove_class("selected");
                        day_row.remove_class("selected");
                        year_row.add_class("selected");
                    }
                    _ => {
                        // ignore
                    }
                }
            }
        },
    ));

    sidebar
}

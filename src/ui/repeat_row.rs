use std::collections::HashMap;
use std::rc::Rc;

use gettextrs::gettext;
use glib::clone::Downgrade;
use grx::components::*;
use grx::{component, grx, props};

use crate::domain::time::{KarlenderDate, KarlenderDateTime, KarlenderTimezone};

use super::editor_page::{get_month_str, get_month_val};

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Clone, Default)]
pub(crate) struct State {
    rrule: HashMap<String, String>,
    change: Option<Rc<dyn Fn()>>,
}

impl std::fmt::Debug for State {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("State")
            .field("rrule", &self.rrule)
            .field("change", &self.change.is_some())
            .finish()
    }
}

impl PartialEq for State {
    fn eq(&self, other: &Self) -> bool {
        self.rrule == other.rrule && self.change.is_none() && other.change.is_none()
    }
}

#[component(Props, State)]
pub(crate) struct RepeatRow {}

const YEARLY: &str = "yearly";
const MONTHLY: &str = "monthly";
const WEEKLY: &str = "weekly";
const DAILY: &str = "daily";
const HOURLY: &str = "hourly";

pub(crate) fn repeat_row(props: Props) -> Rc<RepeatRow> {
    let freq = gettext("Frequency");

    let yearly = gettext("Yearly");
    let monthly = gettext("Monthly");
    let weekly = gettext("Weekly");
    let daily = gettext("Daily");
    let hourly = gettext("Hourly");

    let mon = gettext("M");
    let tue = gettext("T");
    let wed = gettext("W");
    let thu = gettext("T");
    let fri = gettext("F");
    let sat = gettext("S");
    let sun = gettext("S");

    let end = gettext("End");

    let never = gettext("Never");
    let fixedcount = gettext("Fixed count");
    let on_date = gettext("On date");
    let nmonths = gettext("Every N months");
    let nweeks = gettext("Every N weeks");
    let ndays = gettext("Every N days");
    let nhours = gettext("Every N hweeksours");

    let count = gettext("Count");
    let enddate = gettext("End date");

    let freq_row: Rc<ComboRow>;
    let interval_row: Rc<ActionRow>;
    let interval_button: Rc<SpinButton>;
    let by_day_row: Rc<ActionRow>;
    let byday_btn_mon: Rc<ToggleButton>;
    let byday_btn_tue: Rc<ToggleButton>;
    let byday_btn_wed: Rc<ToggleButton>;
    let byday_btn_thu: Rc<ToggleButton>;
    let byday_btn_fri: Rc<ToggleButton>;
    let byday_btn_sat: Rc<ToggleButton>;
    let byday_btn_sun: Rc<ToggleButton>;
    let end_row: Rc<ComboRow>;
    let end_count_row: Rc<ActionRow>;
    let end_count_btn: Rc<SpinButton>;
    let end_date_row: Rc<ExpanderRow>;
    let end_year_sb: Rc<SpinButton>;
    let end_month_sb: Rc<SpinButton>;
    let end_day_sb: Rc<SpinButton>;

    let component = grx! {
        list (classes="boxed-list frag_list_box".into(), styles=[vertical, border(0,0,0,0), bg("transparent".into())]) [
            combo_row (
                id=freq_row,
                title=freq,
                selectable=Some(false),
                activatable=Some(false)
            ),
            action_row (
                id=interval_row,
                selectable=Some(false),
                activatable=Some(false)
            ) [
                spin_button(id=interval_button, lower=0.0, upper=999.0, step=1.0, page=6.0, styles=[valign(center)])
            ],
            action_row (
                id=by_day_row,
                hide_title=Some(true),
                selectable=Some(false),
                activatable=Some(false),
                expand_suffix=Some(true)
            ) [
                container (classes="by_day".into(), styles=[hexpand, homogeneous, valign(center), spacing(4)]) [
                    toggle_button (id=byday_btn_mon) [ text(text=mon) ],
                    toggle_button (id=byday_btn_tue) [ text(text=tue) ],
                    toggle_button (id=byday_btn_wed) [ text(text=wed) ],
                    toggle_button (id=byday_btn_thu) [ text(text=thu) ],
                    toggle_button (id=byday_btn_fri) [ text(text=fri) ],
                    toggle_button (id=byday_btn_sat) [ text(text=sat) ],
                    toggle_button (id=byday_btn_sun) [ text(text=sun) ]
                ]
            ],
            combo_row (
                id=end_row,
                title=end,
                selectable=Some(false),
                activatable=Some(false)
            ),
            action_row (
                id=end_count_row,
                title=count,
                selectable=Some(false),
                activatable=Some(false)
            ) [
                spin_button(id=end_count_btn, lower=0.0, upper=999.0, step=1.0, page=6.0, styles=[valign(center)])
            ],
            expander_row(id=end_date_row, title=enddate, selectable=Some(false), activatable=Some(false), expanded=Some(true)) [
                container (styles=[horizontal, halign(end), spacing(4)]) [
                    spin_button (id=end_day_sb, styles=[vertical, halign(end), w(40)], value=1.0, upper=31.0, lower=1.0, step=1.0, page=10.0),
                    spin_button (id=end_month_sb, styles=[vertical, halign(end), w(40)], value=1.0, upper=12.0, lower=1.0, step=1.0, page=4.0,
                        value_mapper=Some((
                            Rc::new(get_month_str),
                            Rc::new(get_month_val),
                        ))
                    ),
                    spin_button (id=end_year_sb, styles=[vertical, halign(end), w(60)], value=2023.0, upper=9999.0, lower=1960.0, step=1.0, page=10.0)
                ]
            ]
        ]
    };

    freq_row.append(Rc::new(Item::new(YEARLY, &yearly)));
    freq_row.append(Rc::new(Item::new(MONTHLY, &monthly)));
    freq_row.append(Rc::new(Item::new(WEEKLY, &weekly)));
    freq_row.append(Rc::new(Item::new(DAILY, &daily)));
    freq_row.append(Rc::new(Item::new(HOURLY, &hourly)));

    end_row.append(Rc::new(Item::new("never", &never)));
    end_row.append(Rc::new(Item::new("count", &fixedcount)));
    end_row.append(Rc::new(Item::new("date", &on_date)));

    interval_row.set_visible(false);
    by_day_row.set_visible(false);

    let row = RepeatRow::new(props, component);

    let byday_btn_mon_w = byday_btn_mon.downgrade();
    let byday_btn_tue_w = byday_btn_tue.downgrade();
    let byday_btn_wed_w = byday_btn_wed.downgrade();
    let byday_btn_thu_w = byday_btn_thu.downgrade();
    let byday_btn_fri_w = byday_btn_fri.downgrade();
    let byday_btn_sat_w = byday_btn_sat.downgrade();
    let byday_btn_sun_w = byday_btn_sun.downgrade();

    let weakrow = row.downgrade();
    freq_row.on_change(move |r| {
        if let Some(row) = weakrow.upgrade() {
            if let Some(item) = r.selected() {
                match item.id().as_str() {
                    YEARLY => {
                        row.set_state(|s| {
                            s.rrule.insert("FREQ".into(), "YEARLY".into());
                            s.rrule.remove("INTERVAL");
                        });
                        interval_row.set_visible(false);
                        by_day_row.set_visible(false);
                    }
                    MONTHLY => {
                        row.set_state(|s| {
                            s.rrule.insert("FREQ".into(), "MONTHLY".into());
                            if !s.rrule.contains_key("INTERVAL") {
                                s.rrule.insert("INTERVAL".into(), "1".into());
                            }
                        });
                        interval_row.set_visible(true);
                        by_day_row.set_visible(false);
                        interval_row.set_title(&nmonths);
                    }
                    WEEKLY => {
                        let byday_btn_mon = byday_btn_mon_w.clone();
                        let byday_btn_tue = byday_btn_tue_w.clone();
                        let byday_btn_wed = byday_btn_wed_w.clone();
                        let byday_btn_thu = byday_btn_thu_w.clone();
                        let byday_btn_fri = byday_btn_fri_w.clone();
                        let byday_btn_sat = byday_btn_sat_w.clone();
                        let byday_btn_sun = byday_btn_sun_w.clone();

                        let byday_btn_mon = byday_btn_mon.upgrade();
                        let byday_btn_tue = byday_btn_tue.upgrade();
                        let byday_btn_wed = byday_btn_wed.upgrade();
                        let byday_btn_thu = byday_btn_thu.upgrade();
                        let byday_btn_fri = byday_btn_fri.upgrade();
                        let byday_btn_sat = byday_btn_sat.upgrade();
                        let byday_btn_sun = byday_btn_sun.upgrade();

                        if let (
                            Some(byday_btn_mon),
                            Some(byday_btn_tue),
                            Some(byday_btn_wed),
                            Some(byday_btn_thu),
                            Some(byday_btn_fri),
                            Some(byday_btn_sat),
                            Some(byday_btn_sun),
                        ) = (
                            byday_btn_mon,
                            byday_btn_tue,
                            byday_btn_wed,
                            byday_btn_thu,
                            byday_btn_fri,
                            byday_btn_sat,
                            byday_btn_sun,
                        ) {
                            match KarlenderTimezone::local()
                                .today()
                                .weekday()
                                .number_from_monday()
                            {
                                0 => {
                                    byday_btn_mon.set_pressed(true);
                                }
                                1 => {
                                    byday_btn_tue.set_pressed(true);
                                }
                                2 => {
                                    byday_btn_wed.set_pressed(true);
                                }
                                3 => {
                                    byday_btn_thu.set_pressed(true);
                                }
                                4 => {
                                    byday_btn_fri.set_pressed(true);
                                }
                                5 => {
                                    byday_btn_sat.set_pressed(true);
                                }
                                6 => {
                                    byday_btn_sun.set_pressed(true);
                                }
                                _ => {
                                    // ignore
                                }
                            }
                        }

                        row.set_state(|s| {
                            s.rrule.insert("FREQ".into(), "WEEKLY".into());
                            if !s.rrule.contains_key("INTERVAL") {
                                s.rrule.insert("INTERVAL".into(), "1".into());
                            }

                            if !s.rrule.contains_key("BYDAY") {
                                match KarlenderTimezone::local()
                                    .today()
                                    .weekday()
                                    .number_from_monday()
                                {
                                    0 => {
                                        s.rrule.insert("BYDAY".into(), "MO".into());
                                    }
                                    1 => {
                                        s.rrule.insert("BYDAY".into(), "TU".into());
                                    }
                                    2 => {
                                        s.rrule.insert("BYDAY".into(), "WE".into());
                                    }
                                    3 => {
                                        s.rrule.insert("BYDAY".into(), "TH".into());
                                    }
                                    4 => {
                                        s.rrule.insert("BYDAY".into(), "FR".into());
                                    }
                                    5 => {
                                        s.rrule.insert("BYDAY".into(), "SA".into());
                                    }
                                    6 => {
                                        s.rrule.insert("BYDAY".into(), "SU".into());
                                    }
                                    _ => {
                                        // ignore
                                    }
                                }
                            }
                        });
                        interval_row.set_visible(true);
                        by_day_row.set_visible(true);
                        interval_row.set_title(&nweeks);
                    }
                    DAILY => {
                        row.set_state(|s| {
                            s.rrule.insert("FREQ".into(), "DAILY".into());
                            if !s.rrule.contains_key("INTERVAL") {
                                s.rrule.insert("INTERVAL".into(), "1".into());
                            }
                        });
                        interval_row.set_visible(true);
                        interval_row.set_title(&ndays);
                    }
                    HOURLY => {
                        row.set_state(|s| {
                            s.rrule.insert("FREQ".into(), "HOURLY".into());
                            if !s.rrule.contains_key("INTERVAL") {
                                s.rrule.insert("INTERVAL".into(), "1".into());
                            }
                        });
                        interval_row.set_visible(true);
                        interval_row.set_title(&nhours);
                    }
                    s => {
                        unreachable!("unknown repeat frequency {}", s)
                    }
                }
            }
            if let Some(change) = row.state.borrow().change.as_ref() {
                change()
            }
        }
    });

    let weakrow = row.downgrade();
    interval_button.set_value(1.0);
    interval_button.on_change(move |b| {
        let val = format!("{}", b.value() as u32);
        if let Some(row) = weakrow.upgrade() {
            row.set_state(move |s| {
                s.rrule.insert("INTERVAL".into(), val.clone());
            });
            if let Some(change) = row.state.borrow().change.as_ref() {
                change()
            }
        }
    });

    end_count_row.set_visible(false);
    end_date_row.set_visible(false);

    {
        let weakrow = row.downgrade();
        let end_count_row = end_count_row.downgrade();
        let end_date_row = end_date_row.downgrade();

        let end_year_sb = end_year_sb.downgrade();
        let end_month_sb = end_month_sb.downgrade();
        let end_day_sb = end_day_sb.downgrade();

        end_row.on_change(move |r| {
            if let Some(row) = weakrow.upgrade() {
                if let Some(sel) = r.selected() {
                    match sel.id().as_str() {
                        "never" => {
                            row.set_state(|s| {
                                s.rrule.remove("COUNT");
                                s.rrule.remove("UNTIL");
                            });

                            if let Some(end_count_row) = end_count_row.upgrade() {
                                if let Some(end_date_row) = end_date_row.upgrade() {
                                    end_count_row.set_visible(false);
                                    end_date_row.set_visible(false);
                                }
                            }
                        }
                        "count" => {
                            row.set_state(|s| {
                                s.rrule.insert("COUNT".into(), "1".into());
                                s.rrule.remove("UNTIL");
                            });
                            if let Some(end_count_row) = end_count_row.upgrade() {
                                if let Some(end_date_row) = end_date_row.upgrade() {
                                    end_count_row.set_visible(true);
                                    end_date_row.set_visible(false);
                                }
                            }
                        }
                        "date" => {
                            let end_year_sb = end_year_sb.clone();
                            let end_month_sb = end_month_sb.clone();
                            let end_day_sb = end_day_sb.clone();

                            let tz = KarlenderTimezone::local();
                            let today = tz.today();
                            if let (Some(end_year_sb), Some(end_month_sb), Some(end_day_sb)) = (
                                end_year_sb.upgrade(),
                                end_month_sb.upgrade(),
                                end_day_sb.upgrade(),
                            ) {
                                end_year_sb.set_value(today.year() as f64);
                                end_month_sb.set_value(today.month() as f64);
                                end_day_sb.set_value(today.day() as f64);
                            }

                            row.set_state(move |s| {
                                s.rrule.remove("COUNT");
                                s.rrule.insert(
                                    "UNTIL".into(),
                                    tz.today()
                                        .and_hms(0, 0, 0, Some(&tz))
                                        .format(crate::domain::time::DatetimeFormat::ISO8601Z),
                                );
                            });
                            if let Some(end_count_row) = end_count_row.upgrade() {
                                if let Some(end_date_row) = end_date_row.upgrade() {
                                    end_count_row.set_visible(false);
                                    end_date_row.set_visible(true);
                                }
                            }
                        }
                        _ => {
                            // ignore
                        }
                    }
                }
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
    }

    // end date
    {
        let r = row.downgrade();
        end_year_sb.on_change(move |b| {
            let value = b.value() as i32;
            if let Some(row) = r.upgrade() {
                row.set_state(move |s| {
                    if let Some(v) = s.rrule.get_mut("UNTIL") {
                        if let Ok((dt, _)) = KarlenderDateTime::from_iso8601(v, None) {
                            let new_date = KarlenderDate::from_ymd(value, dt.month(), dt.day());
                            *v = new_date
                                .and_hms(0, 0, 0, None)
                                .format(crate::domain::time::DatetimeFormat::ISO8601Z);
                        }
                    }
                });
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
        let r = row.downgrade();
        end_month_sb.on_change(move |b| {
            let value = b.value() as u8;
            if let Some(row) = r.upgrade() {
                row.set_state(move |s| {
                    if let Some(v) = s.rrule.get_mut("UNTIL") {
                        if let Ok((dt, _)) = KarlenderDateTime::from_iso8601(v, None) {
                            let new_date = KarlenderDate::from_ymd(dt.year(), value, dt.day());
                            *v = new_date
                                .and_hms(0, 0, 0, None)
                                .format(crate::domain::time::DatetimeFormat::ISO8601Z);
                        }
                    }
                });
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
        let r = row.downgrade();
        end_day_sb.on_change(move |b| {
            let value = b.value() as u8;
            if let Some(row) = r.upgrade() {
                row.set_state(move |s| {
                    if let Some(v) = s.rrule.get_mut("UNTIL") {
                        if let Ok((dt, _)) = KarlenderDateTime::from_iso8601(v, None) {
                            let new_date = KarlenderDate::from_ymd(dt.year(), dt.month(), value);
                            *v = new_date
                                .and_hms(0, 0, 0, None)
                                .format(crate::domain::time::DatetimeFormat::ISO8601Z);
                        }
                    }
                });
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
    }

    // end count
    {
        let weakrow = row.downgrade();
        end_count_btn.set_value(1.0);
        end_count_btn.on_change(move |b| {
            if let Some(row) = weakrow.upgrade() {
                let val = format!("{}", b.value() as u32);
                row.set_state(move |s| {
                    s.rrule.insert("COUNT".into(), val.clone());
                });
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
    }

    // week byday
    {
        let weakrow = row.downgrade();
        byday_btn_mon.on_click(move |b| {
            if let Some(row) = weakrow.upgrade() {
                on_byday(b, &row, "MO");
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
        let weakrow = row.downgrade();
        byday_btn_tue.on_click(move |b| {
            if let Some(row) = weakrow.upgrade() {
                on_byday(b, &row, "TU");
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
        let weakrow = row.downgrade();
        byday_btn_wed.on_click(move |b| {
            if let Some(row) = weakrow.upgrade() {
                on_byday(b, &row, "WE");
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
        let weakrow = row.downgrade();
        byday_btn_thu.on_click(move |b| {
            if let Some(row) = weakrow.upgrade() {
                on_byday(b, &row, "TH");
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
        let weakrow = row.downgrade();
        byday_btn_fri.on_click(move |b| {
            if let Some(row) = weakrow.upgrade() {
                on_byday(b, &row, "FR");
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
        let weakrow = row.downgrade();
        byday_btn_sat.on_click(move |b| {
            if let Some(row) = weakrow.upgrade() {
                on_byday(b, &row, "SA");
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
        let weakrow = row.downgrade();
        byday_btn_sun.on_click(move |b| {
            if let Some(row) = weakrow.upgrade() {
                on_byday(b, &row, "SU");
                if let Some(change) = row.state.borrow().change.as_ref() {
                    change()
                }
            }
        });
    }

    row.set_state(|s| {
        s.rrule.insert("FREQ".into(), "YEARLY".into());
    });

    row
}

fn day_num(day1: &str) -> u8 {
    match day1 {
        "MO" => 0,
        "TU" => 1,
        "WE" => 2,
        "TH" => 3,
        "FR" => 4,
        "Sa" => 5,
        "SU" => 6,
        _ => 7,
    }
}

fn on_byday(b: &Rc<ToggleButton>, row: &Rc<RepeatRow>, day: &str) {
    if b.pressed() {
        let day = day.to_string();
        row.set_state(move |s| {
            if let Some(byday) = s.rrule.get_mut("BYDAY") {
                let mut days: Vec<String> = byday.split(',').map(|s| s.to_string()).collect();
                if !days.contains(&day) {
                    days.push(day.clone());
                    days.sort_by_key(|a| day_num(a));
                    s.rrule.insert("BYDAY".into(), days.join(","));
                }
            } else {
                s.rrule.insert("BYDAY".into(), day.clone());
            }
        });
    } else {
        let day = day.to_string();
        row.set_state(move |s| {
            if let Some(byday) = s.rrule.get("BYDAY").cloned() {
                let mut days: Vec<String> = byday.split(',').map(|s| s.to_string()).collect();
                days.retain(|d: &String| d != &day);
                if days.is_empty() {
                    s.rrule.remove("BYDAY");
                } else {
                    s.rrule.insert("BYDAY".into(), days.join(","));
                }
            }
        })
    }
}

impl RepeatRow {
    pub(crate) fn on_change(self: &Rc<Self>, handler: impl Fn(&Rc<Self>) + 'static) {
        // let state = self.state.borrow();
        let sel = self.downgrade();
        let handler = Rc::new(handler);
        self.set_state(move |s| {
            let sel = sel.clone();
            let handler = handler.clone();
            s.change = Some(Rc::new(move || {
                // println!("{:?}", sel.state.borrow());
                if let Some(sel) = sel.upgrade() {
                    handler(&sel);
                }
                //
            }))
        });
    }

    pub(crate) fn value(self: &Rc<Self>) -> String {
        let mut s = String::new();
        let borrow = self.state.borrow();
        let mut values: Vec<(&String, &String)> = borrow.rrule.iter().collect();
        values.sort_by(|(a, _), (b, _)| rrule_order(a).cmp(&rrule_order(b)));
        for (k, v) in values {
            s.push_str(&format!("{k}={v};"));
        }
        s
    }

    pub(crate) fn notify(self: &Rc<Self>) {
        if let Some(change) = self.state.borrow().change.as_ref() {
            change()
        }
    }

    pub(crate) fn get_subtitle(self: &Rc<Self>) -> String {
        let state = self.state.borrow();
        match state.rrule.get("FREQ").map(|f| f.as_str()) {
            Some("YEARLY") => gettext("yearly"),
            Some("MONTHLY") => gettext("monthly"),
            Some("WEEKLY") => gettext("weekly"),
            Some("DAILY") => gettext("daily"),
            Some("HOURLY") => gettext("hourly"),
            _ => gettext("yearly"),
        }
    }
}

fn rrule_order(key: &str) -> u8 {
    match key {
        "FREQ" => 0,
        "BYDAY" => 1,
        "INTERVAL" => 3,
        "UNTIL" => 3,
        "COUNT" => 4,
        _ => 5,
    }
}

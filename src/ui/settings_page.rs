use std::rc::Rc;

use gettextrs::gettext;
use grx::components::*;
use grx::{component, grx, props};

use crate::domain::time::{FormatKey, KarlenderDate};
use crate::domain::DEFAULT_COLOR;
use crate::store::calendar::SELECT_CALENDARS;
use crate::store::store;
use crate::store::ui::SETTINGS_PAGE_NAME;
use crate::ui::login_row;

pub(crate) const NAME: &str = SETTINGS_PAGE_NAME;
#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct SettingsPage {}

pub(crate) fn settings_page(props: Props) -> Rc<SettingsPage> {
    let dateformat = gettext("Date/Time format");
    let defaultcalendar = gettext("Default calendar:");
    let calendars = gettext("Calendars");
    let enabled = gettext("Enabled");
    let name = gettext("Name");
    let color = gettext("Color");
    let remove = gettext("Remove");

    let date_format_row: Rc<ComboRow>;
    let calendars_row: Rc<ComboRow>;
    let add_calendar_btn: Rc<Button>;
    let calendars_list: Rc<List>;

    let component = grx! {
        container (styles=[p(10)]) [
            scroll [
                clamp (max_width=600, styles=[vexpand, hexpand]) [
                    container (styles=[vertical, spacing(10)]) [
                        login_row,
                        list (classes="boxed-list".into(), styles=[m(2)]) [
                            combo_row (id=date_format_row, title=dateformat)
                        ],
                        text(classes="title-2".into(), text=calendars),
                        list (classes="boxed-list".into(), styles=[m(2)]) [
                            combo_row (id=calendars_row, title=defaultcalendar, selectable=Some(false)) [
                                #[prefix=true]
                                container (styles=[hexpand]) [
                                    button (id=add_calendar_btn, styles=[valign(center)]) [
                                        icon("list-add-symbolic")
                                    ]
                                ]
                            ]
                        ],
                        list (id=calendars_list, classes="boxed-list".into(), styles=[m(2)])
                    ]
                ]
            ]
        ]
    };
    let page = SettingsPage::new(props, component);

    add_calendar_btn.on_click(|_| store().dispatch(crate::store::calendar::Action::New));

    calendars_row.on_change(|c| {
        if let Some(s) = c.selected() {
            let url = s.id();
            store().dispatch(crate::store::calendar::Action::SetDefault(url))
        }
    });

    page.later_drop(store().select_dbg("settings_page.select(SELECT_CALENDARS)", SELECT_CALENDARS, move |state| {
        calendars_row.clear();
        calendars_row.set_disabled(false);
        calendars_list.clear();
        for calendar in state.calendar.calendars.iter() {
            let item = Rc::new(Item::new(calendar.url().as_str(), calendar.name()));
            calendars_row.append(item);
            if calendar.is_default() {
                calendars_row.select(calendar.url().as_str());
            }

            let mut subtitle = gettext("Local");
            if calendar.is_remote() {
                subtitle = calendar.login_url().unwrap_or_else(|| calendar.url()).to_string();
            }

            let enabled_switch: Rc<Switch>;
            let name_row: Rc<ActionRow>;
            let name_entry: Rc<Entry>;
            let color_row: Rc<ActionRow>;
            let color_entry: Rc<Entry>;
            let remove_row: Rc<ActionRow>;
            let remove_btn: Rc<Button>;

            let child = grx! {
                expander_row (title=calendar.name().into(), subtitle=subtitle) [
                    #[prefix=true]
                    icon(name="media-playback-stop-symbolic", styles=[text(calendar.color().cloned().unwrap_or_else(|| DEFAULT_COLOR.into()))]),
                    action_row (title=enabled.clone(), selectable=Some(false), activatable=Some(false)) [
                        switch (id=enabled_switch, styles=[valign(center)])
                    ],
                    action_row (id=name_row, title=name.clone(), selectable=Some(false), activatable=Some(false)) [
                        entry (id=name_entry, styles=[valign(center)])
                    ],
                    action_row (id=color_row,title=color.clone(), selectable=Some(false), activatable=Some(false)) [
                        entry (id=color_entry, styles=[valign(center)])
                    ],
                    action_row (id=remove_row,title=remove.clone(), selectable=Some(false), activatable=Some(false)) [
                        button (id=remove_btn, styles=[valign(center)]) [
                            icon("app-remove-symbolic")
                        ]
                    ]
                ]
            };

            enabled_switch.set_state(calendar.enabled());

            let url = calendar.url().clone();
            enabled_switch.on_change(move |s| {
                store().dispatch(crate::store::calendar::Action::UpdateEnable(url.clone(), s.state()));
            });

            name_row.set_visible(!calendar.is_remote());
            color_row.set_visible(!calendar.is_remote());
            remove_row.set_visible(!calendar.is_remote());

            name_entry.set_text(calendar.name());
            color_entry.set_text(&calendar.color().cloned().unwrap_or_default());

            let url = calendar.url().clone();
            name_entry.on_blur(move |e| {
                store().dispatch(crate::store::calendar::Action::UpdateName(url.clone(), e.text()));
            });
            let url = calendar.url().clone();
            color_entry.on_blur(move |e| {
                store().dispatch(crate::store::calendar::Action::UpdateColor(url.clone(), e.text()));
            });
            let url = calendar.url().clone();
            remove_btn.on_click(move |_| {
                store().dispatch(crate::store::calendar::Action::Remove(url.clone()));
            });

            calendars_list.append(child)

        }
    }));

    let now = KarlenderDate::from_ymd(2023, 5, 24).and_hms(22, 30, 0, None);
    for item in date_formats() {
        date_format_row.append(Rc::new(Item::new(
            item.as_str(),
            &now.format(item.get_time_format()),
        )));
    }

    date_format_row.on_change(|r| {
        if let Some(s) = r
            .selected()
            .and_then(|item| FormatKey::from_str(&item.id()))
        {
            store().dispatch(crate::store::settings::Action::SetFormat(s))
        }
    });

    page
}

fn date_formats() -> Vec<FormatKey> {
    vec![
        FormatKey::BottomUp12,
        FormatKey::BottomUp24,
        FormatKey::TopDown12,
        FormatKey::TopDown24,
        FormatKey::Wrong,
    ]
}

use std::rc::Rc;

use grx::{component, grx, prelude::*, props};

use crate::domain::time::{KarlenderDate, KarlenderDateTimeDuration};
use crate::store::selection::SELECT_CURRENT_YEAR_AND_INDEX;
use crate::store::store;
use crate::store::ui::YEAR_PAGE_NAME;
use crate::ui::year_month;

pub(crate) const NAME: &str = YEAR_PAGE_NAME;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct YearPage {}

pub(crate) fn year_page(props: Props) -> Rc<YearPage> {
    let months: Rc<Grid>;
    let component = grx! {
        container (styles=[p(10)]) [
            clamp (max_width=600, styles=[vexpand, hexpand]) [
                scroll [
                    grid (id=months, styles=[vertical, halign(center), spacing(10)])
                ]
            ]
        ]
    };

    let page = YearPage::new(props, component);
    let cols = 2;
    page.later_drop(store().select_dbg(
        "year_page.select(SELECT_CURRENT_YEAR_AND_INDEX)",
        SELECT_CURRENT_YEAR_AND_INDEX,
        move |state| {
            for i in 0..12 {
                let month = KarlenderDate::from_ymd(state.selection.current_year.year(), 1 + i, 1);
                let mut days = vec![];
                for i in 0..31 {
                    let day = month.clone() + KarlenderDateTimeDuration::days(i);
                    if day.month() == month.month() && state.index.index.has_events(&day) {
                        days.push(day.day());
                    }
                }
                let child = grx! { year_month (month=month, days=days )};
                let child: Component = child;
                months.append(&child, (i as i32).rem_euclid(cols), i as i32 / cols, 1, 1);
            }
        },
    ));

    page
}

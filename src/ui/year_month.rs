use std::rc::Rc;

use grx::{component, grx, props};
use grx::{components::*, Style};

use crate::domain::time::{KarlenderDate, KarlenderDateTimeDuration, KarlenderTimezone};

use crate::store::store;

use super::editor_page::get_month_str;
use super::month_page;

#[props]
#[derive(Debug)]
pub(crate) struct Props {
    pub(crate) month: KarlenderDate,
    pub(crate) days: Vec<u8>,
}

impl Default for Props {
    fn default() -> Self {
        Self {
            id: Default::default(),
            classes: Default::default(),
            styles: Default::default(),
            children: Default::default(),
            days: Default::default(),
            month: KarlenderTimezone::local().today(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {
    days: Vec<u8>,
}

#[component(Props, State)]
pub(crate) struct YearPage {}

pub(crate) fn year_month(props: Props) -> Rc<YearPage> {
    let year_month_container: Rc<Container>;
    let grid: Rc<Grid>;
    let t = get_month_str(props.month.month() as f64);
    let component = grx! {
        container (id=year_month_container, styles=[vertical, halign(center)]) [
            text(text=t),
            grid (id=grid, styles=[p(10), hover:bg("alpha(currentColor, 0.1)".into())])
        ]
    };

    let month = props.month.clone();
    year_month_container.on_click(move |_| {
        store().dispatch(crate::store::selection::Action::SetMonth(month.clone()));
        store().dispatch(crate::store::ui::Action::Replace(month_page::NAME))
    });

    let month = props.month.clone();
    let mut col: u8 = month.weekday().number_from_monday();
    let mut row: u8 = 0;
    let days = props.days.clone();

    let page = YearPage::new(props, component);

    let start_x = month.weekday().number_from_monday();

    for i in 0..31 {
        let day = month.clone() + KarlenderDateTimeDuration::days(i);
        if day.month() == month.month() {
            let child = grx! { container (styles=[h(16), w(16), rounded_full, border_color("@borders".into()), border(1,1,1,1)])};
            let child: Component = child;
            grid.append(&child, col as i32, row as i32, 1, 1);
        }
        if col + 1 >= 7 {
            row += 1
        }
        col = (col + 1).rem_euclid(7);
    }

    page.select_state(
        |a, b| a.days != b.days,
        move |state| {
            for day in &state.days {
                let x = (start_x + day - 1).rem_euclid(7);
                let y = (start_x + day - 1) / 7;
                if let Some(child) = grid.get(x as i32, y as i32) {
                    child.set_styles(vec![Style::bg(
                        grx::styles::Modifier::none,
                        "@borders".into(),
                    )])
                }
            }
        },
    );
    page.set_state(move |s| s.days = days.clone());

    page
}

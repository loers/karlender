use std::rc::Rc;

use grx::{component, components::*, grx, props, styles::Modifier::none, Style};

use crate::{
    domain::time::KarlenderDate,
    store::{store, ui::SELECT_MOBILE},
};

use super::calendar_utils::get_week_day_name;

pub(crate) const HEIGHT: u32 = 40;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {
    pub(crate) day: Option<KarlenderDate>,
    pub(crate) index: u8,
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {
    label: String,
}

#[component(Props, State)]
pub(crate) struct WeekHeaderCell {}

pub(crate) fn week_header_cell(props: Props) -> Rc<WeekHeaderCell> {
    let mut border = Style::none(none);
    if props.index < 8 {
        border = Style::border(none, 0, 1, 1, 0);
    }
    if props.index == 7 {
        border = Style::border(none, 0, 0, 1, 0);
    }
    let txt: Rc<Text>;
    let component = grx! {
        container (
            id="week_header_cell".into(),
            styles=[
                p(4),
                h(HEIGHT),
                {border},
                hover:bg("alpha(currentColor, 0.1)".into())
            ]
        ) [
            text (
                id=txt,
                styles=[
                    hexpand
                ],
                classes="title-3".into()
            )
        ]
    };

    let mc = WeekHeaderCell::new(props, component);

    let t = txt;
    mc.on_state(move |s| t.set_text(s.label.as_ref()));

    let m = mc.clone();
    mc.later_drop(store().select_dbg(
        "week_header_cell.select(SELECT_MOBILE)",
        SELECT_MOBILE,
        move |s| {
            if m.props.index == 0 {
                m.set_state(|s| s.label = "".into())
            } else if let Some(day) = m.props.day.as_ref() {
                let lbl =
                    get_week_day_name(day.weekday().number_from_monday(), s.ui.mobile.is_mobile());
                let lbl = format!("{}\n {}.", lbl, day.day());
                m.set_state(move |l| l.label = lbl.clone())
            }
        },
    ));

    mc
}

impl Drop for WeekHeaderCell {
    fn drop(&mut self) {
        println!("drop MHC {:?}", self.props.day)
    }
}

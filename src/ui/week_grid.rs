use crate::{
    domain::{
        get_text_color,
        time::{KarlenderDate, KarlenderDateTimeDuration, KarlenderTimezone},
        DEFAULT_COLOR,
    },
    store::{calendar::SELECT_NOW, index::SELECT_INDEX, store, ui::SELECT_GOTO_NOW},
    ui::{week_cell, week_header_cell},
};
use glib::clone::Downgrade;
use grx::{component, components::*, grid::Grid, grx, props, Style};
use std::{cell::RefCell, collections::HashMap, rc::Rc};

use super::week_cell::WeekCell;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct State {
    week: KarlenderDate,
    initialized: bool,
}
impl Default for State {
    fn default() -> Self {
        Self {
            week: KarlenderTimezone::local().today(),
            initialized: false,
        }
    }
}

pub(crate) const STEPS: u16 = 4;
pub(crate) const SLOTS_PER_HOUR: u16 = 4;
pub(crate) const ROWS: u16 = 24 * SLOTS_PER_HOUR;
#[component(Props, State)]
pub(crate) struct WeekGrid {}

pub(crate) fn week_grid(props: Props) -> Rc<WeekGrid> {
    let week_grid: Rc<Grid>;
    let hour_indicator: Rc<Container>;
    let component = grx! {
        scroll [
            overlay [
                container [
                    grid (
                        id=week_grid,
                        styles=[hexpand, vexpand]
                    )
                ],
                #[overlay=true]
                container (id=hour_indicator, styles=[border_t(2), valign(start), border_color("alpha(@theme_unfocused_selected_bg_color, 0.2)".into())])
            ]
        ]
    };

    let grid = WeekGrid::new(props, component);
    let g = grid.clone();
    grid.later_drop(store().select_dbg(
        "week_grid.select(SELECT_GOTO_NOW)",
        SELECT_GOTO_NOW,
        move |_| {
            g.set_state(|s| {
                s.week = KarlenderTimezone::local().today();
            })
        },
    ));

    let tz = KarlenderTimezone::local();

    let handlers: RefCell<Vec<Rc<dyn Fn()>>> = Default::default();
    let weak_week_grid = week_grid.downgrade();
    grid.select_state(
        |a, b| a.week != b.week,
        move |state| {
            if let Some(week_grid) = weak_week_grid.upgrade() {
                let day = state.week.clone();
                let first_day_of_week = day.clone()
                    - KarlenderDateTimeDuration::days(day.weekday().number_from_monday() as i64);

                for _ in 0..ROWS+2 {
                    week_grid.remove_row(0);
                }

                for d in 0..8 {
                    let mut props = week_header_cell::Props {
                        index: d,
                        ..Default::default()
                    };
                    if d == 0 {
                    } else {
                        let day =
                            first_day_of_week.clone() + KarlenderDateTimeDuration::days(d as i64 - 1);
                        props.day = Some(day);
                    }
                    let h = week_header_cell::week_header_cell(props);
                    let h: grx::Component = h;
                    week_grid.append(&h, d as i32, 0, 1, 1);
                }

                let mut full_day_cells = Vec::new();
                // full day row
                for d in 0..8 {
                    let props = week_cell::Props
                    {
                        pos: (d, 1),
                        full_day: true,
                        ..Default::default()
                    };
                    let cell = week_cell::week_cell(props);
                    if d > 0 {
                        full_day_cells.push(cell.clone());
                    }
                    let cell: grx::Component = cell;
                    week_grid.append(&cell, d as i32, 1, 1, 1);
                }

                let cells: Rc<RefCell<Vec<std::rc::Weak<WeekCell>>>> = Rc::new(RefCell::default());
                for half_hour_slot in 0..ROWS {
                    for col in 0u16..8 {
                        let day = first_day_of_week.clone()
                            + KarlenderDateTimeDuration::days(col as i64 - 1);
                        let x = col;
                        let y = 2 + half_hour_slot;
                        if col == 0 && half_hour_slot.rem_euclid(STEPS) == 0 {
                            let lbl = format!(
                                    "{:02}:{:02}",
                                    half_hour_slot / SLOTS_PER_HOUR,
                                    half_hour_slot.rem_euclid(SLOTS_PER_HOUR) * (60/SLOTS_PER_HOUR)
                                );
                            let timeslot_label: grx::Component = grx!{
                                text(text=lbl, styles=[valign(start), m(4)])
                            };
                            week_grid.append(&timeslot_label, x as i32, y as i32, 1, 4);
                        }
                        let timeslot = day.and_hms(
                            (half_hour_slot / SLOTS_PER_HOUR) as u8,
                            (half_hour_slot.rem_euclid(SLOTS_PER_HOUR) * (60/SLOTS_PER_HOUR)) as u8,
                            0,
                            Some(&tz),
                        );
                        let mut props: week_cell::Props = week_cell::Props {
                            timeslot,
                            pos: (x, y),
                            ..Default::default()
                        };
                        let moved_cells = cells.clone();
                        props.on_click = Some(Rc::new(move |cell| {
                            let selected = cell.selected();
                            for cell in &*moved_cells.borrow() {
                                if let Some(cell) = cell.upgrade() {
                                    cell.deselect();
                                }
                            }
                            cell.select(); // trigger gstore action dispatch in any case
                            if selected {
                                cell.deselect()
                            }
                        }));
                        let cell = week_cell::week_cell(props);
                        let cells = &mut *cells.as_ref().borrow_mut();
                        cells.push(cell.downgrade());
                        let cell: grx::Component = cell;
                        week_grid.append(&cell, x as i32, y as i32, 1, 1);
                    }
                }

                handlers.borrow_mut().retain(|h| {
                    h();
                    false
                });

                let remove = store()
                    .select_dbg("week_grid.select(SELECT_INDEX)", SELECT_INDEX, move |state| {
                        for cell in full_day_cells.iter() {
                            cell.reset_events();
                        }

                        for c in week_grid.children().iter() {
                            if c.props().id() == Some("week_event") {
                                week_grid.remove_child(c);
                            }
                        }

                        for i in 0i32..7 {
                            let day = first_day_of_week.clone()
                                + KarlenderDateTimeDuration::days(i as i64);
                            let events = state.index.index.get_events(&day);
                            let mut colors = HashMap::new();
                            for c in state.calendar.calendars.iter() {
                                if let Some(color) = c.color() {
                                    colors.insert(c.url(), format!("{}50", color));
                                }
                            }
                            for ev in events {
                                if ev.is_full_day() {
                                    if let Some(cell) = full_day_cells.get(i as usize) {
                                        cell.inc_events(ev.summary().cloned().unwrap_or_default());
                                    }
                                } else {
                                    let (start, end) = match (ev.start_local(), ev.end_local()) {
                                        (Ok(s), Ok(e)) => (s, e),
                                        (Ok(s), Err(_)) => (s.clone(), s),
                                        _ => {
                                            log::warn!("Found event with no DTSTART, DTEND: {:?}", ev.local_path());
                                            continue
                                        }
                                    };
                                    let hour_start = start.hour() as i32;
                                    let minute_start = start.minute() as i32;

                                    let hour_end = end.hour() as i32;
                                    let minute_end = end.minute() as i32;

                                    let y_start = hour_start * SLOTS_PER_HOUR as i32 + minute_start / (60/SLOTS_PER_HOUR) as i32;
                                    let y_end =
                                        (hour_end * SLOTS_PER_HOUR as i32 + minute_end / (60/SLOTS_PER_HOUR) as i32)
                                        .max(y_start + 1);
                                    let bg_color = colors.get(ev.calendar_url()).cloned().unwrap_or_else(|| DEFAULT_COLOR.into());
                                    let child = grx! {
                                        container (id="week_event".into(), styles=[p(2), target(false), bg(bg_color.clone())]) [
                                            text(text=ev.summary().cloned().unwrap_or_default(), styles=[hexpand, vexpand, wrap(word_char), text(get_text_color(&bg_color))])
                                        ]
                                    };
                                    week_grid.append(
                                        &child,
                                        1 + i,
                                        2 + y_start,
                                        1,
                                        y_end - y_start,
                                    );
                                }
                            }
                        }
                    })
                    // we can all this savely because selectors always return a successfull result
                    // they return a result to notify us, to handle the remove callback.
                    .unwrap();

                handlers.borrow_mut().push(remove);
            }
        },
    );

    let tz = KarlenderTimezone::local();
    let weak_week_grid = week_grid.downgrade();
    grid.later_drop(
        store().select_dbg("week_grid.select(SELECT_NOW)", SELECT_NOW, move |state| {
            if let Some(week_grid) = weak_week_grid.upgrade() {
                let now = &state.calendar.now;
                let now_0 = now.date().and_hms(0, 0, 0, Some(&tz));
                let delta = now.signed_duration_since(now_0);
                let delta_minutes = delta.num_minutes() as f64;
                let factor = delta_minutes / 1440.0;

                let grid_height = week_grid.height() as u32;
                if week_grid.height() > 0 {
                    let grid_height = (grid_height - week_header_cell::HEIGHT) as f64;
                    let margin = (grid_height * factor) as u32;
                    hour_indicator.set_styles(vec![Style::mt(
                        grx::styles::Modifier::none,
                        week_header_cell::HEIGHT + margin,
                    )]);
                }
            }
        }),
    );

    grid
}

impl WeekGrid {
    pub(crate) fn set_week(self: &Rc<Self>, month: KarlenderDate) {
        self.set_state(move |s| s.week = month.clone());
    }
}

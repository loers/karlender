use gettextrs::gettext;

pub(crate) fn get_week_day_name(num: u8, mobile: bool) -> String {
    match num {
        0 => {
            if mobile {
                gettext("M")
            } else {
                gettext("Mon")
            }
        }
        1 => {
            if mobile {
                gettext("T")
            } else {
                gettext("Tue")
            }
        }
        2 => {
            if mobile {
                gettext("W")
            } else {
                gettext("Wed")
            }
        }
        3 => {
            if mobile {
                gettext("T")
            } else {
                gettext("Thu")
            }
        }
        4 => {
            if mobile {
                gettext("F")
            } else {
                gettext("Fri")
            }
        }
        5 => {
            if mobile {
                gettext("S")
            } else {
                gettext("Sat")
            }
        }
        _ => {
            if mobile {
                gettext("S")
            } else {
                gettext("Sun")
            }
        }
    }
}

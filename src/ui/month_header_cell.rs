use std::rc::Rc;

use grx::{component, components::*, grx, props, styles::Modifier::none, Style};

use crate::store::{store, ui::SELECT_MOBILE};

use super::calendar_utils::get_week_day_name;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {
    pub(crate) day: u8,
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {
    label: String,
}

#[component(Props, State)]
pub(crate) struct MonthHeaderCell {}

pub(crate) fn month_header_cell(props: Props) -> Rc<MonthHeaderCell> {
    let mut border = Style::none(none);
    if props.day < 6 {
        border = Style::border_r(none, 1)
    }
    let txt: Rc<Text>;
    let component = grx! {
        container (
            id="month_header_cell".into(),
            styles=[
                p(4),
                h(40),
                {border},
                hover:bg("alpha(currentColor, 0.1)".into())
            ]
        ) [
            text (
                id=txt,
                styles=[
                    hexpand
                ],
                classes="title-3".into()
            )
        ]
    };

    let mc = MonthHeaderCell::new(props, component);

    let t = txt;
    mc.on_state(move |s| t.set_text(s.label.as_ref()));

    let m = mc.clone();
    mc.later_drop(store().select_dbg(
        "month_header_cell.select(SELECT_MOBILE)",
        SELECT_MOBILE,
        move |s| {
            let lbl = get_week_day_name(m.props.day, s.ui.mobile.is_mobile());
            m.set_state(move |l| l.label = lbl.clone())
        },
    ));

    mc
}

use std::rc::Rc;

use grx::components::*;
use grx::{component, grx, props};

use crate::store::ui::ABOUT_PAGE_NAME;
use crate::{APP_ID, APP_NAME};

pub(crate) const NAME: &str = ABOUT_PAGE_NAME;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct AboutPage {}

pub(crate) fn about_page(props: Props) -> Rc<AboutPage> {
    let version = env!("CARGO_PKG_VERSION");

    let component = grx! {
        container (styles=[p(10)]) [
            clamp (max_width=600, styles=[vexpand, hexpand]) [
                container (styles=[vertical, spacing(10)]) [
                    icon(name=APP_ID, styles=[h(128), w(128)]),
                    text(classes="title-1".into(), text=APP_NAME.into()),
                    text(text=format!("Version: {version}")),
                    container (styles=[vertical]) [
                        "Copyright © 2022 Florian Loers",
                        "This program comes with absolutely no warranty. See the GNU General Public License, version 3 or later for details."
                    ],
                    container (styles=[mt(10), vertical]) [
                        "Created by",
                        "Florian Loers"
                    ]
                ]
            ]
        ]
    };
    AboutPage::new(props, component)
}

use std::rc::Rc;

use grx::components::*;
use grx::{component, grx, props};

use crate::store::selection::SELECT_CURRENT_WEEK;
use crate::store::store;
use crate::store::ui::WEEK_PAGE_NAME;

use super::week_grid::{self, WeekGrid};

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct WeekPage {}

pub(crate) const NAME: &str = WEEK_PAGE_NAME;

pub(crate) fn week_page(props: Props) -> Rc<WeekPage> {
    let week_grid: Rc<WeekGrid>;
    let component = grx! {
        container (styles=[vertical, spacing(10)]) [
            week_grid (id=week_grid)
        ]
    };

    let page = WeekPage::new(props, component);

    page.later_drop(store().select_dbg(
        "week_page.select(SELECT_CURRENT_WEEK)",
        SELECT_CURRENT_WEEK,
        move |state| {
            week_grid.set_week(state.selection.current_week.clone());
        },
    ));

    page
}

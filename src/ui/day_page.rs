use std::rc::Rc;

use gettextrs::gettext;
use grx::components::*;
use grx::{component, grx, props};

use crate::store::selection::SELECT_CURRENT_DAY_AND_INDEX;
use crate::store::store;
use crate::store::ui::DAY_PAGE_NAME;
use crate::ui::{editor_page, list_event};

pub(crate) const NAME: &str = DAY_PAGE_NAME;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct DayPage {}

pub(crate) fn day_page(props: Props) -> Rc<DayPage> {
    let create_new = gettext("Create New");

    let events: Rc<Container>;

    let component = grx! {
        container (styles=[p(10)]) [
            clamp (max_width=600, styles=[vexpand, hexpand]) [
                container (id=events, styles=[vertical, spacing(10)])
            ]
        ]
    };
    let page = DayPage::new(props, component);

    page.later_drop(store().select_dbg("day_page.select(SELECT_CURRENT_DAY_AND_INDEX)", SELECT_CURRENT_DAY_AND_INDEX, move |state| {
        let day = state
            .selection
            .current_day.clone();
        events.clear();
        for ev in state.index.index.get_events(&day) {
            events.append(grx! {
                list_event (event={Some(ev)}, on_click=Some(Rc::new(|e| {
                    if let Some(event) = e.event() {
                        store().dispatch(crate::store::editor::Action::Edit(event.clone()));
                    }
                    store().dispatch(crate::store::ui::Action::Push(editor_page::NAME))
                })))
            })
        }

        events.append(grx! {
            list (classes="boxed-list".into()) [
                action_row (title=create_new.clone(), selectable=Some(false), activatable=Some(true), on_click=|| {
                    store().dispatch(crate::store::editor::Action::NewEvent)
                })
            ]
        })
    }));

    page
}

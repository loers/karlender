use std::rc::Rc;

use grx::{component, components::*, grx, props, Style};

use crate::{
    domain::DEFAULT_COLOR,
    store::{calendar::SELECT_CALENDARS, store},
};

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {
    pub(crate) event: Option<crate::domain::Event>,
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {
    text: String,
    alt_text: String,
}

#[component(Props, State)]
pub(crate) struct Event {}

pub(crate) fn event(props: Props) -> Rc<Event> {
    let bg: Rc<Container>;
    if props.event.is_none() {
        let empty = grx! {container};
        return Event::new(props, empty);
    }
    let ev = props.event.as_ref();

    let text = ev
        .and_then(|e| e.summary().cloned())
        .unwrap_or_else(|| "".into());

    let height = (text.len() * 3).max(20) as u32;
    let height = height.min(30);

    let id = format!("{:?}", ev.and_then(|e| e.url()));
    let cal_url = ev.map(|e| e.calendar_url().to_string()).unwrap_or_default();

    let txt: Rc<Text>;
    let component = grx! {
        container (id={id}, styles=[
            vertical,
            valign(start),
            mt(4)
        ]) [
            container (id=bg, styles=[bg(DEFAULT_COLOR.into())]) [
                scroll (styles=[overflow(external), vexpand, h(height)]) [
                    container (styles=[
                        vertical,
                        spacing(3),
                        p(2)
                    ]) [
                        text (
                            id=txt,
                            styles=[
                                justify(middle),
                                wrap(word_char),
                                text_size("xs".into()), // TODO
                                valign(start),
                                halign(center),
                                hexpand
                            ],
                            text={text.clone()}
                        )
                    ]
                ]
            ]
        ]
    };
    let event = Event::new(props, component);

    event.later_drop(store().select_dbg(
        "event.select(SELECT_CALENDARS)",
        SELECT_CALENDARS,
        move |state| {
            let calendar = state
                .calendar
                .calendars
                .iter()
                .find(|c| c.url().as_str() == cal_url);

            if let Some(calendar) = calendar {
                bg.set_styles(vec![Style::bg(
                    grx::styles::Modifier::none,
                    calendar
                        .color()
                        .cloned()
                        .unwrap_or_else(|| DEFAULT_COLOR.into()),
                )])
            }
        },
    ));

    event.set_state(move |s| s.text = text.clone());
    event.on_state(move |s| {
        if !s.alt_text.is_empty() {
            txt.set_text(&s.alt_text);
        } else {
            txt.set_text(&s.text);
        }
    });
    event
}

impl Event {
    pub(crate) fn set_alt_text(self: &Rc<Self>, text: String) {
        self.set_state(move |s| s.alt_text = text.clone());
    }
    pub(crate) fn remove_alt_text(self: &Rc<Self>) {
        self.set_state(move |s| s.alt_text = String::new());
    }
}

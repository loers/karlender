use super::month_cell;
use crate::{
    domain::time::{get_month_by_delta, get_month_len, KarlenderDate, KarlenderTimezone},
    store::{store, ui::SELECT_GOTO_NOW},
    ui::{month_cell::MonthCell, month_header_cell},
};
use glib::clone::Downgrade;
use grx::{component, components::*, grid::Grid, grx, props};
use std::{cell::RefCell, rc::Rc};

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct State {
    month: KarlenderDate,
}
impl Default for State {
    fn default() -> Self {
        Self {
            month: KarlenderTimezone::local().today(),
        }
    }
}
#[component(Props, State)]
pub(crate) struct MonthGrid {}

pub(crate) fn month_grid(props: Props) -> Rc<MonthGrid> {
    let month_grid: Rc<Grid>;

    let component = grx! {
        container [
            grid (
                id=month_grid,
                styles=[hexpand, vexpand],
                col_homogeneous=true
            ) [
                #[row=0, col=0] month_header_cell (day=0),
                #[row=0, col=1] month_header_cell (day=1),
                #[row=0, col=2] month_header_cell (day=2),
                #[row=0, col=3] month_header_cell (day=3),
                #[row=0, col=4] month_header_cell (day=4),
                #[row=0, col=5] month_header_cell (day=5),
                #[row=0, col=6] month_header_cell (day=6)
            ]
        ]
    };

    let grid = MonthGrid::new(props, component);
    let g = grid.clone();
    grid.later_drop(store().select_dbg(
        "month_grid.select(SELECT_GOTO_NOW)",
        SELECT_GOTO_NOW,
        move |_| {
            g.set_state(|s| {
                s.month = KarlenderTimezone::local().today();
            })
        },
    ));

    grid.select_state(
        |a, b| a.month != b.month,
        move |state| {
            let state: &State = state;
            for _ in 1..7 {
                month_grid.remove_row(1);
            }
            let this_month = &state.month;
            let this_month_first_day = this_month.with_day(1).unwrap();
            let former_month = get_month_by_delta(this_month, -1);
            let former_month_len = get_month_len(&former_month);
            let next_month = get_month_by_delta(this_month, 1);
            let this_month_len = get_month_len(this_month);
            let this_month_start = this_month_first_day.weekday().number_from_monday();
            let mut whole_week_not_in_month = false;

            let cells: Rc<RefCell<Vec<std::rc::Weak<MonthCell>>>> = Rc::new(RefCell::default());
            for week in 0..6 {
                for day in 0..7 {
                    let day_num = day + (week * 7);

                    let (date, in_current_month) = if day_num < this_month_start {
                        (
                            former_month
                                .with_day0(former_month_len - (this_month_start - day_num))
                                .unwrap(),
                            false,
                        )
                    } else if day_num - this_month_start >= this_month_len {
                        (
                            next_month
                                .with_day0(day_num - this_month_start - this_month_len)
                                .unwrap(),
                            false,
                        )
                    } else {
                        (
                            this_month.with_day0(day_num - this_month_start).unwrap(),
                            true,
                        )
                    };

                    if week > 0 && day == 0 && !in_current_month || whole_week_not_in_month {
                        whole_week_not_in_month = true;
                        continue;
                    }

                    let x = day;
                    let y = 1 + week;
                    let mut props = month_cell::Props {
                        day: date,
                        pos: (x, y),
                        other_month: !in_current_month,
                        ..Default::default()
                    };
                    let moved_cells = cells.clone();
                    props.on_click = Some(Rc::new(move |cell| {
                        let selected = cell.selected();
                        for cell in &*moved_cells.borrow() {
                            if let Some(cell) = cell.upgrade() {
                                cell.deselect();
                            }
                        }
                        cell.select(); // trigger gstore action dispatch in any case
                        if selected {
                            cell.deselect()
                        }
                    }));
                    let cell: Rc<MonthCell> = month_cell::month_cell(props);
                    let cells = &mut *cells.as_ref().borrow_mut();
                    cells.push(cell.downgrade());
                    let cell: grx::Component = cell;
                    month_grid.append(&cell, x as i32, y as i32, 1, 1);
                }
            }
        },
    );

    grid
}

impl MonthGrid {
    pub(crate) fn set_month(self: &Rc<Self>, month: KarlenderDate) {
        self.set_state(move |s| s.month = month.clone());
    }
}

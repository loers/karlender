use std::rc::Rc;

use crate::store::store;
use crate::{APP_ID, APP_NAME};
use gettextrs::gettext;
use glib::clone::Downgrade;
use grx::components::*;
use grx::{component, grx, props};
use percent_encoding::{utf8_percent_encode, AsciiSet, CONTROLS};

pub(crate) const NAME: &str = "feedback";

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct AboutPage {}

const MAILTO: &str = "mailto:contact-project+loers-karlender-32697518-issue-@incoming.gitlab.com";

const MAIL_TEMPLATE: &str = r#"
## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots


<details>
  <summary>My latest logs</summary>

  ```
Please add the logs you want to share here.
```
</details>
"#;

pub(crate) fn feedback_page(props: Props) -> Rc<AboutPage> {
    let message = gettext(
        "You can use this page to submit issues to _APPNAME_ and get the application logs.",
    )
    .replace("_APPNAME_", APP_NAME);
    let submit = gettext("Submit Issue");
    let logs = gettext("Show Logs");

    let submit_btn: Rc<Button>;
    let mailto_btn: Rc<Button>;
    let component = grx! {
        container (styles=[p(10)]) [
            clamp (max_width=600, styles=[vexpand, hexpand]) [
                container (styles=[vertical, spacing(10)]) [
                    icon(name=APP_ID, styles=[h(128), w(128)]),
                    container (styles=[vertical]) [
                        text(text=message)
                    ],
                    container (styles=[mt(10), vertical, spacing(10)]) [
                        button (id=submit_btn) [
                            text(text=submit)
                        ],
                        button (on_click=|| {
                            store().dispatch(crate::store::notification::Action::InApp {
                                toast: Toast::new(&gettext("Opening logs...")),
                                dismiss: None,
                                timeout: None,
                            });
                            store().dispatch(crate::store::editor::Action::OpenLogs)
                        }) [
                            text(text=logs)
                        ],

                        button (id=mailto_btn, url=Some(MAILTO.into())) [
                            "mailto:karlender"
                        ]
                    ]
                ]
            ]
        ]
    };

    let mw = mailto_btn.downgrade();
    submit_btn.on_click(move |_| {
        if let Some(mailto_btn) = mw.upgrade() {
            const FRAGMENT: &AsciiSet = &CONTROLS.add(b' ').add(b'"').add(b'<').add(b'>').add(b'`');
            let url = format!("{}?body={}", MAILTO, MAIL_TEMPLATE);
            let url = utf8_percent_encode(&url, FRAGMENT);
            mailto_btn.set_url(&url.to_string());
            mailto_btn.click();
            mailto_btn.set_url(MAILTO);
        }
    });

    mailto_btn.on_click(move |_| {
        store().dispatch(crate::store::notification::Action::InApp {
            toast: Toast::new(&gettext("Opening mail...")),
            dismiss: None,
            timeout: None,
        })
    });
    AboutPage::new(props, component)
}

// fn get_log_messages() -> String {
//     if let Ok(logs) = std::fs::read_to_string(crate::domain::get_app_dir().join("karlender.log")) {
//         logs
//     } else {
//         "No logs found".into()
//     }
// }

use std::rc::Rc;

use gettextrs::gettext;
use glib::clone::Downgrade;
use grx::{
    component,
    components::*,
    grx, props,
    styles::{Align, Modifier::none},
    Style,
};

use crate::{
    domain::time::{KarlenderDateTime, KarlenderTimezone},
    store::{selection, store},
};

use super::week_grid::STEPS;

type OnClick = Option<Rc<dyn Fn(&Rc<WeekCell>)>>;

#[props]
pub(crate) struct Props {
    pub(crate) timeslot: KarlenderDateTime,
    pub(crate) pos: (u16, u16),
    pub(crate) on_click: OnClick,
    pub(crate) full_day: bool,
}

impl std::fmt::Debug for Props {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Props")
            .field("id", &self.id)
            .field("classes", &self.classes)
            .field("styles", &self.styles)
            .field("children", &self.children)
            .field("timeslot", &self.timeslot)
            .field("full_day", &self.full_day)
            .field("pos", &self.pos)
            .field("on_click", &self.on_click.is_some())
            .finish()
    }
}

impl Default for Props {
    fn default() -> Self {
        Self {
            id: Default::default(),
            classes: Default::default(),
            styles: Default::default(),
            children: Default::default(),
            timeslot: KarlenderTimezone::local().now(),
            pos: Default::default(),
            full_day: false,
            on_click: Default::default(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {
    label: String,
    selected: bool,
    events: Vec<String>,
}

#[component(Props, State)]
pub(crate) struct WeekCell {}

pub(crate) fn week_cell(props: Props) -> Rc<WeekCell> {
    let events = gettext("Events");

    let border;

    if props.pos.0 < 7 && (props.pos.1 < 2 || (props.pos.1 - 2).rem_euclid(STEPS) == 0) {
        border = Style::border(none, 1, 1, 0, 0)
    } else if props.pos.0 < 7 {
        border = Style::border(none, 0, 1, 0, 0)
    } else if props.pos.1 < 2 || (props.pos.1 - 2).rem_euclid(STEPS) == 0 {
        border = Style::border(none, 1, 0, 0, 0)
    } else {
        border = Style::border(none, 0, 0, 0, 0)
    }

    let ts = props.timeslot.clone();

    let week_cell: Rc<Container>;
    let label: Rc<Text>;
    let component = grx! {
        container (
            id=week_cell,
            styles=[
                h(10),
                {border},
                vexpand,
                hover:bg("alpha(currentColor, 0.1)".into()),
                active:bg("alpha(@theme_unfocused_selected_bg_color, 0.4)".into())
            ]
        ) [
            container [
                text (
                    id=label,
                    styles=[
                        m(4),
                        valign(start),
                        hexpand
                    ]
                    // text={timeslot}
                )
            ]
        ]
    };

    if !props.full_day {
        label.set_visible(false);
        // label.set_styles(vec![Style::text_size(none, "1.5rem".into())]);
    }

    // week_cell.on_swipe(|_, x, _y| {
    //     if x > 400. {
    //         store().dispatch(selection::Action::PrevWeek)
    //     } else if x < -400. {
    //         store().dispatch(selection::Action::NextWeek)
    //     }
    // });

    let onclick = props.on_click.clone();
    let cell = WeekCell::new(props, component);

    let txt = label.downgrade();
    cell.on_state(move |s| {
        if let Some(txt) = txt.upgrade() {
            #[allow(clippy::comparison_chain)]
            if s.events.len() == 1 {
                txt.set_styles(vec![Style::ellipsize(none, Align::end)]);
                txt.set_text(&s.events[0])
            } else if s.events.len() > 1 {
                txt.set_text(&format!("{}\n{}", s.events.len(), events))
            }
        }
    });

    let d = ts;
    let weak = week_cell.downgrade();
    cell.on_state(move |s| {
        if let Some(month_cell) = weak.upgrade() {
            if s.selected {
                month_cell.add_class("selected");
                store().dispatch(selection::Action::SelectTimeslot(d.clone()))
            } else {
                month_cell.remove_class("selected");
            }
        }
    });

    let weak = cell.downgrade();
    if let Some(och) = onclick {
        week_cell.on_click(move |_| {
            if let Some(cell) = weak.upgrade() {
                let h = &*och;
                h(&cell);
            }
        });
    }

    cell
}

impl WeekCell {
    pub(crate) fn select(self: &Rc<Self>) {
        self.set_state(|s| s.selected = true);
    }
    pub(crate) fn selected(self: &Rc<Self>) -> bool {
        self.state.borrow().selected
    }
    pub(crate) fn deselect(self: &Rc<Self>) {
        self.set_state(|s| s.selected = false);
    }

    pub(crate) fn inc_events(self: &Rc<Self>, event: String) {
        self.set_state(move |s| s.events.push(event.clone()))
    }
    pub(crate) fn reset_events(self: &Rc<Self>) {
        self.set_state(|s| s.events.clear())
    }
}

impl Drop for WeekCell {
    fn drop(&mut self) {
        self.drop_selectors_from_store();
    }
}

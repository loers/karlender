use std::rc::Rc;

use gettextrs::gettext;
use grx::action_row::ActionRow;
use grx::components::*;
use grx::list::List;
use grx::{component, entry::Entry, grx, props};

use crate::domain::Login;
use crate::store::login::{Action, PASSWORD, SELECT_LOGIN};
use crate::store::{dispatch_err, store};

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct LoginRow {}

pub(crate) fn login_row(props: Props) -> Rc<LoginRow> {
    let url_row: Rc<List>;
    let email_row: Rc<List>;
    let password_row: Rc<List>;
    let submit_row: Rc<List>;
    let logged_in_row: Rc<List>;
    let logged_in_action_row: Rc<ActionRow>;

    let url_entry: Rc<Entry>;
    let email_entry: Rc<Entry>;
    let password_entry: Rc<Entry>;

    let component = grx! {
        container (styles=[vertical, spacing(10), m(2)]) [
            list (id=url_row, styles=[hexpand, valign(start)], classes="boxed-list".into()) [
                action_row (
                    title=gettext("CalDAV Url"),
                    selectable=Some(false),
                    expand_suffix=Some(true)
                ) [
                    entry (
                        id=url_entry,
                        styles=[valign(center), hexpand]
                    )
                ]
            ],
            list (id=email_row, styles=[hexpand, valign(start)], classes="boxed-list".into()) [
                action_row (
                    title=gettext("E-Mail"),
                    selectable=Some(false)
                ) [
                    entry (
                        id=email_entry,
                        styles=[valign(center)]
                    )
                ]
            ],
            list (id=password_row, styles=[hexpand, valign(start)], classes="boxed-list".into()) [
                action_row (
                    title=gettext("Password"),
                    selectable=Some(false)
                ) [
                    entry (
                        id=password_entry,
                        password=Some(true),
                        styles=[valign(center)]
                    )
                ]
            ],
            list (id=submit_row, styles=[hexpand, valign(start)], classes="boxed-list".into()) [
                button (
                    styles=[m(6)],
                    classes="flat".into(),
                    on_click=move || {
                        match Login::new(&url_entry.text(), &email_entry.text()) {
                            Ok(login) => {
                                if let Some(mut lock) = PASSWORD.try_lock() {
                                    lock.replace(password_entry.text());
                                    store().dispatch(Action::Login(login));
                                } else {
                                    dispatch_err("Failed process password internally.");
                                }
                            }
                            Err(e) => {
                                dispatch_err(format!("Login url is not well formatted: {e}"))
                            }
                        }
                    }
                ) [
                    "Login"
                ]
            ],
            list (id=logged_in_row, styles=[hexpand, valign(start)], classes="boxed-list".into()) [
                action_row (id=logged_in_action_row, selectable=Some(false)) [
                    button (
                        styles=[valign(center)],
                        on_click=move || store().dispatch(Action::Logout)
                    ) [
                        icon ("app-remove-symbolic")
                    ]
                ]
            ]
        ]
    };
    let login_row = LoginRow::new(props, component);

    login_row.later_drop(store().select_dbg(
        "login_row.select(SELECT_LOGIN)",
        SELECT_LOGIN,
        move |state| {
            let visible = match &state.login.login {
                Some(login) => {
                    logged_in_action_row.set_title(login.url.as_str());
                    logged_in_action_row.set_subtitle(login.email.as_str());
                    false
                }
                None => true,
            };
            url_row.set_visible(visible);
            email_row.set_visible(visible);
            password_row.set_visible(visible);
            submit_row.set_visible(visible);
            logged_in_row.set_visible(!visible);
        },
    ));

    login_row
}

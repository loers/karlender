use std::rc::Rc;

use gettextrs::gettext;
use grx::{
    component,
    components::*,
    grx,
    header_bar::{self, HeaderBar},
    menu::{self, Menu},
    props,
    spinner::Spinner,
    Component,
};

use crate::{
    store::{
        selection::SELECT_HISTORY_AND_SELECTION_AND_MOBILE,
        store,
        sync::SELECT_SYNCING,
        ui::{self, Mobile, SELECT_HISTORY_AND_MOBILE, SELECT_MOBILE, SELECT_TITLE},
        Action,
    },
    ui::editor_page,
    APP_NAME,
};

use super::{day_page, month_page, week_page};

#[props]
#[derive(Debug, Default)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct Root {}

pub(crate) fn header(_props: Props) -> Component {
    let menu: Rc<Menu>;
    let header: Rc<HeaderBar>;

    let prev: Rc<Button>;
    let next: Rc<Button>;
    let add: Rc<Button>;
    let save_btn: Rc<Button>;

    let spinner: Rc<Spinner>;

    let save = gettext("Save");

    let settings = gettext("Settings");
    let feedback = gettext("Feedback");
    let about = gettext("About");
    let open = gettext("Open");
    let open_dir = gettext("Open App Folder");
    let sync = gettext("Sync");
    let quit = gettext("Quit");

    let c = grx! {
        header_bar (id=header) [
            button (id=prev) [
                icon("go-previous-symbolic")
            ],
            button (id=next) [
                icon("go-next-symbolic")
            ],
            button (id=add, on_click=|| {
                store().dispatch(crate::store::editor::Action::NewEvent)
            }) [
                icon("list-add-symbolic")
            ],
            button (id=save_btn, on_click=|| {
                let _a = async_std::task::spawn(async {
                    async_std::task::sleep(std::time::Duration::from_millis(101)).await;
                    store().dispatch(crate::store::editor::Action::Save);
                });
            }) [
                text(text=save)
            ],
            #[pack="end"]
            container [
                menu (id=menu, items=vec![
                    menu_item(open, Some(Action::Open)),
                    menu_item(open_dir, Some(Action::Editor(crate::store::editor::Action::OpenFolder))),
                    menu_item(sync, Some(Action::Sync(crate::store::sync::Action::Syncing {report_errors: true}))),
                    menu_item(settings, Some(Action::UI(crate::store::ui::Action::NavigateSettings))),
                    menu_item(about, Some(Action::UI(crate::store::ui::Action::NavigateAbout))),
                    menu_item(feedback, Some(Action::UI(crate::store::ui::Action::NavigateFeedback))),
                    menu_item(quit, Some(Action::Quit)),
                ]),
                button (on_click=move || menu.show()) [
                    icon("open-menu-symbolic")
                ]
            ],
            #[pack="end"]
            button (on_click=|| {
                store().dispatch(crate::store::ui::Action::GotoNow)
            }) [
                icon("find-location-symbolic")
            ],
            spinner (id=spinner)
        ]
    };

    next.on_click(|_| {
        store().dispatch(ui::Action::Next);
    });
    prev.on_click(|_| {
        store().dispatch(ui::Action::Back);
    });

    store()
        .select_dbg(
            "header.select(SELECT_SYNCING)",
            SELECT_SYNCING,
            move |state| {
                spinner.set_visible(state.sync.syncing);
            },
        )
        .expect("Does not need to be dropped ever.");

    store()
        .select_dbg(
            "header.select(SELECT_HISTORY_AND_MOBILE)",
            SELECT_HISTORY_AND_MOBILE,
            move |state| {
                let month_controls = state.ui.history.back() == Some(&month_page::NAME);

                let day_controls = state.ui.history.back() == Some(&day_page::NAME);

                let week_controls = state.ui.history.back() == Some(&week_page::NAME);

                next.set_visible(month_controls || day_controls || week_controls);
                prev.set_visible(
                    month_controls || day_controls || week_controls || state.ui.history.len() > 1,
                );

                let current_page = state.ui.history.back().cloned().unwrap_or_default();

                save_btn.set_visible(
                    current_page == editor_page::NAME && state.ui.mobile != Mobile::Portrait,
                );
            },
        )
        .expect("Does not need to be dropped ever.");

    store()
        .select_dbg(
            "header.select(SELECT_HISTORY_AND_SELECTION_AND_MOBILE)",
            SELECT_HISTORY_AND_SELECTION_AND_MOBILE,
            move |state| {
                let current_page = state.ui.history.back().cloned().unwrap_or_default();
                add.set_visible(
                    state.selection.selection.is_some()
                        && current_page != editor_page::NAME
                        && !state.ui.mobile.is_mobile(),
                );
            },
        )
        .expect("Does not need to be dropped ever.");

    let h = header.clone();
    store()
        .select_dbg("header.select(SELECT_TITLE)", SELECT_TITLE, move |state| {
            if let Some(title) = state.ui.title.as_ref() {
                h.set_title(title);
            } else {
                h.set_title(APP_NAME);
            }
        })
        .expect("Does not need to be dropped ever.");
    let h = header.clone();
    store()
        .select_dbg(
            "header.select(SELECT_MOBILE)",
            SELECT_MOBILE,
            move |state| {
                let mobile = state.ui.mobile.clone();
                if mobile.is_mobile() {
                    h.add_class("flat");
                } else {
                    h.remove_class("flat");
                }
            },
        )
        .expect("Does not need to be dropped ever.");
    // store()
    //     .select_dbg("header.select(SELECT_CURRENT_MONTH)", SELECT_CURRENT_MONTH, move |state| {
    //         // update_title(state, &header);
    //     })
    //     .expect("Does not need to be dropped ever.");
    store()
        .select_dbg("header.select(SELECT_TITLE)", SELECT_TITLE, move |state| {
            // header.set_title(&state.ui.title);
            if let Some(title) = state.ui.title.as_ref() {
                header.set_title(title);
            } else {
                header.set_title(APP_NAME);
            }
        })
        .expect("Does not need to be dropped ever.");
    c
}

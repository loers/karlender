use std::rc::Rc;

use gettextrs::gettext;
use glib::clone::Downgrade;
use grx::{component, components::*, grx, leaflet::Leaflet, props, styles::Modifier::none, Style};

use crate::{
    domain::time::{KarlenderDate, KarlenderTimezone},
    store::{calendar::SELECT_NOW, index::SELECT_INDEX, selection, store},
    ui::event,
};

type OnClick = Option<Rc<dyn Fn(&Rc<MonthCell>)>>;

#[props]
pub(crate) struct Props {
    pub(crate) day: KarlenderDate,
    pub(crate) pos: (u8, u8),
    pub(crate) other_month: bool,
    pub(crate) label: String,
    pub(crate) on_click: OnClick,
}

impl std::fmt::Debug for Props {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Props")
            .field("id", &self.id)
            .field("classes", &self.classes)
            .field("styles", &self.styles)
            .field("children", &self.children)
            .field("day", &self.day)
            .field("pos", &self.pos)
            .field("other_month", &self.other_month)
            .field("label", &self.label)
            .field("on_click", &self.on_click.is_some())
            .finish()
    }
}

impl Default for Props {
    fn default() -> Self {
        Self {
            id: Default::default(),
            classes: Default::default(),
            styles: Default::default(),
            children: Default::default(),
            day: KarlenderTimezone::local().today(),
            pos: Default::default(),
            other_month: Default::default(),
            label: Default::default(),
            on_click: Default::default(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {
    label: String,
    selected: bool,
}

#[component(Props, State)]
pub(crate) struct MonthCell {}

pub(crate) fn month_cell(props: Props) -> Rc<MonthCell> {
    let border = if props.pos.0 < 6 {
        Style::border(none, 1, 1, 0, 0)
    } else {
        Style::border(none, 1, 0, 0, 0)
    };

    let mut classes = "".to_string();
    if props.other_month {
        classes = "unfocused".to_string();
    }

    let month_cell: Rc<Container>;
    let events_leaflet: Rc<Leaflet>;
    let component = grx! {
        container (
            id=month_cell,
            classes={classes},
            styles=[
                h(40),
                {border},
                vexpand,
                hover:bg("alpha(currentColor, 0.1)".into()),
                active:bg("alpha(@theme_unfocused_selected_bg_color, 0.4)".into())
            ]
        ) [
            leaflet (
                styles=[vertical],
                visible_child=Some("events_leaflet"),
                fold_threshold_natural=false
            ) [
                text (
                    id="day_label".into(),
                    styles=[
                        valign(start),
                        text("mix(@theme_text_color, @borders, 0.85)".into()),
                        hexpand
                    ],
                    classes="title-3".into(),
                    text={props.day.day().to_string()}
                ),
                leaflet (
                    id=events_leaflet,
                    styles=[vertical]
                )
            ]
        ]
    };

    // month_cell.on_swipe(|_, x, _y| {
    //     if x > 400. {
    //         store().dispatch(selection::Action::PrevMonth)
    //     } else if x < -400. {
    //         store().dispatch(selection::Action::NextMonth)
    //     }
    // });

    events_leaflet.on_fold(|ll| {
        let len = ll.children().len();
        if let Some(event) = ll.children().first().cloned() {
            if let Ok(ev) = event.into_any().downcast::<crate::ui::event::Event>() {
                if ll.is_folded() {
                    let msg = gettext("Events");
                    ev.set_alt_text(format!("{} {}", len, msg));
                } else {
                    ev.remove_alt_text();
                }
            }
        }
    });

    let day = props.day.clone();
    let onclick = props.on_click.clone();
    let cell = MonthCell::new(props, component);

    let moved_day = day.clone();
    let weak_month_cell = month_cell.downgrade();
    cell.later_drop(store().select_dbg(
        "month_cell.select(SELECT_NOW)",
        SELECT_NOW,
        move |state| {
            if let Some(moved_month_cell) = weak_month_cell.upgrade() {
                if moved_day == state.calendar.now.date() {
                    moved_month_cell.add_class("now");
                } else {
                    moved_month_cell.remove_class("now");
                }
            }
        },
    ));

    let weak_events = events_leaflet.downgrade();
    let moved_day = day.clone();

    cell.later_drop(store().select_dbg(
        "month_cell.select(SELECT_INDEX)",
        SELECT_INDEX,
        move |state| {
            if let Some(el) = weak_events.upgrade() {
                el.clear();
                let events = state.index.index.get_events(&moved_day);
                for ev in events {
                    el.append(grx! {
                        event (event = {Some(ev)})
                    });
                }
            }
        },
    ));

    let d = day;
    let weak = month_cell.downgrade();
    cell.on_state(move |s| {
        if let Some(month_cell) = weak.upgrade() {
            if s.selected {
                month_cell.add_class("selected");
                store().dispatch(selection::Action::SelectDay(d.clone()))
            } else {
                month_cell.remove_class("selected");
            }
        }
    });

    let weak = cell.downgrade();
    if let Some(och) = onclick {
        month_cell.on_click(move |_| {
            if let Some(cell) = weak.upgrade() {
                let h = &*och;
                h(&cell);
            }
        });
    }

    cell
}

impl MonthCell {
    pub(crate) fn select(self: &Rc<Self>) {
        self.set_state(|s| s.selected = true);
    }
    pub(crate) fn selected(self: &Rc<Self>) -> bool {
        self.state.borrow().selected
    }
    pub(crate) fn deselect(self: &Rc<Self>) {
        self.set_state(|s| s.selected = false);
    }
}

impl Drop for MonthCell {
    fn drop(&mut self) {
        self.drop_selectors_from_store();
    }
}

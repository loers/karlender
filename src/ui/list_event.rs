use std::rc::Rc;

use grx::{action_row::ActionRow, component, components::*, grx, handlers::Handler, props};

use crate::{
    domain::{time::KarlenderDateTimeDuration, Event},
    store::store,
};

#[props]
#[derive(Default)]
pub(crate) struct Props {
    pub(crate) event: Option<crate::domain::Event>,
    pub(crate) on_click: Option<Handler<ListEvent>>,
}

impl std::fmt::Debug for Props {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Props")
            .field("id", &self.id)
            .field("classes", &self.classes)
            .field("styles", &self.styles)
            .field("children", &self.children)
            .field("event", &self.event)
            .field("on_click", &self.on_click.is_some())
            .finish()
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct ListEvent {}

pub(crate) fn list_event(props: Props) -> Rc<ListEvent> {
    if props.event.is_none() {
        let empty = grx! {container};
        return ListEvent::new(props, empty);
    }
    let ev = props.event.clone();

    let title = ev
        .as_ref()
        .and_then(|e| e.summary().cloned())
        .unwrap_or_default();

    let start = ev.as_ref().and_then(|e| e.start_local().ok());
    let end = ev.as_ref().and_then(|e| e.end_local().ok());

    let location = ev.as_ref().and_then(|e| e.location().cloned());

    let subtitle = if let (Some(start), Some(end)) = (start, end) {
        if start.clone() + KarlenderDateTimeDuration::days(1) == end || start == end {
            start.format(crate::domain::time::DatetimeFormat::DateBottomUp)
        } else {
            format!(
                "{}{}\n{}",
                location.map(|l| format!("{}\n", l)).unwrap_or_default(),
                start.format(crate::domain::time::DatetimeFormat::DateBottomUpAndTime24),
                end.format(crate::domain::time::DatetimeFormat::DateBottomUpAndTime24),
            )
        }
    } else {
        String::new()
    };
    let delete_event = ev;
    let row: Rc<ActionRow>;
    let component = grx! {
        list (classes="boxed-list".into()) [
            action_row (
                id=row,
                title={title},
                subtitle={subtitle},
                selectable=Some(false),
                activatable=Some(true)
            ) [
                button (styles=[valign(center)], on_click=move || {
                    if let Some(delete_event) = &delete_event {
                        store().dispatch(crate::store::editor::Action::Remove(delete_event.clone()))
                    }
                }) [
                    icon("app-remove-symbolic")
                ]
            ]
        ]
    };

    let onclick = props.on_click.clone();
    let event = ListEvent::new(props, component);
    if let Some(on_click) = onclick {
        let e = event.clone();
        row.on_click(Rc::new(move |_| {
            on_click(e.clone());
        }));
    }

    event
}

impl ListEvent {
    pub fn event<'a>(self: &'a Rc<Self>) -> &'a Option<Event> {
        &self.props.event
    }
}

use std::rc::Rc;

use gettextrs::gettext;
use grx::components::*;
use grx::toast_overlay::ToastOverlay;
use grx::view_stack::ViewStack;
use grx::view_switcher_bar::ViewSwitcherBar;
use grx::{component, grx, props};

use crate::store::notification::SELECT_IN_APP_NOTIFICATION;
use crate::store::store;
use crate::store::ui::{Action, Mobile, SELECT_HISTORY, SELECT_MOBILE};
use crate::ui::{
    about_page, day_page, editor_page, feedback_page, settings_page, sidebar, week_page, year_page,
};

use super::month_page;

#[props]
#[derive(Default, Debug)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub(crate) struct State {}

#[component(Props, State)]
pub(crate) struct Main {}

const MAIN_PAGES: &[&str] = &[month_page::NAME, "week", "day", "year"];

pub(crate) fn main(_: Props) -> Component {
    let stack: Rc<ViewStack>;
    let bar: Rc<ViewSwitcherBar>;
    let toast_overlay: Rc<ToastOverlay>;

    // to allow gettext to pick this up. Doesn't seem to work in macro code...
    let month = gettext("Month");
    let week = gettext("Week");
    let day = gettext("Day");
    let year = gettext("Year");

    let component = grx! {
        toast_overlay (id=toast_overlay) [
            leaflet (
                visible_child=Some("main"),
                on_fold=|l| {
                    if l.is_folded() {
                        store().dispatch(Action::SetMobile(Mobile::Portrait))
                    } else {
                        store().dispatch(Action::SetMobile(Mobile::NoPortrait))
                    }
                }
            ) [
                sidebar (id="sidebar".into(), styles=[border_r(1)]),
                container (id="main".into(), styles = [w(300), vertical]) [
                    scroll [
                        view_stack (id=stack, styles=[vexpand, hexpand]) [
                            #[name={month_page::NAME}, title=month, icon="month-symbolic"]
                            month_page,

                            #[name=week_page::NAME, title=week, icon="week-symbolic"]
                            week_page (id={month_page::NAME.into()}),

                            #[name=day_page::NAME, title=day, icon="view-list-bullet-symbolic"]
                            day_page,

                            #[name=year_page::NAME, title=year, icon="view-list-bullet-symbolic"]
                            year_page,

                            #[name=settings_page::NAME]
                            settings_page,

                            #[name={editor_page::NAME}]
                            editor_page,

                            #[name={about_page::NAME}]
                            about_page,

                            #[name={feedback_page::NAME}]
                            feedback_page
                        ]
                    ],
                    view_switcher_bar (id=bar, stack=Some(stack.clone()))
                ]
            ]
        ]
    };

    stack.on_change_view(|name| {
        if let Some(page) = MAIN_PAGES.iter().find(|p| **p == name) {
            store().dispatch(Action::Replace(page));
        }
    });

    store()
        .select_dbg(
            "main.select(SELECT_HISTORY)",
            SELECT_HISTORY,
            move |state| {
                if let Some(name) = state.ui.history.back() {
                    stack.set_visible_child_name(name);
                }
            },
        )
        .expect("Does not need to be dropped ever");

    store()
        .select_dbg("main.select(SELECT_MOBILE)", SELECT_MOBILE, move |state| {
            bar.set_visible(state.ui.mobile == Mobile::Portrait);
        })
        .expect("Does not need to be dropped ever");

    store()
        .select_dbg(
            "main.select(SELECT_IN_APP_NOTIFICATION)",
            SELECT_IN_APP_NOTIFICATION,
            move |state| {
                if let Some((toast, dismiss_action, timeout_action)) =
                    state.notification.in_app.as_ref()
                {
                    let da = dismiss_action.clone();
                    let ta = timeout_action.clone();

                    toast_overlay.show(
                        toast,
                        Some(Rc::new(move || {
                            if let Some(da) = da.as_ref() {
                                store().dispatch(da.clone());
                                store().dispatch(crate::store::notification::Action::Dismiss);
                            }
                        })),
                        Some(Rc::new(move || {
                            if let Some(ta) = ta.as_ref() {
                                store().dispatch(ta.clone());
                            }
                        })),
                    );
                }
            },
        )
        .expect("Does not need to be dropped ever");

    component
}

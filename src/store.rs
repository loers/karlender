use std::{fmt::Debug, rc::Rc};

use gstore::{Actionable, Keybinding};

pub(crate) mod calendar;
pub(crate) mod editor;
pub(crate) mod index;
pub(crate) mod login;
pub(crate) mod notification;
pub(crate) mod selection;
pub(crate) mod settings;
pub(crate) mod sync;
pub(crate) mod ui;

#[derive(Debug, Default, Clone)]
pub(crate) struct State {
    pub(crate) notification: notification::Slice,
    pub(crate) login: login::Slice,
    pub(crate) ui: ui::Slice,
    pub(crate) sync: sync::Slice,
    pub(crate) calendar: calendar::Slice,
    pub(crate) index: index::Slice,
    pub(crate) selection: selection::Slice,
    pub(crate) editor: editor::Slice,
    pub(crate) settings: settings::Slice,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Action {
    Init,
    Quit,
    Open,
    Error(String),
    Notification(notification::Action),
    Login(login::Action),
    UI(ui::Action),
    Sync(sync::Action),
    Calendar(calendar::Action),
    Index(index::Action),
    Selection(selection::Action),
    Editor(editor::Action),
    Settings(settings::Action),
}

impl Actionable for Action {
    fn name(&self) -> &'static str {
        match self {
            Action::Init => "init",
            Action::Quit => "quit",
            Action::Open => "open",
            Action::UI(crate::store::ui::Action::Back) => "back",
            Action::UI(crate::store::ui::Action::Next) => "next",
            Action::Sync(sync::Action::Syncing { report_errors: _ }) => "sync",
            Action::UI(crate::store::ui::Action::NavigateSettings) => "navigate-settings",
            Action::UI(crate::store::ui::Action::NavigateFeedback) => "navigate-feedback",
            Action::UI(crate::store::ui::Action::NavigateAbout) => "navigate-about",
            Action::Editor(crate::store::editor::Action::OpenFolder) => "open-app-dir",
            _ => "",
        }
    }

    fn list() -> Vec<Self> {
        vec![
            Self::Quit,
            Self::Open,
            Self::UI(ui::Action::Next),
            Self::UI(ui::Action::Back),
            Self::Sync(sync::Action::Syncing {
                report_errors: true,
            }),
            Self::UI(crate::store::ui::Action::NavigateSettings),
            Self::UI(crate::store::ui::Action::NavigateAbout),
            Self::UI(crate::store::ui::Action::NavigateFeedback),
            Self::Editor(crate::store::editor::Action::OpenFolder),
        ]
    }

    fn keybinding(&self) -> Option<gstore::Keybinding> {
        match self {
            Self::Quit => Some(Keybinding::Ctrl("Q")),
            Self::Open => Some(Keybinding::Ctrl("O")),
            Self::UI(ui::Action::Back) => Some(Keybinding::Ctrl("J")),
            Self::UI(ui::Action::Next) => Some(Keybinding::Ctrl("K")),
            Self::Sync(sync::Action::Syncing { .. }) => Some(Keybinding::Ctrl("F")),
            Self::Editor(editor::Action::OpenFolder) => Some(Keybinding::Ctrl("B")),
            _ => None,
        }
    }

    fn try_from_name(name: &str) -> Option<Self>
    where
        Self: Sized,
    {
        match name {
            "quit" => Some(Self::Quit),
            "open" => Some(Self::Open),
            "next" => Some(Self::UI(ui::Action::Next)),
            "back" => Some(Self::UI(ui::Action::Back)),
            "sync" => Some(Self::Sync(sync::Action::Syncing {
                report_errors: true,
            })),
            "navigate-settings" => Some(Self::UI(ui::Action::NavigateSettings)),
            "navigate-about" => Some(Self::UI(ui::Action::NavigateAbout)),
            "navigate-feedback" => Some(Self::UI(ui::Action::NavigateFeedback)),
            "open-app-dir" => Some(Self::Editor(editor::Action::OpenFolder)),
            _ => None,
        }
    }
}

pub(crate) fn append_middlewares(store: &Rc<Store>) {
    store.append(Box::new(calendar::Middleware));
    store.append(Box::new(notification::Middleware));
    store.append(Box::new(login::Middleware));
    store.append(Box::new(sync::Middleware));
    store.append(Box::new(ui::Middleware));
    store.append(Box::new(index::Middleware));
    store.append(Box::new(selection::Middleware));
    store.append(Box::new(editor::Middleware));
    store.append(Box::new(settings::Middleware));
}

pub(crate) fn reduce(action: &Action, state: &mut State) {
    if std::env::var("GSTORE").is_ok() {
        println!("{:?}", action);
    }
    index::reduce(action, state);
    notification::reduce(action, state);
    login::reduce(action, state);
    ui::reduce(action, state);
    sync::reduce(action, state);
    calendar::reduce(action, state);
    selection::reduce(action, state);
    // grx::gstore_debug::send_state_update(action, state);
    editor::reduce(action, state);
    settings::reduce(action, state);
}

gstore::store!(Action, State);

pub(crate) fn dispatch_err(msg: impl Into<String>) {
    store().dispatch(Action::Error(msg.into()))
}

// pub(crate)selectors

// pub(crate)const SELECT_NOTIFICATION: &'static Selector = &|a, b| a.notification != b.notification;

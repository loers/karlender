use log::Level;
use log4rs::{
    append::{
        console::ConsoleAppender,
        rolling_file::{
            policy::compound::{
                roll::delete::DeleteRoller, trigger::size::SizeTrigger, CompoundPolicy,
            },
            RollingFileAppender,
        },
    },
    config::{Appender, Root},
    encode::pattern::PatternEncoder,
    filter::{Filter, Response},
};
use std::str::FromStr;

use crate::domain::get_app_dir;

pub fn init() {
    let version = env!("CARGO_PKG_VERSION");
    let debugging = version.contains("beta");

    let mut level = std::env::var("RUST_LOG")
        .ok()
        .and_then(|v| {
            v.split_once(',')
                .and_then(|(lvl, _)| log::LevelFilter::from_str(lvl).ok())
        })
        .unwrap_or(log::LevelFilter::Error);

    if debugging {
        level = log::LevelFilter::Debug;
    }

    log4rs::init_config(
        log4rs::Config::builder()
            .appender(
                Appender::builder()
                    .filter(Box::new(ModuleFilter::new()))
                    .build(
                        "console",
                        Box::new(
                            ConsoleAppender::builder()
                                .encoder(Box::new(PatternEncoder::new(
                                    "[{d(%Y-%m-%d %H:%M:%S)(utc)} {h({l})} {M}] {m}{n}",
                                )))
                                .build(),
                        ),
                    ),
            )
            .appender(
                Appender::builder().build(
                    "file",
                    Box::new(
                        RollingFileAppender::builder()
                            .encoder(Box::new(PatternEncoder::new(
                                "[{d(%Y-%m-%d %H:%M:%S)(utc)} {h({l})} {M}] {m}{n}",
                            )))
                            .build(
                                get_app_dir().join("karlender.log"),
                                Box::new(CompoundPolicy::new(
                                    Box::new(SizeTrigger::new(50000)),
                                    #[allow(clippy::box_default)]
                                    Box::new(DeleteRoller::default()),
                                )),
                            )
                            .unwrap(),
                    ),
                ),
            )
            .build(
                Root::builder()
                    .appender("console")
                    .appender("file")
                    .build(level),
            )
            .unwrap(),
    )
    .unwrap();
}

#[derive(Debug)]
struct ModuleFilter {
    modules: Vec<(String, Level)>,
}
impl ModuleFilter {
    fn new() -> Self {
        let mut paths = Vec::new();
        if let Ok(var) = std::env::var("RUST_LOG") {
            for kv_pair in var.split(',') {
                if let Some((k, v)) = kv_pair.split_once('=') {
                    if let Ok(l) = Level::from_str(v) {
                        paths.push((k.into(), l));
                    }
                }
            }
        }

        Self { modules: paths }
    }
}
impl Filter for ModuleFilter {
    fn filter(&self, record: &log::Record) -> log4rs::filter::Response {
        if self.modules.is_empty() {
            return Response::Neutral;
        }
        if let Some(module) = record.module_path() {
            if let Some((_, l)) = self.modules.iter().find(|p| module.starts_with(&p.0)) {
                if &record.level() <= l {
                    Response::Accept
                } else {
                    Response::Reject
                }
            } else {
                Response::Reject
            }
        } else {
            Response::Neutral
        }
    }
}

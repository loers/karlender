#!/bin/sh

pacman-key --init
pacman -Sy
pacman -S cocogitto base-devel gtk4 glibc libadwaita just openssh --noconfirm
curl https://sh.rustup.rs -sSf | sh -s -- -y
source $HOME/.cargo/env

# install cargo-gra locally
mkdir .gra -p
curl -L -o .gra/cargo-gra "https://gitlab.com/floers/cargo-gra/-/package_files/158632755/download"
chmod +x .gra/cargo-gra
export PATH="$CI_PROJECT_DIR/.gra:$PATH"

# install audit
mkdir .audit -p
curl -L -o .audit/audit.tar.gz "https://github.com/rustsec/rustsec/releases/download/cargo-audit%2Fv0.17.6/cargo-audit-x86_64-unknown-linux-musl-v0.17.6.tgz"
tar xvzf .audit/audit.tar.gz --directory .audit/
mv .audit/cargo-audit-*/cargo-audit .audit/cargo-audit
export PATH="$CI_PROJECT_DIR/.audit:$PATH"

# install outdated
mkdir -p .outdated
curl -L -o .outdated/outdated.tar.gz "https://github.com/kbknapp/cargo-outdated/releases/download/v0.13.1/cargo-outdated-0.13.1-x86_64-unknown-linux-musl.tar.gz"
tar xvzf .outdated/outdated.tar.gz --directory .outdated/
export PATH="$CI_PROJECT_DIR/.outdated:$PATH"

# install bump
mkdir .bump -p
curl -L -o .bump/cargo-bump "https://gitlab.com/floers/cargo-gra/-/package_files/84094222/download"
chmod +x .bump/cargo-bump
export PATH="$CI_PROJECT_DIR/.bump:$PATH"

VERSION=$1
DATE=$(git log -1 --date=short --format=%ad v$VERSION)
RELEASE_CONTENT=$(./ci/generate-flatpak-changelog.sh "v$VERSION")
TEMPLATE=$(echo "  # this_comment_is_required_for_automated_release\n\n\
  { version = \"${VERSION}\", date = \"${DATE}\", description = \"\"\"\n\
$RELEASE_CONTENT
\
\"\"\" },")
TEMPLATE_ESACPED=$(echo $TEMPLATE | sed -r 's/\//\\\//g')


sed -i "s/# this_comment_is_required_for_automated_release/$TEMPLATE_ESACPED/g" App.toml
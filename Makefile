clean:
	rm -rf out && rm -rf target/flatpak-temp && rm -rf target/flatpak-build && rm -f Cargo.lock && make build-aux

flatpak:
	make -C out .prepare-flatpak
	make -C out flatpak

flatpak-arm64:
	make -C out .prepare-flatpak
	make -C out flatpak-arm64

release:
	echo release

install-gsettings:
	install -D target/gra-gen/codes.loers.Karlender.gschema.xml /usr/share/glib-2.0/schemas/codes.loers.Karlender.gschema.xml
	glib-compile-schemas /usr/share/glib-2.0/schemas

uninstall-gsettings:
	rm /usr/share/glib-2.0/schemas/codes.loers.Karlender.gschema.xml
	glib-compile-schemas /usr/share/glib-2.0/schemas

build-aux:
	cargo build --bin build-aux --features build-aux --no-default-features
	./target/debug/build-aux
	@echo Done

setup:
	flatpak install org.gnome.Sdk//42 -y
	flatpak install org.gnome.Platform//42 -y
	flatpak install org.freedesktop.Sdk.Extension.rust-stable//21.08 -y